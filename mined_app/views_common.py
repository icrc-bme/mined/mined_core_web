import functools

from django.shortcuts import render, redirect
from datetime import datetime
import pytz
from pytz import timezone

from mined_app.models_demographic import SubjectView, SubjectHistopathologyView, SubjectInterventionView, \
    SubjectOutcomeView
from mined_app.models_recording import SessionView
from mined_app.models_space import ElectrodeView, ContactView


def is_nan(num):
    return num != num


def convert_to_utc(str_date_loc, time_zone, date_only):
    local_timezone = timezone(time_zone)
    if date_only:
        date_loc = datetime.strptime(str_date_loc, '%Y-%m-%d')
        # date_loc = datetime.strptime(str_date_loc, '%m/%d/%Y')
    else:
        date_loc = datetime.strptime(str_date_loc, '%Y-%m-%d %H:%M')
        # date_loc = datetime.strptime(str_date_loc, '%m/%d/%Y %H:%M')
    # print(date_loc)
    date_localize = local_timezone.localize(date_loc)
    date_utc = date_localize.astimezone(pytz.utc)
    # print(date_utc)
    db_date = datetime(date_utc.year, date_utc.month, date_utc.day, date_utc.hour, date_utc.minute)
    # print(db_date)
    return db_date


def unique_dict(in_list):
    for idx in range(len(in_list) - 1, -1, -1):
        if in_list.count(in_list[idx]) > 1:
            del in_list[idx]
    return in_list


def convert_from_utc(db_date, time_zone, date_only):
    # print(db_date)
    local_zone = timezone(time_zone)
    utc = db_date
    utc = utc.replace(tzinfo=pytz.utc)
    local_date_tz = utc.astimezone(local_zone)
    # print("localize")
    # print(local_date_tz)
    if date_only:
        local_date = datetime(local_date_tz.year, local_date_tz.month, local_date_tz.day, 0, 0)
    else:
        local_date = datetime(local_date_tz.year, local_date_tz.month, local_date_tz.day, local_date_tz.hour,
                              local_date_tz.minute)
    return local_date


def get_header_context(request, **kwargs):
    alert = kwargs.get('alert', None)
    warning = kwargs.get('warning', None)
    if warning:
        request.session['static_warning'] = warning

    header = {
        
        # User information
        'id_user': request.session["id_user"],
        'first_name': request.session['first_name'],
        'last_name': request.session['last_name'],
        'user_groups': request.session['user_groups'],

        # 'user_name': request.session['name'],
        # 'id_access': request.session['id_access'],
        # 'access_name': request.session['access'],

        # Mined information
        'db_id_institution': request.session['db_id_institution'],
        'institution_name': request.session['institution_name'],
        'timezone_name': request.session['timezone_name'],
        'id_session': request.session['db_id_session'],
        'id_subject': request.session['db_id_subject'],
        'id_electrode': request.session['db_id_electrode'],
        'id_contact': request.session['db_id_contact'],
        'subject_count': request.session['db_subject_count'],
        'session_count': request.session['db_session_count'],
        'contact_count': request.session['db_contact_count'],
        'electrode_count': request.session['db_electrode_count'],
        'session_name': request.session['db_session_name'],
        'contact_name': request.session['db_contact_name'],
        'electrode_name': request.session['db_electrode_name'],
        'id_mined': request.session['db_id_mined'],  # only for internal use - db relationship
        'main_alert': alert,
        'static_warning': request.session['static_warning'],
        'timezone': request.session['timezone'],

    }
    return header


def alert_common(request):
    alert = 'Please select appropriate record'

    if request.method == 'GET':
        id_alert = request.GET.get('id_alert')
        print(id_alert)
        if id_alert == '1':
            alert = 'Please select subject record'
        if id_alert == '2':
            alert = 'Please select session record'
        if id_alert == '3':
            alert = 'No data available'
        if id_alert == '4':
            alert = 'Please select electrode record'
        if id_alert == '5':
            alert = 'Please select contact record'
        if id_alert == '6':
            alert = 'Page under construct '

    context = get_header_context(request, alert=alert)

    return render(request, 'mined_app/alert-common.html', context)


def access(requested_permission=None):
    def wrap(function):
        @functools.wraps(function)
        def decorator(*args, **kwargs):
            if not args[0].user.has_perm(f'mined_app.{requested_permission}'):
                return 2, " "
            else:
                return function(*args, **kwargs)
        return decorator
    return wrap


def login_test(func):
    def wrapper_login_test(*args, **kwargs):
        #        return func(*args, **kwargs)  ##### Test
        if len(args) > 0:
            if not args[0].user.is_authenticated:
                return redirect('login')
            if 'id_user' not in args[0].session:
                return redirect('login')
            if int(args[0].session.get('id_user')) < 1:
                return redirect('login')
            return func(*args, **kwargs)

    return wrapper_login_test


def available_detail(request):
    detail_contact = list()
    detail_electrode = list()
    detail_session = list()
    detail_subject = list()
    histopatho_str = ""
    inter_str = ""
    outcome_str = ""
    if request.session['db_id_mined'] != 0:
        detail_subject = SubjectView.objects.get(id_mined=request.session['db_id_mined'])
        histopathologies = SubjectHistopathologyView.objects.filter(id_mined_subject=request.session['db_id_mined'])
        for histopathology in histopathologies:
            histopatho_str = " " + histopatho_str + histopathology.histopathology_type + " / "
        histopatho_str = histopatho_str[:-3]
        interventions = SubjectInterventionView.objects.filter(id_mined_subject=request.session['db_id_mined'])
        for intervention in interventions:
            inter_str = " " + inter_str + intervention.intervention_name + " / "
        inter_str = inter_str[:-3]
        outcomes = SubjectOutcomeView.objects.filter(id_mined_subject=request.session['db_id_mined'])
        for outcome in outcomes:
            outcome_str = " " + outcome_str + outcome.ilae + " : " + outcome.engel + "   "

    if request.session['db_id_session'] != 0:
        detail_session = SessionView.objects.get(id=request.session['db_id_session'])

    if request.session['db_id_electrode'] != 0 and request.session['db_id_session'] != 0:
        detail_electrode = ElectrodeView.objects.filter(id=request.session['db_id_electrode'],
                                                        id_mined_subject=request.session['db_id_mined'])[0]

    if request.session['db_id_contact'] != 0 and request.session['db_id_session'] != 0:
        detail_contact = ContactView.objects.filter(id=request.session['db_id_contact'])[0]

    context = get_header_context(request)
    context_data = {
        'detail_contact': detail_contact,
        'detail_electrode': detail_electrode,
        'detail_session': detail_session,
        'detail_subject': detail_subject,
        'histo': histopatho_str,
        'inter': inter_str,
        'outcome': outcome_str

    }
    context.update(context_data)
    print('available data', context)
    return render(request, 'mined_app/available-detail.html', context)
