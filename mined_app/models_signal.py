# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AnnotationChannelType(models.Model):
    id = models.BigAutoField(primary_key=True)
    type_name = models.CharField(max_length=1024, blank=False, null=False)
    subtype_name = models.CharField(max_length=1024, blank=False, null=False)

    class Meta:
        managed = False
        db_table = 'annotation_channel_type'

class AnnotationChannel(models.Model):
    id = models.BigAutoField(primary_key=True)
    event_start_utc = models.DateTimeField()
    event_stop_utc = models.DateTimeField(blank=True, null=True)
    annotation_text = models.CharField(max_length=256, blank=True, null=True)
#    id_channel = models.ForeignKey('Channel', models.DO_NOTHING, db_column='id_channel')
    id_channel = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'annotation_channel'
        unique_together = (('event_start_utc', 'event_stop_utc', 'id_channel'),)


class AnnotationChannelView(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_session = models.BigIntegerField(blank=True, null=True)
    event_start_utc = models.DateTimeField(blank=True, null=True)
    event_stop_utc = models.DateTimeField(blank=True, null=True)
    annotation_text = models.CharField(max_length=256, blank=True, null=True)
    type_name = models.CharField(max_length=1024, blank=False, null=False)
    subtype_name = models.CharField(max_length=1024, blank=False, null=False)
    id_channel = models.BigIntegerField(blank=True, null=True)
    channel_name = models.CharField(max_length=1024, blank=True, null=True)
    zone_name = models.CharField(max_length=512, blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'annotation_channel_view'


class Channel(models.Model):
    id = models.BigAutoField(primary_key=True)
    channel_name = models.CharField(max_length=1024, blank=True, null=True)
    sampling_frequency = models.FloatField()
    id_quality_type = models.BigIntegerField(blank=True, null=True)
    id_channel_type = models.BigIntegerField(blank=True, null=True)

#    id_quality_type = models.ForeignKey('QualityType', models.DO_NOTHING, db_column='id_quality_type')
#    id_channel_type = models.ForeignKey('ChannelType', models.DO_NOTHING, db_column='id_channel_type')
    id_session = models.BigIntegerField()
    low_cutoff = models.FloatField()
    high_cutoff = models.FloatField()
    unit = models.CharField(max_length=4)
    acquisition_number = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'channel'
        unique_together = (('channel_name', 'id_session'),)


class ChannelContact(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_channel = models.ForeignKey(Channel, models.DO_NOTHING, db_column='id_channel')
    id_contact = models.BigIntegerField()
    id_recording_reference = models.ForeignKey('RecordingReference', models.DO_NOTHING,
                                               db_column='id_recording_reference')

    class Meta:
        managed = False
        db_table = 'channel_contact'
        unique_together = (('id_channel', 'id_contact', 'id_recording_reference'),)


class ChannelContactView(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_channel = models.BigIntegerField(blank=True, null=True)
    channel_name = models.CharField(max_length=1024, blank=True, null=True)
    contact_number = models.IntegerField(blank=True, null=True)
    electrode_name = models.CharField(max_length=128, blank=True, null=True)
    c_x = models.FloatField(blank=True, null=True)
    c_y = models.FloatField(blank=True, null=True)
    c_z = models.FloatField(blank=True, null=True)
    system_name = models.CharField(max_length=1024, blank=True, null=True)
    system_unit = models.CharField(max_length=32, blank=True, null=True)
    id_session = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'channel_contact_view'


class ChannelType(models.Model):
    id = models.AutoField(primary_key=True)
    type_name = models.CharField(unique=True, max_length=1024)

    class Meta:
        managed = False
        db_table = 'channel_type'


class ChannelView(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_session = models.BigIntegerField(blank=True, null=True)
    channel_name = models.CharField(max_length=1024, blank=True, null=True)
    sampling_frequency = models.FloatField(blank=True, null=True)
    quality_type = models.CharField(max_length=1024, blank=True, null=True)
    channel_type = models.CharField(max_length=1024, blank=True, null=True)
    low_cutoff = models.FloatField(blank=True, null=True)
    high_cutoff = models.FloatField(blank=True, null=True)
    unit = models.CharField(max_length=1024, blank=True, null=True)
    acquisition_number = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'channel_view'


class QualityType(models.Model):
    id = models.AutoField(primary_key=True)
    type_name = models.CharField(unique=True, max_length=1024)

    class Meta:
        managed = False
        db_table = 'quality_type'


class RecordingReference(models.Model):
    id = models.AutoField(primary_key=True)
    reference_name = models.CharField(unique=True, max_length=1024)
    reference_note = models.CharField(max_length=1024, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'recording_reference'


class Seizure(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_seizure_channel_type = models.ForeignKey('SeizureChannelType', models.DO_NOTHING,
                                                db_column='id_seizure_channel_type', blank=True, null=True)
    id_seizure_type = models.ForeignKey('SeizureType', models.DO_NOTHING, db_column='id_seizure_type', blank=True,
                                        null=True)
    id_annotation_channel = models.ForeignKey(AnnotationChannel, models.DO_NOTHING, db_column='id_annotation_channel',
                                              blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'seizure'
        unique_together = (('id_seizure_channel_type', 'id_annotation_channel'),)


class SeizureChannelType(models.Model):
    id = models.BigAutoField(primary_key=True)
    type_name = models.CharField(unique=True, max_length=128)

    class Meta:
        managed = False
        db_table = 'seizure_channel_type'


class SeizureType(models.Model):
    id = models.BigAutoField(primary_key=True)
    type_name = models.CharField(unique=True, max_length=256)

    class Meta:
        managed = False
        db_table = 'seizure_type'


class SeizureView(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_session = models.BigIntegerField(blank=True, null=True)
    id_annotation_channel = models.BigIntegerField(blank=True, null=True)
    event_start_utc = models.DateTimeField(blank=True, null=True)
    event_stop_utc = models.DateTimeField(blank=True, null=True)
    annotation_text = models.CharField(max_length=256, blank=True, null=True)
    id_channel = models.BigIntegerField(blank=True, null=True)
    channel_name = models.CharField(max_length=1024, blank=True, null=True)
    seizure_type = models.CharField(max_length=256, blank=True, null=True)
    seizure_channel_type = models.CharField(max_length=128, blank=True, null=True)
    zone_name = models.CharField(max_length=512, blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'seizure_view'
