from django.apps import AppConfig


class AppConfig(AppConfig):
    name = 'mined_app'
