from .models_demographic import Subject, SubjectType, Sex, Histopathology, SubjectInterventionView, \
    Intervention, IlaeScale, EngelScale, MchughScale, SubjectHistopathologyView, SubjectOutcomeView, Handedness, \
    InstitutionView, SubjectBasicView
from .models_recording import Session
from .models_space import InterventionContactView
from .views_common import *
from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.urls import reverse
from urllib.parse import urlencode
from django.db import connection, Error, IntegrityError


####################################
#
# S U B J E C T     pages
#
####################################
@login_test
def overview(request):
    request.session['db_id_subject'] = 0
    request.session['db_id_mined'] = 0
    request.session['db_id_session'] = 0
    request.session['db_id_electrode'] = 0
    request.session['db_id_contact'] = 0
    request.session['db_session_count'] = 0
    request.session['db_contact_count'] = 0
    request.session['db_electrode_count'] = 0
    request.session['db_session_name'] = ''
    request.session['db_contact_name'] = ''
    request.session['db_electrode_name'] = ''
    request.session['db_id_subject_intervention'] = 0
    request.session['db_id_outcome'] = 0
    request.session['filter'] = 'false'
    request.session['db_subject_count'] = Subject.objects.all().count()
    warning = ""
    if request.session['timezone'] == 1:
        warning = "Your computer's time zone does not match the " + request.session[
            'institution_name'] + "'s time zone."

    context = get_header_context(request, warning=warning)
    filter = ""
    if request.session['patient_filter'] is not None:
        filters = request.session['patient_filter']
        filter = ' ' + filters['histo'] + ' ' + filters['inter'] + ' ' + filters['ilae'] + ' ' + filters['engel']

    context_data = {
        'card_name': 'Subject',
        'filter': filter,
    }
    context.update(context_data)
    return render(request, 'mined_app/demographic/subject-overview.html', context)


@login_test
def detail(request):
    detail_subject = list()
    alert = ""
    if request.session['db_id_mined']:
        detail_subject = SubjectView.objects.get(id_mined=request.session['db_id_mined'])
    else:
        alert = 'Please select subject record'

    histopatho_str = ""
    histopathologies = SubjectHistopathologyView.objects.filter(id_mined_subject=request.session['db_id_mined'])
    for histopathology in histopathologies:
        histopatho_str = " " + histopatho_str + histopathology.histopathology_type + "(" + histopathology.histopathology_subtype + ")" + " / "
    histopatho_str = histopatho_str[:-3]
    inter_str = ""
    interventions = SubjectInterventionView.objects.filter(id_mined_subject=request.session['db_id_mined'])
    for intervention in interventions:
        inter_str = " " + inter_str + intervention.intervention_name + " / "
    inter_str = inter_str[:-3]
    outcome_str = ""
    outcomes = SubjectOutcomeView.objects.filter(id_mined_subject=request.session['db_id_mined'])
    for outcome in outcomes:
        outcome_str = " " + outcome_str + outcome.ilae + " : " + outcome.engel + "   "

    context = get_header_context(request, alert=alert)
    context_data = {
        'detail_subject': detail_subject,  # name is used by available detail
        'histo': histopatho_str,
        'inter': inter_str,
        'outcome': outcome_str
    }
    context.update(context_data)
    return render(request, 'mined_app/demographic/subject-detail.html', context)


@login_test
def add(request):
    subject_types = SubjectType.objects.all().order_by('type_name')
    sexes = Sex.objects.all()
    handednesses = Handedness.objects.all()
    request.session['form_type'] = 'add'

    context = get_header_context(request)
    context_data = {
        'card_name': 'Subject',
        # catalogs data
        'subject_types': subject_types,
        'sexes': sexes,
        'handednesses': handednesses,
    }
    context.update(context_data)
    return render(request, 'mined_app/demographic/subject-add.html', context)


@login_test
def edit(request):
    alert = ''
    if not request.session['db_id_mined']:
        alert = 'Please select subject'
    # init search
    request.session['db_id_subject_intervention'] = 0
    request.session['db_id_outcome'] = 0

    request.session['form_type'] = 'edit'
    # catalogs data
    subject_types = SubjectType.objects.all().order_by('type_name')
    sexes = Sex.objects.all()
    handednesses = Handedness.objects.all()

    context = get_header_context(request, alert=alert)

    context_data = {
        'card_name': 'Subject',
        # catalogs without JSON
        'subject_types': subject_types,
        'sexes': sexes,
        'handednesses': handednesses,

    }
    context.update(context_data)
    return render(request, 'mined_app/demographic/subject-edit.html', context)


@login_test
def institution_overview(request):
    warning = ""
    if request.session['timezone'] == 1:
        warning = "Your computer's time zone does not match the " + request.session[
            'institution_name'] + "'s time zone."

    context = get_header_context(request, warning=warning)

    context_data = {
        'card_name': 'Institution',
    }
    context.update(context_data)
    return render(request, 'mined_app/demographic/institution-overview.html', context)


@login_test
def histopathology_edit(request):
    if not request.session['db_id_mined']:
        base_url = reverse('alert-common')  # 1 /alert-common/
        query_string = urlencode({'id_alert': 1})  # 2 id_alert=1
        url = '{}?{}'.format(base_url, query_string)  # 3 /alert-common/?id_alert=1
        return redirect(url)  # 4
    # catalogs data
    histopathologies = Histopathology.objects.all()

    context = get_header_context(request)
    context_data = {
        'card_name': 'Histopathology',
        # catalogs without JSON
        'histopathologies': histopathologies,
        # init
        'id_subject': request.session['db_id_subject'],
        'timezone': request.session['institution_timezone'],
    }
    context.update(context_data)
    return render(request, 'mined_app/demographic/histopathology-edit.html', context)


@login_test
def intervention_edit(request):
    if not request.session['db_id_mined']:
        base_url = reverse('alert-common')  # 1 /alert-common/
        query_string = urlencode({'id_alert': 1})  # 2 id_alert=1
        url = '{}?{}'.format(base_url, query_string)  # 3 /alert-common/?id_alert=1
        return redirect(url)  # 4

    alert = ""

    # actual data
    # contact list is intended to create table witch check box. Boxes are checked after select table's row
    contact_list = list(
        ContactView.objects.filter(id_mined_subject=request.session['db_id_mined']).values('contact_number',
                                                                                           'electrode_name', 'id'))
    # unique
    for idx in range(len(contact_list) - 1, -1, -1):
        if contact_list.count(contact_list[idx]) > 1:
            del contact_list[idx]
    # get max column for table with check boxes
    max_column = 0
    column = 0
    electrode_name = ""
    for contact in contact_list:
        if electrode_name == contact.get('electrode_name'):
            column = column + 1
        else:
            if column > max_column:
                max_column = column
            column = 0
            electrode_name = contact.get('electrode_name')
    if column > max_column:
        max_column = column

    # Table with intervention header - without electrodes. Electrodes are loaded after row in this table is selected
    intervention_list = list(SubjectInterventionView.objects.filter
                             (id_subject_intervention=request.session['db_id_subject_intervention']))

    # catalogs data
    intervention_type = list(Intervention.objects.all())

    context = get_header_context(request, alert=alert)
    context_data = {
        'card_name': 'Intervention',
        # catalogs without JSON
        'intervention_type': intervention_type,
        # init
        'id_subject': request.session['db_id_subject'],
        'timezone': request.session['institution_timezone'],
        # data
        'max_column': range(max_column + 1),
        'contacts': contact_list,
        'interventions': intervention_list,

    }
    context.update(context_data)
    return render(request, 'mined_app/demographic/intervention-edit.html', context)


@login_test
def outcome_edit(request):
    if not request.session['db_id_mined']:
        base_url = reverse('alert-common')  # 1 /alert-common/
        query_string = urlencode({'id_alert': 1})  # 2 id_alert=1
        url = '{}?{}'.format(base_url, query_string)  # 3 /alert-common/?id_alert=1
        return redirect(url)  # 4

    ilaes = IlaeScale.objects.all()
    engels = EngelScale.objects.all()
    mchughs = MchughScale.objects.all()

    context = get_header_context(request)
    context_data = {
        'card_name': 'Outcome',
        # catalogs without JSON
        'ilaes': ilaes,
        'engels': engels,
        'mchughs': mchughs,
        # init
        'id_subject': request.session['db_id_subject'],
        'timezone': request.session['institution_timezone'],
    }
    context.update(context_data)
    return render(request, 'mined_app/demographic/outcome-edit.html', context)


@access(requested_permission='change_subject')
def subject_save(request, sql_type):
    # subject add and subject edit must be separated because value of id_subject is entered manually, e.g.
    # Institution internal id
    # id_subject must be checked in the database
    print("Subject save called")
    seizure = 0
    sex = None
    birth = None
    handedness = None
    if 'sex' in request.POST and request.POST.get('sex'):
        sex = request.POST.get('sex')

    if 'handedness' in request.POST and request.POST.get('handedness'):
        handedness = request.POST.get('handedness')

    if 'birth' in request.POST and request.POST.get('birth') and request.POST.get('birth') != '':
        birth = datetime.strptime(request.POST.get('birth'), "%Y-%m-%d")

    if 'seizure' in request.POST and request.POST.get('seizure'):
        seizure = float(request.POST.get('seizure'))

    with connection.cursor() as cur:
        try:
            if sql_type == 0:
                cur.callproc('demographic.insert_subject',
                             [int(request.session['db_id_institution']), int(request.POST.get('id_internal')),
                              request.POST.get('type'), sex, handedness, birth, seizure, request.POST.get('note')])

            if sql_type == 1:
                cur.callproc('demographic.update_subject',
                             [int(request.session['db_id_mined']), int(request.POST.get('id_internal')),
                              request.POST.get('type'), sex, handedness, birth, seizure, request.POST.get('note')])

            id_mined = cur.fetchall()
            if id_mined[0][0] == -1:
                return 1, "id_mined = -1"

        except IntegrityError:
            return 0, ""

        except (Exception, Error) as error:
            print("internal", error.args[0])
            return 1, error.args[0]
    return 0, ""


@access(requested_permission='change_histopathology')
def histopathology_save(request):
    if request.method == 'POST':
        db_date = None
        if request.POST.get('date'):
            db_date = convert_to_utc(request.POST.get('date'), request.session['os_timezone'], False)
        print(request.POST)
        with connection.cursor() as cur:
            try:
                # demographic.save_subject_histopathology(id_subject_histopathologies integer, id_mined integer, date_h timestamp,
                # name_h character varying, OUT out_id_h integer)
                hist = Histopathology.objects.get(id=int(request.POST.get('id_type')))
                cur.callproc('demographic.save_subject_histopathologies',
                             [int(request.POST.get('id_subject_histo')), int(request.session['db_id_mined']), db_date,
                              hist.histopathology_type, hist.histopathology_subtype])
                histopathology = cur.fetchall()
                if histopathology[0][0] == -301:
                    return 1, "histopathology not found"

            except IntegrityError:
                return 0, ""

            except (Exception, Error) as error:
                print("internal", error.args[0])
                return 1, error.args[0]

        return 0, ""


@access(requested_permission='change_intervention')
def intervention_save(request):
    if request.method == 'POST':
        db_date = None
        if request.POST.get('date'):
            db_date = convert_to_utc(request.POST.get('date'), request.session['os_timezone'], False)
        print(request.POST)
        # post_list = list(request.POST.keys())
        # POST.keys contain id contacts that have been checked - see intervention_form_inc.html
        # example --- post_list['csrfmiddlewaretoken', '1796', 'inputInterventionDate', 'inputInterventionId',
        # 'inputInterventionName']
        # demographic.save_subject_interventions(id_subject_intervention integer,
        # in_dt_date_utc timestamp without time zone, in_id_mined_subject integer,
        # intervention_name character varying, ida_contact bigint[],
        # OUT out_id_subject_interventions integer
        # contact_list = list()
        # for post in post_list:
        #     if post.isnumeric():
        #         contact_list.append(int(post))
        contact_list = [int(x) for x in request.POST.getlist('contact_ids[]')]
        with connection.cursor() as cur:
            # cur.callproc('demographic.save_subject_interventions',
            #              [int(request.session['db_id_subject_intervention']), db_date,
            #               int(request.session['db_id_mined']), request.POST.get('inputInterventionName'), contact_list])
            # intervention = cur.fetchall()
            try:
                print([int(request.session['db_id_subject_intervention']), db_date,
                        int(request.session['db_id_mined']), request.POST.get('name'), contact_list])
                cur.callproc('demographic.save_subject_interventions',
                             [int(request.session['db_id_subject_intervention']), db_date,
                              int(request.session['db_id_mined']), request.POST.get('name'), contact_list])
                intervention = cur.fetchall()

                if intervention[0][0] == -501:
                    return 1, "intervention not found"

            except IntegrityError:
                return 0, ""

            except (Exception, Error) as error:
                print("internal", error.args[0])
                return 1, error.args[0]
        return 0, ''


@access(requested_permission='change_subjectoutcomes')
def outcome_save(request):
    # if add then request.POST.get('inputInterventionID') not exists
    # else edit record with inputInterventionID
    if request.method == 'POST':
        ilae = IlaeScale.objects.get(id=1).scale_name
        engel = EngelScale.objects.get(id=1).scale_name
        db_date = None

        if request.POST.get('date'):
            db_date = convert_to_utc(request.POST.get('date'), request.session['os_timezone'], False)
        if request.POST.get('ilae'):
            ilae = request.POST.get('ilae')
        if request.POST.get('engel'):
            engel = request.POST.get('engel')
        if request.POST.get('mchugh'):
            mchugh = request.POST.get('mchugh')

        with connection.cursor() as cur:
            try:
                cur.callproc('demographic.save_subject_outcome',
                             [int(request.POST.get('id_subject_outcome')), db_date, int(request.session['db_id_mined']),
                              engel, ilae, mchugh])
                outcome = cur.fetchall()
                if outcome[0][0] == -401:
                    return 1, "outcome not found"

            except IntegrityError:
                return 0, ""

            except (Exception, Error) as error:
                print("internal", error.args[0])
                return 1, error.args[0]

        return 0, ""


####################################
#
# S U B J E C T     AJAX data
#
####################################

def ajax_data(request):
    if request.method == 'POST':
        patients_filter = list()
        data_list = list()
        search = False
        filter = dict()
        if request.session['filter'] == 'true':
            filter = request.session['patient_filter']
            if len(filter['histo']) > 0 or len(filter['inter']) > 0 or len(filter['ilae']) > 0 or len(
                    filter['engel']) > 0:
                patients_filter_query = SubjectBasicView.objects.filter(
                    histopathology_name__contains=filter['histo'],
                    intervention_name__contains=filter['inter'],
                    ilae_name__contains=filter['ilae'],
                    engel_name__contains=filter['engel']).values('id_mined').distinct('id_mined')

                patients_filter = [patient['id_mined'] for patient in patients_filter_query]
                search = True
        subject_list = list(
            SubjectView.objects.filter().order_by('id_subject').all())

        for subject in subject_list:
            if not search or (search and int(subject.id_mined) in patients_filter):
                data_item = dict()
                data_item['id_mined'] = subject.id_mined
                data_item['id_subject'] = subject.id_subject
                data_item['type_name'] = subject.type_name
                data_item['dt_birth'] = ""
                if subject.dt_birth:
                    data_item['dt_birth'] = subject.dt_birth.strftime("%Y-%m-%d")
                data_item['sex_name'] = subject.sex_name
                data_item['handedness_name'] = subject.handedness_name
                data_item['note'] = subject.note
                data_item['institution_name'] = subject.institution_name
                data_item['seizure_onset_age'] = subject.seizure_onset_age
                data_item['histopathologies'] = ""
                data_item['interventions'] = ""
                data_item['outcomes'] = ""
                histopathologies = SubjectHistopathologyView.objects.filter(id_mined_subject=subject.id_mined)
                for histopathology in histopathologies:
                    data_item['histopathologies'] = " " + data_item[
                        'histopathologies'] + histopathology.histopathology_type + f" ({histopathology.histopathology_subtype})"" / "
                data_item['histopathologies'] = data_item['histopathologies'][:-3]
                interventions = SubjectInterventionView.objects.filter(id_mined_subject=subject.id_mined)
                for intervention in interventions:
                    data_item['interventions'] = " " + data_item[
                        'interventions'] + intervention.intervention_name + " / "

                data_item['interventions'] = data_item['interventions'][:-3]
                outcomes = SubjectOutcomeView.objects.filter(id_mined_subject=subject.id_mined)
                for outcome in outcomes:
                    data_item['outcomes'] = " " + data_item[
                        'outcomes'] + outcome.ilae + " : " + outcome.engel + "   "
                data_list.append(data_item)
        #            if subject.dt_birth:
        #                subject.dt_birth = subject.dt_birth.strftime("%Y-%m-%d")

        data = {
            'data': [[
                data['id_mined'],
                data['id_subject'],
                data['type_name'],
                data['dt_birth'],
                data['sex_name'],
                data['handedness_name'],
                data['note'],
                # data['institution_name'],
                data['seizure_onset_age'],
                data['histopathologies'],
                data['interventions'],
                data['outcomes']
            ] for data in data_list]
        }
        return JsonResponse(data)


"""
        data = {
            'data': [[
                subject.id_mined,
                subject.id_subject,
                subject.type_name,
                subject.dt_birth,
                subject.sex_name,
                subject.handedness_name,
                subject.note,
                subject.institution_name,
                subject.seizure_onset_age,
                subject.histopathology_count,
                subject.intervention_count,
                subject.outcome_count
            ] for subject in subject_list]
        }

    return JsonResponse(data)
"""


def ajax_select(request):
    if request.method == 'POST':
        selected = request.POST.get('selected')
        request.session['db_id_mined'] = selected
        subject = Subject.objects.get(id_mined=selected)
        request.session['db_id_subject'] = subject.id_subject
        request.session['db_session_count'] = Session.objects.filter(id_mined_subject=selected).count()

        request.session['db_id_session'] = 0
        request.session['db_id_contact'] = 0
        request.session['db_contact_count'] = 0
        request.session['db_electrode_count'] = 0
        request.session['db_session_name'] = ''
        request.session['db_contact_name'] = ''
        request.session['db_electrode_name'] = ''
        request.session['db_contact_count'] = 0
        request.session['db_electrode_count'] = 0
        data = {
            'data': [
                request.session['db_id_subject'],
                request.session['db_session_count'],
            ]
        }

        return JsonResponse(data)


def ajax_search_data(request):
    if request.method == 'POST':
        if request.POST.get('q'):
            query = request.POST.get('q')
            val = int(query)
            subject_list = list(
                SubjectView.objects.filter(id_institution=request.session['db_id_institution'],
                                           id_subject=val).order_by(
                    'id_subject').values('id_subject', 'id_mined'))

        else:
            subject_list = list(
                SubjectView.objects.all().order_by(
                    'id_subject').values('id_subject', 'id_mined'))

        return JsonResponse(subject_list, safe=False)


def ajax_insert(request):
    if request.method == 'POST':
        success, message = subject_save(request, 0)
        data = {
            'data': success,
            'message': message
        }
        return JsonResponse(data)


def ajax_update(request):
    if request.method == 'POST':
        success, message = subject_save(request, 1)
        data = {
            'data': success,
            'message': message
        }
        return JsonResponse(data)


def ajax_actual_data(request):
    # actual data - data to edit
    if request.method == 'POST':
        data = {}
        subjects_actual = SubjectView.objects.filter(id_mined=request.session['db_id_mined'])
        if len(subjects_actual) > 0:
            subject_actual = subjects_actual[0]
            if subject_actual.dt_birth:
                subject_actual.dt_birth = subject_actual.dt_birth.strftime("%Y-%m-%d")
            else:
                subject_actual.dt_birth = ''

            data = {
                'id_internal': subject_actual.id_subject,
                'type_name': subject_actual.type_name,
                'sex_name': subject_actual.sex_name,
                'birth': subject_actual.dt_birth,
                'handedness_name': subject_actual.handedness_name,
                'seizure': subject_actual.seizure_onset_age,
                'note': subject_actual.note,
                'form_type': request.session['form_type'],  # if form type insert value in html is not set

            }

        return JsonResponse(data)


def ajax_institution_data(request):
    if request.method == 'POST':
        institution_list = list(
            InstitutionView.objects.all())

        data = {
            'data': [[
                institution.id,
                institution.institution_name,
                institution.zone_name,
            ] for institution in institution_list]
        }

        return JsonResponse(data)


def ajax_institution_select(request):
    if request.method == 'POST':
        institution = InstitutionView.objects.filter(institution_name=request.POST.get('selected'))
        if len(institution) > 0:
            request.session['db_id_institution'] = institution[0].id
        data = {
            'data': []
        }
        return JsonResponse(data)


def ajax_internal_id_select(request):
    # check if institution internal id exists
    if request.method == 'POST':
        selected = request.POST.get('selected')
        count = SubjectView.objects.filter(id_institution=request.session['db_id_institution'],
                                           id_subject=int(selected)).count()

        data = {
            'data': count,
        }

        return JsonResponse(data)


def ajax_histopathology_data(request):
    if request.method == 'POST':
        histopathology_list = list(
            SubjectHistopathologyView.objects.filter(id_mined_subject=request.session['db_id_mined']))
        for histopathology in histopathology_list:
            if histopathology.dt_date_utc:
                histopathology.dt_date_utc = convert_from_utc(histopathology.dt_date_utc, histopathology.zone_name,
                                                              False)
                if request.session['timezone'] == 1:
                    histopathology.dt_date_utc = str(histopathology.dt_date_utc) + u'\t' + \
                                                 request.session['institution_timezone']

        data = {
            'data': [[
                histopathology.id,
                histopathology.id_histopathology,
                histopathology.dt_date_utc,
                histopathology.histopathology_type + f' ({histopathology.histopathology_subtype})',
            ] for histopathology in histopathology_list]
        }

        return JsonResponse(data)

# def ajax_histopathology_select(request):
#     if request.method == 'POST':
#         request.session['db_id_subject_intervention'] = request.POST.get('selected')
#         # list of electrode - to be checked in table
#         intervention_list = list(
#             InterventionContactView.objects.filter(
#                 id_subject_intervention=request.session['db_id_subject_intervention']))
#         data = {
#             'data': [[
#                 intervention.id_contact
#             ] for intervention in intervention_list]
#         }
#         return JsonResponse(data)

def ajax_histopathology_submit(request):
    if request.method == 'POST':
        success, message = histopathology_save(request)
        data = {
            'data': success,
            'message': message
        }
        return JsonResponse(data)


def ajax_intervention_data(request):
    if request.method == 'POST':
        intervention_list = list(
            SubjectInterventionView.objects.filter(id_mined_subject=request.session['db_id_mined']))
        for intervention in intervention_list:
            if intervention.dt_date_utc:
                intervention.dt_date_utc = convert_from_utc(intervention.dt_date_utc, intervention.zone_name, False)
                if request.session['timezone'] == 1:
                    intervention.dt_date_utc = str(intervention.dt_date_utc) + u'\t' + \
                                               request.session['institution_timezone']
        data = {
            'data': [[
                intervention.id_subject_intervention,
                intervention.id_mined_subject,
                intervention.dt_date_utc,
                intervention.intervention_name,
            ] for intervention in intervention_list]
        }

        return JsonResponse(data)


def ajax_intervention_select(request):
    if request.method == 'POST':
        request.session['db_id_subject_intervention'] = request.POST.get('selected')
        # list of electrode - to be checked in table
        intervention_list = list(
            InterventionContactView.objects.filter(
                id_subject_intervention=request.session['db_id_subject_intervention']))
        data = {
            'data': [[
                intervention.id_contact
            ] for intervention in intervention_list]
        }
        return JsonResponse(data)


def ajax_intervention_submit(request):
    if request.method == 'POST':
        success, message = intervention_save(request)
        data = {
            'data': success,
            'message': message
        }
        return JsonResponse(data)


def ajax_contact_data(request):
    if request.method == 'POST':
        data = {
            'data': []
        }
        if 'db_id_subject_intervention' in request.session:
            contact_list = list(
                InterventionContactView.objects.filter(id_subject_intervention=request.session[
                    'db_id_subject_intervention']))
            data = {
                'data': [[
                    contact.contact_name,
                    contact.system_name,
                    contact.system_unit,
                    contact.c_x,
                    contact.c_y,
                    contact.c_z,
                ] for contact in contact_list]
            }
        return JsonResponse(data)


def ajax_outcome_data(request):
    if request.method == 'POST':
        outcome_list = list(
            SubjectOutcomeView.objects.filter(id_mined_subject=request.session['db_id_mined']))
        for outcome in outcome_list:
            if outcome.dt_date_utc:
                outcome.dt_date_utc = convert_from_utc(outcome.dt_date_utc, outcome.zone_name, False)
                if request.session['timezone'] == 1:
                    outcome.dt_date_utc = str(outcome.dt_date_utc) + u'\t' + request.session['institution_timezone']

        data = {
            'data': [[
                outcome.id,
                outcome.dt_date_utc,
                outcome.ilae,
                outcome.engel,
                outcome.mchugh
            ] for outcome in outcome_list]
        }

        return JsonResponse(data)


def ajax_outcome_submit(request):
    if request.method == 'POST':
        success, message = outcome_save(request)
        data = {
            'data': success,
            'message': message
        }
        return JsonResponse(data)


def ajax_plotly_histo(request):
    if request.method == 'POST':
        histopathologies = Histopathology.objects.all()
        hysto_counts = list()
        hysto_names = list()
        count_other = 0
        for histopathology in histopathologies:
            count = SubjectHistopathologyView.objects.filter(
                histopathology_type__contains=histopathology.histopathology_type).count()
            if count > 1:
                hysto_counts.append(count)
                hysto_names.append(histopathology.histopathology_type)
            else:
                count_other = count_other + count

        hysto_counts.append(count_other)
        hysto_names.append('Other')
        plot_data = {'v': hysto_counts}
        plot_labels = {'l': hysto_names}
        plot_data.update(plot_labels)
        data = {'data': plot_data}
        return JsonResponse(data)


def ajax_plotly_inter(request):
    if request.method == 'POST':
        interventions = Intervention.objects.all()
        inter_counts = list()
        inter_names = list()
        for intervention in interventions:
            count = SubjectInterventionView.objects.filter(
                intervention_name__contains=intervention.intervention_name).count()
            if count != 0:
                inter_counts.append(count)
                inter_names.append(intervention.intervention_name)
        plot_data = {'v': inter_counts}
        plot_labels = {'l': inter_names}
        plot_data.update(plot_labels)
        data = {'data': plot_data}
        return JsonResponse(data)


def ajax_plotly_outcome(request):
    if request.method == 'POST':
        ilaes = IlaeScale.objects.all()
        ilae_counts = list()
        ilae_names = list()
        for ilae in ilaes:
            count = SubjectOutcomeView.objects.filter(ilae__contains=ilae.scale_name).count()
            if count != 0:
                ilae_counts.append(count)
                ilae_names.append(ilae.scale_name)
        engels = EngelScale.objects.all()
        engel_counts = list()
        engel_names = list()
        for engel in engels:
            count = SubjectOutcomeView.objects.filter(engel__contains=engel.scale_name).count()
            if count != 0:
                engel_counts.append(count)
                engel_names.append(engel.scale_name)

        plot_data = {'vi': ilae_counts}
        plot_data.update({'li': ilae_names})
        plot_data.update({'ve': engel_counts})
        plot_data.update({'le': engel_names})

        data = {'data': plot_data}
        return JsonResponse(data)


def ajax_plotly_histo_select(request):
    if request.method == 'POST':
        selected = request.POST.get('histo')
        if selected is None:
            selected = ''
        request.session['patient_filter']['histo'] = selected
        filter = request.session['patient_filter']
        patient_count = SubjectBasicView.objects.filter(histopathology_name__contains=filter['histo'],
                                                        intervention_name__contains=filter['inter'],
                                                        ilae_name__contains=filter['ilae'],
                                                        engel_name__contains=filter['engel']).distinct(
            'id_mined').count()
        data = {'data': patient_count}
        return JsonResponse(data)


def ajax_plotly_inter_select(request):
    if request.method == 'POST':
        selected = request.POST.get('inter')
        if selected is None:
            selected = ''
        request.session['patient_filter']['inter'] = selected
        filter = request.session['patient_filter']
        patient_count = SubjectBasicView.objects.filter(histopathology_name__contains=filter['histo'],
                                                        intervention_name__contains=filter['inter'],
                                                        ilae_name__contains=filter['ilae'],
                                                        engel_name__contains=filter['engel']).distinct(
            'id_mined').count()
        data = {'data': patient_count}
        return JsonResponse(data)


def ajax_plotly_outcome_select(request):
    if request.method == 'POST':
        patient_count = 0
        selected = request.POST.get('outcome')
        if selected is None:
            selected = ''
            request.session['patient_filter']['ilae'] = selected
            request.session['patient_filter']['engel'] = selected
        else:
            ilae_count = IlaeScale.objects.filter(scale_name=selected).count()
            if ilae_count > 0:
                request.session['patient_filter']['ilae'] = selected
            else:
                request.session['patient_filter']['engel'] = selected
            filter = request.session['patient_filter']
            patient_count = SubjectBasicView.objects.filter(id_institution=request.session['db_id_institution'],
                                                            histopathology_name__contains=filter['histo'],
                                                            intervention_name__contains=filter['inter'],
                                                            ilae_name__contains=filter['ilae'],
                                                            engel_name__contains=filter['engel']).distinct(
                'id_mined').count()
        data = {'data': patient_count}
        return JsonResponse(data)


def ajax_filter(request):
    if request.method == 'POST':
        request.session['filter'] = request.POST.get('filter')
        data = {'data': 0}
        return JsonResponse(data)
