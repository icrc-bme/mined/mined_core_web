# preparation for clinical database  - it is bad - must be changed

class ClinicRouter:
    """
    A router to control all database operations on models in the
    user application.
    """

    def db_for_read(self, model, **hints):
        """
        Attempts to read clinic models go to clinic_db.
        """
        if model._meta.app_label == 'clinic_data':
            return 'mined_clinic'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write clinic models go to clinic_db.
        """
        if model._meta.app_label == 'clinic_data':
            return 'mined_clinic'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the clinic app is involved.
        """
        if obj1._meta.app_label == 'clinic_data' or \
                obj2._meta.app_label == 'clinic_data':
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the auth app only appears in the 'clinic_db'
        database.
        """
        if app_label == 'clinic_data':
            return db == 'mined'
        return None
