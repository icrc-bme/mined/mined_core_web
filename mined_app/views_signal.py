from django.db import connection, Error, IntegrityError
from django.http import JsonResponse
from django.shortcuts import render, reverse, redirect
from urllib.parse import urlencode

from .models_recording import SessionView, SessionType, Modality, TrialView
from .models_signal import ChannelContactView, AnnotationChannelView, \
    RecordingReference, ChannelView, AnnotationChannelType
from .models_space import ContactFullView, ElectrodeView
from .views_common import *


#######################################
#
# S I G N A L - CH A N N E L     pages
#
#######################################
@login_test
def channel_overview(request):
    alert = ""
    if not request.session['db_id_session']:
        alert = 'Please select session'

    context = get_header_context(request, alert=alert)
    context_data = {
        'card_name': 'Channel',
    }
    context.update(context_data)

    return render(request, 'mined_app/signal/channel-overview.html', context)


@login_test
def channel_annotation_edit(request):
    alert = ""
    # actual data
    if not request.session['db_id_session']:
        alert = 'Please select session'

    request.session['db_id_annotation_channel'] = 0

    # actual data - all channels for checkbox table
    channels = ChannelView.objects.filter(id_session=request.session['db_id_session']).values('id', 'channel_name')

    annotation_channel_types = AnnotationChannelType.objects.order_by().values('type_name').distinct()
    annotation_channel_subtypes = AnnotationChannelType.objects.order_by().values('subtype_name').distinct()

    # catalogs without JSON
    context = get_header_context(request, alert=alert)
    context_data = {
        'card_name': 'Channel annotation',
        # catalogs without JSON
        'timezone': request.session['institution_timezone'],
        # data
        'channels': channels,
        'annotation_channel_types': annotation_channel_types,
        'annotation_channel_subtypes': annotation_channel_subtypes
    }
    context.update(context_data)
    return render(request, 'mined_app/signal/channel-annotation-edit.html', context)


@access(requested_permission='change_annotationchannel')
def channel_annotation_save(request):
    db_start = None
    db_stop = None
    if request.POST.get('start'):
        db_start = convert_to_utc(request.POST.get('start'), request.session['os_timezone'], False)
    if request.POST.get('stop'):
        db_stop = convert_to_utc(request.POST.get('stop'), request.session['os_timezone'], False)

    post_list = list(request.POST.keys())
    # POST.keys contain id contacts that have been checked - see intervention_form_inc.html
    # example --- post_list['csrfmiddlewaretoken', '1796', 'inputStart', 'inputStop',
    # 'inputText']
    # request.session['db_id_annotation_channel'] is set in annotation_select
    channel_list = list()
    for post in post_list:
        if 'ids_insert' in post:
            channel_list.append(int(request.POST.get(post)))
        if 'ids_update' in post:
            channel_list.append(int(request.POST.get(post)))
    if len(channel_list) < 1:
        return 2, "Please select at least one channel"

    with connection.cursor() as cur:
        try:
            cur.callproc('signal.save_annotation_channel',
                         [request.POST.get('insert'), db_start, db_stop,
                          request.POST.get('text'),
                          request.POST.get('type'),
                          request.POST.get('subtype'),
                          channel_list])
            annotation = cur.fetchall()
            if annotation[0][0] < 0:
                return 1, "channel annotation not found"

        except IntegrityError:
            return 0, ""

        except (Exception, Error) as error:
            print("internal", error.args[0])
            return 1, error.args[0]

    return 0, ""


####################################
#
# S I G N A L - CH A N N E L    AJAX data
#
####################################

def ajax_channel_data(request):
    if request.method == 'POST':
        record_list = list(ChannelView.objects.filter(id_session=request.session['db_id_session']))
        for record in record_list:
            if is_nan(record.low_cutoff):
                record.low_cutoff = ''
            if is_nan(record.high_cutoff):
                record.high_cutoff = ''

        data = {
            'data': [[
                record.id,
                record.channel_name,
                record.sampling_frequency,
                record.channel_type,
                record.quality_type,
                record.low_cutoff,
                record.high_cutoff,
                record.unit,
                record.acquisition_number,
            ] for record in record_list]
        }
        return JsonResponse(data)


def ajax_channel_annotation_data(request):
    # annotation channel (not annotation session)
    if request.method == 'POST':
        annotation_list = list(
            AnnotationChannelView.objects.filter(id_session=request.session['db_id_session']))

        for annotation in annotation_list:
            if annotation.event_start_utc:
                annotation.event_start_utc = convert_from_utc(annotation.event_start_utc, annotation.zone_name,
                                                              False)
                if request.session['timezone'] == 1:
                    annotation.event_start_utc = str(annotation.event_start_utc) + u'\t' + \
                                                 request.session['institution_timezone']

            if annotation.event_stop_utc:
                annotation.event_stop_utc = convert_from_utc(annotation.event_stop_utc, annotation.zone_name,
                                                             False)
                if request.session['timezone'] == 1:
                    annotation.event_stop_utc = str(annotation.event_stop_utc) + u'\t' + \
                                                request.session['institution_timezone']

        data = {
            'data': [[
                annotation.id,
                annotation.channel_name,
                annotation.event_start_utc,
                annotation.event_stop_utc,
                annotation.annotation_text,
                annotation.type_name,
                annotation.subtype_name
            ] for annotation in annotation_list]
        }

        return JsonResponse(data)

def ajax_channel_annotation_subtype_select(request):
    if request.method == 'POST':
        annotation_type = request.POST.get('selected_type')
        annotation_channel_subtypes = AnnotationChannelType.objects.filter(type_name=annotation_type).values('subtype_name').distinct()

        data = {
            'data': [x['subtype_name'] for x in annotation_channel_subtypes]
        }

        return JsonResponse(data)


def ajax_channel_annotation_submit(request):
    if request.method == 'POST':
        success, message = channel_annotation_save(request)
        data = {
            'data': success,
            'message': message
        }
        return JsonResponse(data)
