"""mined URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from mined_app import views
from mined_app import views_common
from mined_app import views_subject
from mined_app import views_recording
from mined_app import views_signal
from mined_app import views_electrode
from mined_app import views_contact
from mined_app import views_user


urlpatterns = [
    # The home page
    path('', views.login, name='login'),
    path('index', views.index, name='index'),
    path('login', views.login, name='login'),
    path('logout', views.logout, name='logout'),
    path('alert-common', views_common.alert_common, name='alert-common'),
    path('dashboard', views.dashboard, name='dashboard'),
    path('dashboard-select', views.dashboard_select, name='dashboard-select'),

    # INSTITUTION & USER
    path('change-password', views_user.change_password, name='change-password'),
    path('user-edit', views_user.user_edit, name='user-edit'),
    path('institution-edit', views_user.institution_edit, name='institution-edit'),
    path('institution-insert', views_user.ajax_institution_save, name='institution-insert'),
    path('institution-update', views_user.ajax_institution_save, name='institution-update'),
    path('institution-delete', views_user.ajax_institution_delete, name='institution-delete'),
    path('user-data', views_user.ajax_user_data, name='user-data'),
    path('user-insert', views_user.ajax_user_insert, name='user-insert'),
    path('user-update', views_user.ajax_user_update, name='user-update'),
    path('user-delete', views_user.ajax_user_delete, name='user-delete'),

    # SUBJECT
    path('institution-overview', views_subject.institution_overview, name='institution-overview'),
    path('institution-data', views_subject.ajax_institution_data, name='institution-data'),
    path('institution-select', views_subject.ajax_institution_select, name='select'),
    path('subject-overview', views_subject.overview, name='subject-overview'),
    path('subject-data', views_subject.ajax_data, name='subject-data'),
    path('subject-detail', views_subject.detail, name='subject-detail'),
    path('subject-add', views_subject.add, name='subject-add'),
    path('subject-edit', views_subject.edit, name='subject-edit'),
    path('subject-select', views_subject.ajax_select, name='subject-select'),                       # used on overview
    path('internal-id-select', views_subject.ajax_internal_id_select, name='internal-id-select'),   # check unique for insert
    path('subject-search-data', views_subject.ajax_search_data, name='subject-search-data'),        # used on base_site
    path('subject-insert', views_subject.ajax_insert, name='subject-insert'),                       # button insert
    path('subject-actual-data', views_subject.ajax_actual_data, name='subject-actual-data'),        # data for edit
    path('subject-update', views_subject.ajax_update, name='subject-update'),                       # button update
    path('subject-filter', views_subject.ajax_filter, name='subject-filter'),  # button update


    path('histopathology-edit', views_subject.histopathology_edit, name='histopathology-edit'),
    path('histopathology-data', views_subject.ajax_histopathology_data, name='histopathology-data'),
    #path('histopathology-select', views_subject.ajax_histopathology_select, name='histopathology-select'),
    path('histopathology-submit', views_subject.ajax_histopathology_submit, name='histopathology-submit'),  #edit/insert
    path('histopathology-plotly-data', views_subject.ajax_plotly_histo, name='histopathology-plotly-data'),
    path('histopathology-plotly-select', views_subject.ajax_plotly_histo_select, name='histopathology-plotly-select'),

    path('intervention-edit', views_subject.intervention_edit, name='intervention-edit'),
    path('intervention-data', views_subject.ajax_intervention_data, name='intervention-data'),
    path('intervention-submit', views_subject.ajax_intervention_submit, name='intervention-submit'),
    path('intervention-select', views_subject.ajax_intervention_select, name='intervention-select'),
    path('inter-plotly-data', views_subject.ajax_plotly_inter, name='inter-plotly-data'),
    path('inter-plotly-select', views_subject.ajax_plotly_inter_select, name='histopathology-inter-select'),
    path('subject-contact-data', views_subject.ajax_contact_data, name='subject-contact-data'),  # for intervention

    path('outcome-edit', views_subject.outcome_edit, name='outcome-edit'),
    path('outcome-data', views_subject.ajax_outcome_data, name='outcome-data'),
    path('outcome-submit', views_subject.ajax_outcome_submit, name='outcome-submit'),           #edit/insert
    path('outcome-plotly-data', views_subject.ajax_plotly_outcome, name='outcome-plotly-data'),
    path('outcome-plotly-select', views_subject.ajax_plotly_outcome_select, name='outcome-plotly-select'),

    # SESSION
    path('session-overview', views_recording.overview, name='session-overview'),
    path('session-detail', views_recording.detail, name='session-detail'),
    path('session-edit', views_recording.edit, name='session-edit'),
    path('session-annotation-edit', views_recording.session_annotation_edit, name='session-annotation-edit'),
    path('session-annotation-data', views_recording.ajax_session_annotation_data, name='session-annotation-data'),
    path('session-annotation-subtype-select', views_recording.ajax_session_annotation_subtype_select, name='session-annotation-subtype-select'),
    path('session-annotation-submit', views_recording.ajax_session_annotation_submit, name='session-annotation-submit'),
    path('session-data', views_recording.ajax_data, name='session-data'),                       # used on overview
    path('session-select', views_recording.ajax_select, name='session-select'),                 # used on overview for edit
    path('session-search-data', views_recording.ajax_search_data, name='session-search-data'),  # used on base_site
    path('session-update', views_recording.ajax_session_update, name='session-update'),         # button update
    path('trial-edit', views_recording.trial_edit, name='trial-edit'),
    path('trial-data', views_recording.ajax_trial_data, name='trial-data'),
    path('trial-select', views_recording.ajax_trial_select, name='trial-select'),
    path('type-plotly-data', views_recording.ajax_plotly_type, name='type-plotly-data'),

    # SIGNAL
    path('channel-overview', views_signal.channel_overview, name='channel-overview'),
    path('channel-data', views_signal.ajax_channel_data, name='channel-data'),
    path('channel-annotation-edit', views_signal.channel_annotation_edit, name='channel-annotation-edit'),
    path('channel-annotation-data', views_signal.ajax_channel_annotation_data, name='channel-annotation-data'),
    path('channel-annotation-subtype-select', views_signal.ajax_channel_annotation_subtype_select, name='session-channel-subtype-select'),
    path('channel-annotation-submit', views_signal.ajax_channel_annotation_submit, name='channel-annotation-submit'),

    # ELECTRODE
    path('electrode-edit', views_electrode.edit, name='electrode-edit'),
    path('electrode-data', views_electrode.ajax_data, name='electrode-data'),
    path('electrode-select', views_electrode.ajax_select, name='electrode-select'),                 # used on base_site
    path('electrode-multiple', views_electrode.ajax_multiple_select, name='electrode-multiple'),    # multiple select on click
    path('electrode-contacts-data', views_electrode.ajax_contacts_data, name='electrode-contacts-data'),
    path('electrode-search-data', views_electrode.ajax_search_data, name='electrode-search-data'),  # used on base_site
    path('electrode-type-data', views_electrode.ajax_electrode_type_data, name='electrode-type-data'),
    path('electrode-update', views_electrode.ajax_electrode_update, name='electrode-update'),

    # CONTACT
    path('contact-overview', views_contact.overview, name='contact-overview'),
    path('contact-structured', views_contact.structured, name='contact-structured'),
    path('contact-detail', views_contact.detail, name='contact-detail'),
    path('contact-edit', views_contact.edit, name='contact-edit'),
    path('contact-select', views_contact.ajax_select, name='contact-select'),                   # used on base_site
    path('contact-data', views_contact.ajax_data, name='contact-data'),
    path('contact-search-data', views_contact.ajax_search_data, name='contact-search-data'),    # used on base_site
    path('contact-data-export', views_contact.ajax_export, name='contact-data-export'),
    path('contact-update', views_contact.ajax_update, name='contact-update'),
    path('lobe-plotly-data', views_contact.ajax_plotly_lobe, name='lobe-plotly-data'),
    # DETAIL
    path('available-detail', views.available_detail, name='available-detail'),

]

