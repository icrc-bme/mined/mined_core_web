# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Anatomy(models.Model):
    id = models.BigAutoField(primary_key=True)
    hemisphere = models.BooleanField()
    brain_area = models.SmallIntegerField()
    id_atlas_type = models.BigIntegerField(blank=True, null=True)
    id_anatomy_structure = models.BigIntegerField(blank=True, null=True)
    id_lobe = models.BigIntegerField(blank=True, null=True)

 #   id_atlas_type = models.ForeignKey('AtlasType', models.DO_NOTHING, db_column='id_atlas_type')
 #   id_anatomy_structure = models.ForeignKey('AnatomyStructure', models.DO_NOTHING, db_column='id_anatomy_structure')
 #   id_lobe = models.ForeignKey('Lobe', models.DO_NOTHING, db_column='id_lobe')

    class Meta:
        managed = False
        db_table = 'anatomy'
        unique_together = (('hemisphere', 'id_atlas_type', 'id_anatomy_structure', 'id_lobe'),)


class AnatomyContact(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_contact = models.ForeignKey('Contact', models.DO_NOTHING, db_column='id_contact')
    id_anatomy = models.ForeignKey(Anatomy, models.DO_NOTHING, db_column='id_anatomy')
    manual_assign = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'anatomy_contact'
        unique_together = (('id_contact', 'id_anatomy', 'manual_assign'),)


class AnatomyStructure(models.Model):
    id = models.BigAutoField(primary_key=True)
    structure_name = models.CharField(unique=True, max_length=1024, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'anatomy_structure'


class AnatomyView(models.Model):
    id = models.AutoField(primary_key=True)
    hemisphere = models.BooleanField(blank=True, null=True)
    brain_area = models.SmallIntegerField(blank=True, null=True)
    id_atlas_type = models.IntegerField(blank=True, null=True)
    id_anatomy_structure = models.IntegerField(blank=True, null=True)
    id_lobe = models.IntegerField(blank=True, null=True)
    atlas_name = models.CharField(max_length=1024, blank=True, null=True)
    lobe_name = models.CharField(max_length=1024, blank=True, null=True)
    structure_name = models.CharField(max_length=1024, blank=True, null=True)
    id_contact = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'anatomy_view'


class AtlasStructureView(models.Model):
    id = models.AutoField(primary_key=True)
    id_atlas_type = models.IntegerField(blank=True, null=True)
    structure_name = models.CharField(max_length=1024, blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'atlas_structure_view'


class AtlasType(models.Model):
    id = models.BigAutoField(primary_key=True)
    type_name = models.CharField(unique=True, max_length=1024)

    class Meta:
        managed = False
        db_table = 'atlas_type'


class Contact(models.Model):
    id = models.BigAutoField(primary_key=True)
    contact_number = models.IntegerField(blank=True, null=True)                 # within electrode e.g. conatact name =
    impedance = models.FloatField(blank=True, null=True)                        # electrode name + contact number
    id_contact_size = models.ForeignKey('ContactSize', models.DO_NOTHING, db_column='id_contact_size')
    id_contact_type = models.ForeignKey('ContactType', models.DO_NOTHING, db_column='id_contact_type')
    id_electrode = models.ForeignKey('Electrode', models.DO_NOTHING, db_column='id_electrode', blank=True, null=True)
    id_imaging_pathology = models.ForeignKey('ImagingPathology', models.DO_NOTHING, db_column='id_imaging_pathology')

    class Meta:
        managed = False
        db_table = 'contact'


class ContactAnatomyView(models.Model):
    id_contact = models.BigAutoField(primary_key=True)
    id_anatomy = models.IntegerField(blank=True, null=True)
    hemisphere = models.BooleanField(blank=True, null=True)
    brain_area = models.SmallIntegerField(blank=True, null=True)
    manual_assign = models.BooleanField(blank=True, null=True)
    atlas_name = models.CharField(max_length=1024, blank=True, null=True)
    structure_name = models.CharField(max_length=1024, blank=True, null=True)
    id_atlas_type = models.IntegerField(blank=True, null=True)
    id_anatomy_structure = models.IntegerField(blank=True, null=True)
    id_lobe = models.IntegerField(blank=True, null=True)
    lobe_name = models.CharField(max_length=1024, blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'contact_anatomy_view'


class ContactFullView(models.Model):
    id_contact = models.BigAutoField(primary_key=True)
    id_session = models.BigIntegerField(blank=True, null=True)
    id_mined_subject = models.IntegerField(blank=True, null=True)
    id_channel_contact = models.BigIntegerField(blank=True, null=True)
    id_electrode = models.IntegerField(blank=True, null=True)
    contact_name = models.CharField(max_length=128, blank=True, null=True)
    impedance = models.FloatField(blank=True, null=True)
    acquisition_number = models.IntegerField(blank=True, null=True)
    pathology_name = models.CharField(max_length=1024, blank=True, null=True)
    contact_size = models.CharField(max_length=1024, blank=True, null=True)
    contact_size_value = models.FloatField(blank=True, null=True)
    contact_type = models.CharField(max_length=1024, blank=True, null=True)
    electrode_name = models.CharField(max_length=1024, blank=True, null=True)
    electrode_type = models.CharField(max_length=128, blank=True, null=True)
    electrode_manuf = models.CharField(max_length=1024, blank=True, null=True)
    contact_material = models.CharField(max_length=1024, blank=True, null=True)
    macro_contact_distance = models.FloatField(blank=True, null=True)
    micro_contact_distance = models.FloatField(blank=True, null=True)
    dimension_a = models.SmallIntegerField(blank=True, null=True)
    dimension_b = models.SmallIntegerField(blank=True, null=True)
    manual_assign = models.BooleanField(blank=True, null=True)
    brain_area = models.SmallIntegerField(blank=True, null=True)
    hemisphere = models.BooleanField(blank=True, null=True)
    atlas_name = models.CharField(max_length=1024, blank=True, null=True)
    lobe_name = models.CharField(max_length=1024, blank=True, null=True)
    structure_name = models.CharField(max_length=1024, blank=True, null=True)
    reference_name = models.CharField(max_length=1024, blank=True, null=True)
    system_name = models.CharField(max_length=1024, blank=True, null=True)
    system_unit = models.CharField(max_length=32, blank=True, null=True)
    c_x = models.FloatField(blank=True, null=True)
    c_y = models.FloatField(blank=True, null=True)
    c_z = models.FloatField(blank=True, null=True)


    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'contact_full_view'


class ContactSize(models.Model):
    id = models.BigAutoField(primary_key=True)
    size_name = models.CharField(max_length=1024)
    size_value = models.FloatField()

    class Meta:
        managed = False
        db_table = 'contact_size'


class ContactSubjectsInterventions(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_contact = models.ForeignKey(Contact, models.DO_NOTHING, db_column='id_contact')
    id_subject_intervention = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'contact_subjects_interventions'
        unique_together = (('id_contact', 'id_subject_intervention'),)


class ContactType(models.Model):
    id = models.BigAutoField(primary_key=True)
    type_name = models.CharField(unique=True, max_length=1024)

    class Meta:
        managed = False
        db_table = 'contact_type'


class ContactView(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_mined_subject = models.IntegerField(blank=True, null=True)
    contact_number = models.IntegerField(blank=True, null=True)
    impedance = models.FloatField(blank=True, null=True)
    type_name = models.CharField(max_length=1024, blank=True, null=True)
    size_name = models.CharField(max_length=1024, blank=True, null=True)
    size_value = models.FloatField(blank=True, null=True)
    pathology_name = models.CharField(max_length=1024, blank=True, null=True)
    c_x = models.FloatField(blank=True, null=True)
    c_y = models.FloatField(blank=True, null=True)
    c_z = models.FloatField(blank=True, null=True)
    coordinate_name = models.CharField(max_length=1024, blank=True, null=True)
    coordinate_unit = models.CharField(max_length=32, blank=True, null=True)
    hemisphere = models.BooleanField(blank=True, null=True)
    brain_area = models.SmallIntegerField(blank=True, null=True)
    manual_assign = models.BooleanField(blank=True, null=True)
    atlas_name = models.CharField(max_length=1024, blank=True, null=True)
    structure_name = models.CharField(max_length=1024, blank=True, null=True)
    lobe_name = models.CharField(max_length=1024, blank=True, null=True)
    id_electrode = models.IntegerField(blank=True, null=True)
    electrode_name = models.CharField(max_length=1024, blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'contact_view'


class CoordinateSystem(models.Model):
    id = models.BigAutoField(primary_key=True)
    system_name = models.CharField(max_length=1024)
    system_unit = models.CharField(max_length=32)

    class Meta:
        managed = False
        db_table = 'coordinate_system'
        unique_together = (('system_name', 'system_unit'),)


class Coordinates(models.Model):
    id = models.BigAutoField(primary_key=True)
    c_x = models.FloatField(blank=True, null=True)
    c_y = models.FloatField(blank=True, null=True)
    c_z = models.FloatField(blank=True, null=True)
    id_coordinate_system = models.IntegerField(blank=True, null=True)
    id_contact = models.BigIntegerField(blank=True, null=True)

    #    id_coordinate_system = models.ForeignKey(CoordinateSystem, models.DO_NOTHING, db_column='id_coordinate_system', blank=True, null=True)
    #    id_contact = models.ForeignKey(Contact, models.DO_NOTHING, db_column='id_contact')

    class Meta:
        managed = False
        db_table = 'coordinates'
        unique_together = (('id_contact', 'id_coordinate_system'),)


class Electrode(models.Model):
    id = models.BigAutoField(primary_key=True)
    electrode_name = models.CharField(max_length=1024)
    id_electrode_type = models.ForeignKey('ElectrodeType', models.DO_NOTHING, db_column='id_electrode_type')
    id_mined_subject = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'electrode'
        unique_together = (('electrode_name', 'id_electrode_type'),)


class ElectrodeType(models.Model):
    id = models.BigAutoField(primary_key=True)
    type_name = models.CharField(max_length=128)
    manufacturer = models.CharField(max_length=1024)
    contact_material = models.CharField(max_length=1024, blank=True, null=True)
    macro_contact_distance = models.FloatField(blank=True, null=True)
    micro_contact_distance = models.FloatField(blank=True, null=True)
    dimension_a = models.SmallIntegerField()
    dimension_b = models.SmallIntegerField()

    class Meta:
        managed = False
        db_table = 'electrode_type'
        unique_together = (('type_name', 'manufacturer', 'dimension_a', 'dimension_b'),)


class ElectrodeView(models.Model):
    id = models.AutoField(primary_key=True)
    id_mined_subject = models.IntegerField(blank=True, null=True)
    electrode_name = models.CharField(max_length=1024, blank=True, null=True)
    id_electrode_type = models.IntegerField(blank=True, null=True)
    manufacturer = models.CharField(max_length=1024, blank=True, null=True)
    contact_material = models.CharField(max_length=1024, blank=True, null=True)
    electrode_type = models.CharField(max_length=128, blank=True, null=True)
    macro_contact_distance = models.FloatField(blank=True, null=True)
    micro_contact_distance = models.FloatField(blank=True, null=True)
    dimension_a = models.SmallIntegerField(blank=True, null=True)
    dimension_b = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'electrode_view'


class ImagingPathology(models.Model):
    id = models.BigAutoField(primary_key=True)
    pathology_name = models.CharField(max_length=1024)

    class Meta:
        managed = False
        db_table = 'imaging_pathology'


class InterventionContactView(models.Model):
    id_subject_intervention = models.AutoField(primary_key=True)
    id_contact = models.BigIntegerField(blank=True, null=True)
    contact_number = models.IntegerField(blank=True, null=True)
    electrode_name = models.CharField(max_length=128, blank=True, null=True)
    c_x = models.FloatField(blank=True, null=True)
    c_y = models.FloatField(blank=True, null=True)
    c_z = models.FloatField(blank=True, null=True)
    system_name = models.CharField(max_length=1024, blank=True, null=True)
    system_unit = models.CharField(max_length=32, blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'intervention_contact_view'


class Lobe(models.Model):
    id = models.BigAutoField(primary_key=True)
    lobe_name = models.CharField(unique=True, max_length=1024, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'lobe'


class StructureLobeView(models.Model):
    id_anatomy_structure = models.AutoField(primary_key=True)
    lobe_name = models.CharField(max_length=1024, blank=True, null=True)
    id_lobe = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'structure_lobe_view'
