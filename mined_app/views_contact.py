from .models import *
from .models_space import ContactType, CoordinateSystem, ContactSize, AtlasType, AnatomyStructure, Lobe, \
    ContactView, AtlasStructureView, StructureLobeView, AnatomyView, Anatomy, ContactAnatomyView, ImagingPathology
from .views_common import *
from django.shortcuts import render, reverse, redirect
from django.http import JsonResponse
from urllib.parse import urlencode
from django.db import connection, transaction, Error, connections, IntegrityError
import re


##########################
#
# C O N T A C T     pages
#
##########################

@login_test
def overview(request):
    base_url = reverse('alert-common')  # 1 /alert-common/
    query_string = urlencode({'id_alert': 6})  # 2 id_alert=1
    url = '{}?{}'.format(base_url, query_string)  # 3 /alert-common/?id_alert=1
    return redirect(url)  # 4

    alert = ""
    if not request.session['db_id_session']:
        alert = 'Please select session'
    # data for main overview
    request.session['db_id_electrode'] = 0
    request.session['db_id_contact'] = 0
    request.session['db_electrode_name'] = ''
    request.session['db_contact_name'] = ''

    context = get_header_context(request, alert=alert)
    context_data = {
        'card_name': 'Export',
    }
    context.update(context_data)

    return render(request, 'mined_app/space/contact-overview.html', context)


@login_test
def structured(request):
    alert = ""
    if not request.session['db_id_mined']:
        alert = 'Please select subject'
    else:
        request.session['db_id_electrode'] = 0
        electrode = ElectrodeView.objects.filter(id_mined_subject=request.session['db_id_mined'])
        if len(electrode) > 0:
            request.session['db_id_electrode'] = electrode[0].id
    # for page header
    request.session['db_id_contact'] = 0
    request.session['db_electrode_name'] = ''
    request.session['db_contact_name'] = ''

    coordinate_systems = CoordinateSystem.objects.all().order_by('system_name')
    coordinates = list()
    for coordinate in coordinate_systems:
        coordinate_str = coordinate.system_name
        if coordinate.system_unit:
            coordinate_str = coordinate_str + ' [' + coordinate.system_unit + ']'
        coordinates.append(coordinate_str)

    atlas = AtlasType.objects.all().order_by('type_name')

    context = get_header_context(request, alert=alert)
    context_data = {
        'card_name': 'Contact',
        'atlas': atlas,
        'coordinates': coordinates,
    }
    context.update(context_data)
    return render(request, 'mined_app/space/contact-structured.html', context)


@login_test
def detail(request):
    record = list()
    alert = ""
    if request.session['db_id_contact'] and request.session['db_id_session']:
        record = ContactView.objects.get(id=request.session['db_id_contact'])
    else:
        alert = 'Please select contact'

    context = get_header_context(request, alert=alert)
    context_data = {
        'detail_contact': record,  # name is used by available detail
    }
    context.update(context_data)
    return render(request, 'mined_app/space/contact-detail.html', context)


@login_test
def edit(request):
    # Contact Name: Impedance: Type: Size name: Anatomy  Atlas: Structure: Lobe: Hemisphere: Brain area:
    # Coordinate Name: ( mel by byt svazany s Unit, n dve separatni pole) Axis x: Axis y: Axis z:
    #
    # actual value is loaded from table row
    # contact_size - contact_value, contact_type, atlas_type, coordinate_system
    alert = ""
    if not request.session['db_id_electrode']:
        base_url = reverse('alert-common')  # 1 /alert-common/
        query_string = urlencode({'id_alert': 5})  # 2 id_alert=1
        url = '{}?{}'.format(base_url, query_string)  # 3 /alert-common/?id_alert=1
        return redirect(url)  # 4

    request.session['db_id_contact'] = 0
    request.session['db_contact_name'] = ''

    # catalogs data
    contact_types = ContactType.objects.all().order_by('type_name')
    contact_sizes = ContactSize.objects.all().order_by('size_name')
    contacts = list()
    for contact in contact_sizes:
        contact_str = contact.size_name
        if contact.size_value:
            contact_str = contact_str + ' [' + str(int(contact.size_value)) + ']'
        contacts.append(contact_str)

    coordinate_systems = CoordinateSystem.objects.all().order_by('system_name')
    coordinates = list()
    for coordinate in coordinate_systems:
        coordinate_str = coordinate.system_name
        if coordinate.system_unit:
            coordinate_str = coordinate_str + ' [' + coordinate.system_unit + ']'
        coordinates.append(coordinate_str)

    anatomies = ContactAnatomyView.objects.all().order_by('id_anatomy')
    areas = [anatomy.brain_area for anatomy in list(anatomies)]
    # unique
    areas = set(areas)
    atlas = AtlasType.objects.all().order_by('type_name')
    structures = AnatomyStructure.objects.all().order_by('structure_name')
    lobes = Lobe.objects.all().order_by('lobe_name')
    imagines = ImagingPathology.objects.all().order_by('pathology_name')

    context = get_header_context(request, alert=alert)
    context_data = {
        'card_name': 'Contact',
        # catalogs without JSON
        'contact_types': contact_types,
        'contact_sizes': contacts,
        'atlas': atlas,
        'coordinates': coordinates,
        'structures': structures,
        'lobes': lobes,
        'areas': areas,
        'imagines': imagines,
    }
    context.update(context_data)
    return render(request, 'mined_app/space/contact-edit.html', context)


@access(requested_permission='change_contact')
def save(request):
    print("contact_save")
    #
    # connection must be closed -- all running transaction in autocommit mode will be commit -- django db is too smart
    # client is reconnect automatically
    connections.close_all()
    #transaction.set_autocommit(False)
    ret = 0
    message = ''
    try:
        with transaction.atomic():
            cur = connection.cursor()

            # space.update_contact(id_contact bigint, contact_name character varying, impedance double precision,
            # contact_size_name character varying, contact_size_value real, contact_type character varying,
            # imaging_pathology character VARYING OUT out_id_contact bigint)

            sql = "SELECT * FROM space.update_contact(%s"
            sql_val = [int(request.POST.get('id_contact'))]

            # Extract contact number
            contact_number = int(re.findall(r'\d+', request.POST.get('contact_name'))[0])
            sql = sql + ',%s'
            sql_val.append(contact_number)

            if request.POST.get('impedance') in ['', 'n/a']:
                sql = sql + ',null'
            else:
                sql = sql + ',%s'
                sql_val.append(float(request.POST.get('impedance')))

            sql = sql + ',%s'
            sql_val.append(request.POST.get('size_name').split(' ')[0])

            if len(re.findall(r'\[([^$]*)\]', request.POST.get('size_name'))) > 0:
                size_value = re.findall(r'\[([^$]*)\]', request.POST.get('size_name'))[0]
                sql = sql + ',%s'
                sql_val.append(int(size_value))
            else:
                sql = sql + ',null'

            sql = sql + ',%s'
            sql_val.append(request.POST.get('type'))

            sql = sql + ',%s'
            sql_val.append(int(request.session['db_id_electrode']))

            sql = sql + ',%s'
            sql_val.append(request.POST.get('imaging'))

            sql = sql + ')'
            print('sql', sql, sql_val)

            cur.execute(sql, sql_val)
            contact = cur.fetchall()
            if contact[0][0] < 1:
                message = str(contact[0][0])
                raise Error

            # space.update_contact_coordinate(id_contact bigint, cx real, cy real, cz real, system_name character varying,
            # system_unit character varying, OUT out_id_contact bigint)

            if len(request.POST.get('cx')) != 0 and len(request.POST.get('cy')) != 0 and len(request.POST.get('cz')) != 0:
                sql = "SELECT * FROM space.update_contact_coordinate(%s"
                sql_val = [int(request.POST.get('id_contact'))]

                sql = sql + ',%s'
                sql_val.append(float(request.POST.get('cx')))

                sql = sql + ',%s'
                sql_val.append(float(request.POST.get('cy')))

                sql = sql + ',%s'
                sql_val.append(float(request.POST.get('cz')))

                sql = sql + ',%s'
                sql_val.append(request.POST.get('coordinate').split(' ')[0])
                if len(re.findall(r'\[([^$]*)\]', request.POST.get('coordinate'))) > 0:
                    system_unit = re.findall(r'\[([^$]*)\]', request.POST.get('coordinate'))[0]
                    sql = sql + ',%s'
                    sql_val.append(system_unit)
                else:
                    sql = sql + ',null'

                sql = sql + ')'
                print('sql', sql, sql_val)

                cur.execute(sql, sql_val)
                contact = cur.fetchall()
                if contact[0][0] < 1:
                    message = str(contact[0][0])
                    raise Error

            # space.update_contact_anatomy(id_contact bigint, anatomy_structure character varying, lobe character varying,
            # hemisphere boolean, brain_area integer, manual boolean, atlas_type character varying,
            # OUT  out_id_anatomy  integer)

            sql = "SELECT * FROM space.update_contact_anatomy(%s"
            sql_val = [int(request.POST.get('id_contact'))]

            sql = sql + ',%s'
            sql_val.append(request.POST.get('structure'))

            sql = sql + ',%s'
            sql_val.append(request.POST.get('lobe'))

            sql = sql + ',%s'
            hemisphere = request.POST.get('hemisphere')
            if hemisphere == 'left':
                sql_val.append(True)
            else:
                sql_val.append(False)

            sql = sql + ',%s'
            if len(request.POST.get('brain')) > 0:
                brain = int(request.POST.get('brain'))
                sql_val.append(brain)
            else:
                brain = -1
                sql_val.append(brain)

            sql = sql + ',%s'
            manual = request.POST.get('manual')
            if manual == 'True':
                sql_val.append(True)
            else:
                sql_val.append(False)

            sql = sql + ',%s'
            sql_val.append(request.POST.get('atlas'))

            sql = sql + ')'
            print('sql', sql, sql_val)

            cur.execute(sql, sql_val)
            contact_anatomy = cur.fetchall()
            if contact_anatomy[0][0] < -1:  # -1 is unique
                message = str(contact[0][0])
                raise Error

    except IntegrityError:
        return 0, ""

    except Error as e:
        print(e)
        message = "update contact error " + e.args[0] + message
        ret = 1
        transaction.rollback()
    else:
        print('transaction commit\r\n')
        transaction.commit()
    # finally:
    #     transaction.set_autocommit(True)

    return ret, message


####################################
#
# C O N T A C T     AJAX data
#
####################################


def ajax_data(request):
    if request.method == 'POST':
        # NOTE: the key in QueryDict here is 'regions[]' so we have to use getlist('regions'), this is a jQuery thing
        # For details see: https://stackoverflow.com/questions/11190070/django-getlist
        # For export in XLS
        coordinate_name = request.POST.get('coordinate_sys').split(' ')[0]
        coordinate_unit = re.findall(r'\[([^$]*)\]', request.POST.get('coordinate_sys'))[0]

        if 'db_id_electrode[]' in request.session:
            electrode_list = request.session['db_id_electrode[]']
            del request.session['db_id_electrode[]']
            record_list = list(ContactView.objects.filter(id_electrode__in=electrode_list,
                                                          id_mined_subject=request.session['db_id_mined'],
                                                          coordinate_name=coordinate_name,
                                                          coordinate_unit=coordinate_unit,
                                                          atlas_name=request.POST.get('atlas'),))
            if len(record_list) == 0:
                record_list = list(ContactView.objects.filter(id_electrode__in=electrode_list,
                                                              id_mined_subject=request.session['db_id_mined'], ))

        else:
            record_list = list(ContactView.objects.filter(id_electrode=request.session['db_id_electrode'],
                                                          id_mined_subject=request.session['db_id_mined'],
                                                          coordinate_name=coordinate_name,
                                                          coordinate_unit=coordinate_unit,
                                                          atlas_name=request.POST.get('atlas'),))
            if len(record_list) == 0:
                record_list = list(ContactView.objects.filter(id_electrode=request.session['db_id_electrode'],
                                                              id_mined_subject=request.session['db_id_mined'], ))

        for record in record_list:
            if record.brain_area == -1:
                record.brain_area = ''
            record.hemisphere = 'right'
            if record.hemisphere:
                record.hemisphere = 'left'
            if record.impedance == -1:
                record.impedance = 'n/a'
        try:
            for record in record_list:
                if is_nan(record.c_x):
                    record.c_x = ''
                if is_nan(record.c_y):
                    record.c_y = ''
                if is_nan(record.c_z):
                    record.c_z = ''

        except Exception as e:
            return

        data = {
            'data': [[
                record.id,  # id_contact
                record.electrode_name + str(record.contact_number),
                record.type_name,
                record.structure_name,
                record.pathology_name,
                str(record.hemisphere),
                record.atlas_name,
                record.coordinate_name,
                record.coordinate_unit,
                record.c_x,
                record.c_y,
                record.c_z,
                record.impedance,
                record.size_name,
                record.size_value,
                record.lobe_name,
                record.brain_area,
                str(record.manual_assign),
            ] for record in record_list]
        }

        return JsonResponse(data, safe=False)


def ajax_export(request):
    if request.method == 'POST':
        # NOTE: the key in QueryDict here is 'regions[]' so we have to use getlist('regions'), this is a jQuery thing
        # For details see: https://stackoverflow.com/questions/11190070/django-getlist
        # For export in XLS
        print('Record ajax full view called')
        print(request.session['db_id_subject'])
        print('id_electrode', request.session['db_id_electrode'])

        record_list = list(ContactView.objects.filter(id_session=request.session['db_id_session']))
        for record in record_list:
            if record.brain_area == -1:
                record.brain_area = ''

        data = {
            'data': [[
                record.id_contact,
                record.contact_name,
                record.impedance,
                record.acquisition_number,
                record.contact_type,
                record.contact_size,
                record.contact_size_value,
                record.structure_name,
                record.lobe_name,
                str(record.hemisphere),
                record.brain_area,
                str(record.manual_assign),
                record.atlas_name,
                record.system_name,
                record.system_unit,
                record.c_x,
                record.c_y,
                record.c_z,
                record.reference_name,
                record.electrode_name,
                record.electrode_manuf,
                record.contact_material,
                record.electrode_type,
                record.macro_contact_distance,
                record.micro_contact_distance,
                record.dimension_a,
                record.dimension_b,
            ] for record in record_list]
        }
        return JsonResponse(data)


def ajax_search_data(request):
    if request.method == 'POST':
        if request.POST.get('q'):
            contacts_db = list(
                ContactView.objects.filter(id_electrode=request.session['db_id_electrode'],
                                           #id_session=request.session['db_id_session'],
                                           contact_number=request.POST.get('q')).order_by(
                    'id').values('contact_number', 'id'))
        else:
            contacts_db = list(
                ContactView.objects.filter(id_electrode=request.session['db_id_electrode'],
                                           #id_session=request.session['db_id_session']
                                           ).order_by(
                    'id').values('contact_number', 'id'))

        contact_list = list()
        for contact_db in contacts_db:
            contact = {
                'contact_name': request.session['db_electrode_name'] + str(contact_db.get('contact_number')),
                'id': contact_db.get('id'),
            }
            contact_list.append(contact)

        return JsonResponse(contact_list, safe=False)


def ajax_update(request):
    if request.method == 'POST':
        success, message = save(request)
        data = {
            'data': success,
            'message': message
        }
        return JsonResponse(data)


def ajax_select(request):
    if request.method == 'POST':
        request.session['db_id_contact'] = request.POST.get('selected')
        contacts = ContactView.objects.filter(id=request.session['db_id_contact']).values('contact_number', 'id')
        request.session['db_contact_number'] = contacts[0].get('id')
        request.session['db_contact_name'] = request.session['db_electrode_name'] + \
                                             str(contacts[0].get('contact_number'))

        data = {
            'data': [
                request.session['db_id_contact'],
                request.session['db_id_electrode'],
                request.session['db_contact_name'],
                request.session['db_electrode_name'],
            ]
        }
        return JsonResponse(data)


def ajax_plotly_lobe(request):
    if request.method == 'POST':
        lobes = Lobe.objects.all()
        lobe_counts = list()
        lobe_names = list()
        for lobe in lobes:
            count = AnatomyView.objects.filter(lobe_name__contains=lobe.lobe_name).count()
            lobe_counts.append(count)
            lobe_names.append(lobe.lobe_name)
        plot_data = {'v': lobe_counts}
        plot_labels = {'l': lobe_names}
        plot_data.update(plot_labels)
        data = {'data': plot_data}
        return JsonResponse(data)
