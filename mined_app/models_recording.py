# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AnnotationSessionType(models.Model):
    id = models.BigAutoField(primary_key=True)
    type_name = models.CharField(max_length=1024, blank=False, null=False)
    subtype_name = models.CharField(max_length=1024, blank=False, null=False)

    class Meta:
        managed = False
        db_table = 'annotation_session_type'


class Modality(models.Model):
    id = models.AutoField(primary_key=True)
    modality_name = models.CharField(unique=True, max_length=1024)

    class Meta:
        managed = False
        db_table = 'modality'


class Session(models.Model):
    id = models.BigAutoField(primary_key=True)
    dt_start_utc = models.DateTimeField()
    dt_stop_utc = models.DateTimeField()
    acquisition_device = models.CharField(max_length=128, blank=True, null=True)
    ip_address = models.CharField(max_length=64, blank=True, null=True)
    session_name = models.CharField(max_length=1024)
    note = models.CharField(max_length=2018, blank=True, null=True)
    id_session_type = models.ForeignKey('SessionType', models.DO_NOTHING, db_column='id_session_type')
    id_modality = models.ForeignKey(Modality, models.DO_NOTHING, db_column='id_modality')
    id_recording_reference = models.IntegerField()
    id_mined_subject = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'session'
        unique_together = (('dt_start_utc', 'id_mined_subject'),)


class SessionContact(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_session = models.ForeignKey(Session, models.DO_NOTHING, db_column='id_session', blank=True, null=True)
    id_contact = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'session_contact'
        unique_together = (('id_session', 'id_contact'),)


class SessionType(models.Model):
    id = models.BigAutoField(primary_key=True)
    type_name = models.CharField(unique=True, max_length=1024)

    class Meta:
        managed = False
        db_table = 'session_type'


class SessionView(models.Model):
    id = models.BigAutoField(primary_key=True)
    session_name = models.CharField(max_length=1024, blank=True, null=True)
    dt_start_utc = models.DateTimeField(blank=True, null=True)
    dt_stop_utc = models.DateTimeField(blank=True, null=True)
    acquisition_device = models.CharField(max_length=128, blank=True, null=True)
    ip_address = models.CharField(max_length=64, blank=True, null=True)
    modality_name = models.CharField(max_length=1024, blank=True, null=True)
    type_name = models.CharField(max_length=1024, blank=True, null=True)
    reference_name = models.CharField(max_length=1024, blank=True, null=True)
    id_subject = models.IntegerField(blank=True, null=True)
    id_mined = models.IntegerField(blank=True, null=True)
    trial_count = models.BigIntegerField(blank=True, null=True)
    zone_name = models.CharField(max_length=512, blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'session_view'


class AnnotationSessionView(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_session = models.IntegerField(blank=True, null=True)
    event_start_utc = models.DateTimeField(blank=True, null=True)
    event_stop_utc = models.DateTimeField(blank=True, null=True)
    annotation_text = models.CharField(max_length=2048, blank=True, null=True)
    type_name = models.CharField(max_length=1024, blank=False, null=False)
    subtype_name = models.CharField(max_length=1024, blank=False, null=False)
    zone_name = models.CharField(max_length=512, blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'annotation_session_view'


class Task(models.Model):
    id = models.BigAutoField(primary_key=True)
    task_name = models.CharField(unique=True, max_length=1024)
    task_description = models.CharField(max_length=1024, blank=True, null=True)
    patient_instructions = models.CharField(max_length=1024, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'task'


class Trial(models.Model):
    id = models.BigAutoField(primary_key=True)
    onset = models.IntegerField()
    duration = models.IntegerField()
    response_time = models.IntegerField(blank=True, null=True)
    response = models.CharField(max_length=64, blank=True, null=True)
    ttl_value = models.IntegerField(blank=True, null=True)
    id_trial_type = models.IntegerField(blank=True, null=True)

#    id_trial_type = models.ForeignKey('TrialType', models.DO_NOTHING, db_column='id_trial_type')
    id_session = models.ForeignKey(Session, models.DO_NOTHING, db_column='id_session')

    class Meta:
        managed = False
        db_table = 'trial'
        unique_together = (('onset', 'id_trial_type', 'id_session'),)


class TrialType(models.Model):
    id = models.BigAutoField(primary_key=True)
    type_name = models.CharField(max_length=1024)
    id_task = models.ForeignKey(Task, models.DO_NOTHING, db_column='id_task')

    class Meta:
        managed = False
        db_table = 'trial_type'
        unique_together = (('type_name', 'id_task'),)


class TrialView(models.Model):
    id = models.AutoField(primary_key=True)
    id_session = models.BigIntegerField(blank=True, null=True)
    onset = models.IntegerField(blank=True, null=True)
    duration = models.IntegerField(blank=True, null=True)
    response_time = models.IntegerField(blank=True, null=True)
    response = models.CharField(max_length=64, blank=True, null=True)
    ttl_value = models.IntegerField(blank=True, null=True)
    trial_type = models.CharField(max_length=1024, blank=True, null=True)
    task_name = models.CharField(max_length=1024, blank=True, null=True)
    task_description = models.CharField(max_length=1024, blank=True, null=True)
    patient_instructions = models.CharField(max_length=1024, blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'trial_view'
