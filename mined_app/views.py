from django.contrib import auth
from django.contrib.auth.models import User
from django.http import JsonResponse

from django.shortcuts import redirect

# from .ldap import ldap_auth, ldap_modify_password
from .models_demographic import Subject
from .models_mined_user import UserView
from .models_recording import Session
from .models_signal import Channel
from .models_space import Contact
from .views_common import *
from mined.settings import SETTING_AUTH


def index(request):
    return redirect('login')



def login(request):
    user_id = 0
    request.session["id_user"] = -1
    if request.POST:
        username_req = request.POST['username'].replace(" ", "")
        password_req = request.POST['password']

        user = auth.authenticate(username=username_req, password=password_req)
        if user is None:
            context = {
                'not_auth': '<div><h6 class="text-danger">User name or password</h6><h6 class="text-danger">is incorrect </h6></div>',
            }
            return render(request, 'mined_app/login.html', context)

        request.session.set_expiry(86400)  # 86400 24 hours days sets the exp. value of the session
        auth.login(request, user)  ######## test
        request.session['last_touch'] = datetime.now()

        if SETTING_AUTH == "LDAP":
            from .ldap import ldap_auth
            # get user_id from LDAP sever
            ldap_id = ldap_auth(username_req, password_req)
            if ldap_id > 0:
                user_id = ldap_id - 1000
        else:
            user_id = user.id
        return redirect(set_global_session_redirect(request, user_id))
    #            return redirect('subject-overview')

    return render(request, 'mined_app/login.html')


def set_global_session_redirect(request, user_id):
    if SETTING_AUTH == "LDAP":
        mined_user = list(UserView.objects.filter(id=user_id))      # in this case is not dependency on django auth-user
    else:
        # mined_user = list(UserView.objects.filter(id_auth=user_id))     # with django auth-user
        mined_user = list(User.objects.filter(id=user_id))
        mined_user_groups = list(request.user.groups.values_list('name', flat=True))

    if len(mined_user) == 0:
        return 'login'

    # User information
    request.session["id_user"] = mined_user[0].id
    request.session['first_name'] = mined_user[0].first_name
    request.session['last_name'] = mined_user[0].last_name
    request.session['user_groups'] = ', '.join(mined_user_groups)

    # request.session['name'] = mined_user[0].full_name
    # request.session['id_access'] = mined_user[0].id_access
    # request.session['access'] = mined_user[0].description

    print(request.user.get_all_permissions())

    # Mined information
    request.session['db_id_institution'] = 0
    request.session['institution_name'] = None
    request.session['timezone_name'] = ''
    request.session['db_id_mined'] = 0
    request.session['db_id_subject'] = 0
    request.session['db_id_session'] = 0
    request.session['db_id_electrode'] = 0
    request.session['db_id_contact'] = 0
    request.session['db_session_count'] = 0
    request.session['db_subject_count'] = 0
    request.session['db_contact_count'] = 0
    request.session['db_electrode_count'] = 0
    request.session['db_session_name'] = ''
    request.session['db_contact_name'] = ''
    request.session['db_electrode_name'] = ''
    request.session['static_warning'] = ""
    request.session['timezone'] = 0
    request.session['patient_filter'] = {'histo': '', 'inter': '', 'ilae': '', 'engel': ''}

    #    institution = InstitutionView.objects.get(id=request.session['db_id_institution'])
    os_timezone_get = open('/etc/timezone', 'r')
    os_timezone = os_timezone_get.readline()[:-1]
    request.session['os_timezone'] = os_timezone
    request.session['db_subject_count'] = Subject.objects.all().count()
    request.session['institution_timezone'] = ""
    # if os_timezone != mined_user[0].zone_name:
    #     request.session['timezone'] = 1
    #     request.session['institution_timezone'] = mined_user[0].zone_name
    if user_id == 1:
        return 'user-edit'

    return 'dashboard'


#    return render(request, 'mined_app/demographic/subject-overview.html', context)


def logout(request):
    # auth.logout(request)
    del (request.session["id_user"])
    return redirect('login')


@login_test
def dashboard(request):
    subject_count = Subject.objects.all().count()
    histo_count = SubjectHistopathologyView.objects.all().count()
    inter_count = SubjectInterventionView.objects.all().count()
    outcome_count = SubjectOutcomeView.objects.all().count()
    session_count = Session.objects.all().count()
    eeg_count = SessionView.objects.filter(modality_name='EEG').count()
    ieeg_count = SessionView.objects.filter(modality_name='iEEG').count()
    contact_count = Contact.objects.all().count()
    channel_count = Channel.objects.all().count()
    request.session['patient_filter'] = {'histo': '', 'inter': '', 'ilae': '', 'engel': ''}

    context = get_header_context(request)
    context_data = {
        'subject_count': subject_count,
        'histo_count': histo_count,
        'inter_count': inter_count,
        'outcome_count': outcome_count,
        'rec_count': session_count,
        'eeg_count': eeg_count,
        'ieeg_count': ieeg_count,
        'contact_count': contact_count,
        'channel_count': channel_count,

    }
    context.update(context_data)

    return render(request, 'mined_app/dashboard.html', context)


def dashboard_select(request):
    if request.method == 'POST':
        data = {'data': ''}
        return JsonResponse(data)


