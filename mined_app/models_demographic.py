# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class EngelScale(models.Model):
    id = models.AutoField(primary_key=True)
    scale_name = models.CharField(unique=True, max_length=32)

    class Meta:
        managed = False
        db_table = 'engel_scale'


class Handedness(models.Model):
    id = models.SmallIntegerField(primary_key=True)
    handedness_name = models.CharField(max_length=512)

    class Meta:
        managed = False
        db_table = 'handedness'


class Histopathology(models.Model):
    id = models.AutoField(primary_key=True)
    histopathology_type = models.CharField(unique=True, max_length=1024)
    histopathology_subtype = models.CharField(unique=True, max_length=1024)

    class Meta:
        managed = False
        db_table = 'histopathology'


class IlaeScale(models.Model):
    id = models.AutoField(primary_key=True)
    scale_name = models.CharField(unique=True, max_length=32, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ilae_scale'


class Institution(models.Model):
    id = models.AutoField(primary_key=True)
    institution_name = models.CharField(unique=True, max_length=1024)
    id_time_zone = models.ForeignKey('TimeZone', models.DO_NOTHING, db_column='id_time_zone')

    class Meta:
        managed = False
        db_table = 'institution'


class InstitutionView(models.Model):
    id = models.AutoField(primary_key=True)
    institution_name = models.CharField(max_length=1024, blank=True, null=True)
    zone_name = models.CharField(max_length=512, blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = u'demographic"."institution_view'


class Intervention(models.Model):
    id = models.AutoField(primary_key=True)
    intervention_name = models.CharField(unique=True, max_length=1024)

    class Meta:
        managed = False
        db_table = 'intervention'


class MchughScale(models.Model):
    id = models.AutoField(primary_key=True)
    scale_name = models.CharField(unique=True, max_length=32, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mchugh_scale'


class Sex(models.Model):
    id = models.SmallIntegerField(primary_key=True)
    sex_name = models.CharField(max_length=512, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sex'


class Subject(models.Model):
    id_mined = models.AutoField(primary_key=True)
    dt_birth = models.DateField(blank=True, null=True)
    note = models.CharField(max_length=1024, blank=True, null=True)
    id_subject = models.IntegerField()
    id_institution = models.ForeignKey(Institution, models.DO_NOTHING, db_column='id_institution')
    seizure_onset_age = models.FloatField(blank=True, null=True)
    id_sex = models.ForeignKey(Sex, models.DO_NOTHING, db_column='id_sex')
    id_subject_type = models.ForeignKey('SubjectType', models.DO_NOTHING, db_column='id_subject_type')
    id_handedness = models.ForeignKey(Handedness, models.DO_NOTHING, db_column='id_handedness')

    class Meta:
        managed = False
        db_table = 'subject'
        unique_together = (('id_subject', 'id_institution'),)


class SubjectBasicView(models.Model):
    id_mined = models.AutoField(primary_key=True)
    id_institution = models.ForeignKey(Institution, models.DO_NOTHING, db_column='id_institution')
    histopathology_name = models.CharField(max_length=1024, blank=True, null=True)
    intervention_name = models.CharField(max_length=1024, blank=True, null=True)
    engel_name = models.CharField(max_length=1024, blank=True, null=True)
    ilae_name = models.CharField(max_length=1024, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'subject_basic_view'


class SubjectHistopathologies(models.Model):
    id = models.AutoField(primary_key=True)
    dt_date_utc = models.DateTimeField()
    id_histopathology = models.ForeignKey(Histopathology, models.DO_NOTHING, db_column='id_histopathology', blank=True,
                                          null=True)
    id_mined_subject = models.ForeignKey(Subject, models.DO_NOTHING, db_column='id_mined_subject', blank=True,
                                         null=True)

    class Meta:
        managed = False
        db_table = 'subject_histopathologies'
        unique_together = (('dt_date_utc', 'id_histopathology', 'id_mined_subject'),)


class SubjectHistopathologyView(models.Model):
    id = models.AutoField(primary_key=True)
    id_mined_subject = models.IntegerField(blank=True, null=True)
    id_histopathology = models.IntegerField(blank=True, null=True)
    histopathology_type = models.CharField(max_length=1024, blank=True, null=True)
    histopathology_subtype = models.CharField(max_length=1024, blank=True, null=True)
    dt_date_utc = models.DateTimeField(blank=True, null=True)
    zone_name = models.CharField(max_length=512, blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'subject_histopathology_view'


class SubjectInterventionView(models.Model):
    id_subject_intervention = models.AutoField(primary_key=True)
    id_mined_subject = models.IntegerField(blank=True, null=True)
    intervention_name = models.CharField(max_length=1024, blank=True, null=True)
    dt_date_utc = models.DateTimeField(blank=True, null=True)
    zone_name = models.CharField(max_length=512, blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'subject_intervention_view'


class SubjectInterventions(models.Model):
    id = models.AutoField(primary_key=True)
    dt_date_utc = models.DateTimeField()
    id_intervention = models.ForeignKey(Intervention, models.DO_NOTHING, db_column='id_intervention')
    id_mined_subject = models.ForeignKey(Subject, models.DO_NOTHING, db_column='id_mined_subject')

    class Meta:
        managed = False
        db_table = 'subject_interventions'
        unique_together = (('dt_date_utc', 'id_intervention', 'id_mined_subject'),)


class SubjectOutcomeView(models.Model):
    id_mined_subject = models.AutoField(primary_key=True)
    dt_date_utc = models.DateTimeField(blank=True, null=True)
    ilae = models.CharField(max_length=32, blank=True, null=True)
    id = models.IntegerField(blank=True, null=True)
    engel = models.CharField(max_length=32, blank=True, null=True)
    mchugh = models.CharField(max_length=32, blank=True, null=True)
    zone_name = models.CharField(max_length=512, blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'subject_outcome_view'


class SubjectOutcomes(models.Model):
    id = models.AutoField(primary_key=True)
    dt_date_utc = models.DateTimeField()
    id_ilae_scale = models.ForeignKey(IlaeScale, models.DO_NOTHING, db_column='id_ilae_scale', blank=True, null=True)
    id_engel_scale = models.ForeignKey(EngelScale, models.DO_NOTHING, db_column='id_engel_scale', blank=True, null=True)
    id_mined_subject = models.ForeignKey(Subject, models.DO_NOTHING, db_column='id_mined_subject', blank=True,
                                         null=True)

    class Meta:
        managed = False
        db_table = 'subject_outcomes'
        unique_together = (('dt_date_utc', 'id_mined_subject'),)


class SubjectType(models.Model):
    id = models.AutoField(primary_key=True)
    type_name = models.CharField(unique=True, max_length=1024, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'subject_type'


class SubjectView(models.Model):
    id_subject = models.AutoField(primary_key=True)
    sex_name = models.CharField(max_length=512, blank=True, null=True)
    dt_birth = models.DateField(blank=True, null=True)
    handedness_name = models.CharField(max_length=1024, blank=True, null=True)
    note = models.CharField(max_length=1024, blank=True, null=True)
    id_mined = models.IntegerField(blank=True, null=True)
    seizure_onset_age = models.FloatField(blank=True, null=True)
    institution_name = models.CharField(max_length=1024, blank=True, null=True)
    type_name = models.CharField(max_length=1024, blank=True, null=True)
    intervention_count = models.BigIntegerField(blank=True, null=True)
    histopathology_count = models.BigIntegerField(blank=True, null=True)
    outcome_count = models.BigIntegerField(blank=True, null=True)
    id_institution = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'subject_view'


class TimeZone(models.Model):
    id = models.AutoField(primary_key=True)
    zone_name = models.CharField(unique=True, max_length=512)

    class Meta:
        managed = False
        db_table = 'time_zone'
