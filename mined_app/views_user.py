from django.contrib.auth.models import User
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.db import connection, transaction, Error, connections

from mined.settings import SETTING_AUTH
from mined_app.models_demographic import TimeZone, Institution
from mined_app.models_mined_user import UserView, Access, MinedUser
from mined_app.views_common import get_header_context, login_test

if SETTING_AUTH == "DJANGO":
    from django.contrib.auth.models import User


@login_test
def change_password(request):
    context = get_header_context(request)

    if request.POST:
        alert = 'Change password error, please retype all'
        username = request.POST['username'].replace(" ", "")
        password = request.POST['password']
        password_new = request.POST['password_new']
        if len(password_new) < 8:
            alert = 'Password length is too small'
            context = get_header_context(request, alert=alert)
            return render(request, 'mined_app/users/change-password.html', context)

        password_retype = request.POST['password_retype']
        if password_retype == password_new:
            try:
                if SETTING_AUTH == "LDAP":
                    from .ldap import ldap_modify_password
                    ldap_modify_password(username, password, password_new)
                else:
                    u = User.objects.get(username=username)
                    u.set_password(password_new)
                    u.save()
            except:
                alert = 'User name or password is wrong'
                context = get_header_context(request, alert=alert)
                return render(request, 'mined_app/users/change-password.html', context)

            return redirect('login')
        else:
            context = get_header_context(request, alert=alert)

    return render(request, 'mined_app/users/change-password.html', context)


@login_test
def user_edit(request):
    context = get_header_context(request)
    institutions = Institution.objects.all()
    accesses = Access.objects.all()
    context_data = {
        'settings_auth': SETTING_AUTH,
        'institutions': institutions,
        'accesses': accesses,
    }
    context.update(context_data)
    return render(request, 'mined_app/users/user-edit.html', context)


@login_test
def institution_edit(request):
    context = get_header_context(request)
    time_zone = TimeZone.objects.all()
    context_data = {
        'zones': time_zone,  # name is used by available detail
    }
    context.update(context_data)
    return render(request, 'mined_app/users/institution-edit.html', context)


def ajax_institution_save(request):
    if request.method == 'POST':
        try:
            with connection.cursor() as cur:
                cur.execute("""
                            SELECT * FROM demographic.save_institution(%s,%s,%s);
                        """,
                            (int(request.POST.get("id_inst")), request.POST.get("zone"), request.POST.get("name")))

                id_data = cur.fetchone()[0]

            transaction.commit()
            data = {'data': (0, 'ok')}

        except (Exception, Error) as error:
            print("Error  PostgreSQL", error.args[0])
            transaction.rollback()
            data = {'data': (-1, error.args[0])}
        return JsonResponse(data)


def ajax_institution_delete(request):
    if request.method == 'POST':
        try:
            with connection.cursor() as cur:
                cur.execute("""
                            DELETE FROM demographic.institution WHERE demographic.institution.id = %s;
                        """,
                            (int(request.POST.get("id_inst")),))

            transaction.commit()
            data = {'data': (0, 'ok')}

        except (Exception, Error) as error:
            print("Error  PostgreSQL", error.args[0])
            transaction.rollback()
            data = {'data': (-1, error.args[0])}
        return JsonResponse(data)


def ajax_user_data(request):
    if request.method == 'POST':
        # record_list = list(UserView.objects.all())
        record_list = list(User.objects.all())
        data = {
            'data': [[
                record.id,
                record.username,
                record.first_name,
                record.last_name,
                record.email,
                record.is_active,
            ] for record in record_list]
        }
        return JsonResponse(data)


def ajax_user_insert(request):
    if request.method == 'POST':
        username = request.POST['user_name'].replace(" ", "")
        password = request.POST['password']
        if len(password) < 8:
            data = {'data': (-1, 'Password length is too small')}
            return JsonResponse(data)
        if password == request.POST['password_re']:
            if SETTING_AUTH == "DJANGO":
                from django.contrib.auth.models import User
                user = User.objects.create_user(username=username,
                                                password=password)
                if user is None:
                    data = {'data': (-1, 'django user not created - user name is used')}
                    return JsonResponse(data)
                else:
                    code, error = user_save(request, user.id)
                    data = {'data': (code, error)}
                    return JsonResponse(data)
            else:
                from .ldap import ldap_add_user
                # in this case is not dependency on django auth-user
                # id_auth is last MinedUser id + 1 -> next
                users = list(MinedUser.objects.all().order_by('id'))
                code, id_auth = user_save(request, users[len(users) - 1].id + 1)
                if code == -1:
                    data = {'data': (code, "Database error:" + id_auth)}
                    return JsonResponse(data)
                code, error = ldap_add_user(request.POST['LDAP_Password'], username, password,
                                            request.POST['full_name'], id_auth)
                if code == -1:
                    # user name is used - e.g. user is not created so user must be deleted from mined_user table
                    delete_user_mined_user(id_auth)
                    data = {'data': (code, "LDAP error user name is used:" + error)}
                else:
                    data = {'data': (0, "ok")}

                return JsonResponse(data)


def ajax_user_update(request):
    if request.method == 'POST':
        code, error = user_save(request, int(request.POST.get("id_user")))

        data = {'data': (code, error)}
        return JsonResponse(data)


def user_save(request, id_auth):
    try:
        with connection.cursor() as cur:
            cur.execute("""
                        SELECT * FROM mined_user.save_user(%s,%s,%s,%s,%s,%s);
                    """,
                        (int(request.POST.get("id_user")), request.POST.get("full_name"),
                         request.POST.get("institution"), request.POST.get("contact"), request.POST.get("access"),
                         id_auth))

            id_data = cur.fetchone()[0]

        transaction.commit()
        return 0, id_data

    except (Exception, Error) as error:
        print("Error  PostgreSQL", error.args[0])
        transaction.rollback()
        return -1, error.args[0]


def ajax_user_delete(request):
    if request.method == 'POST':

        if SETTING_AUTH == "DJANGO":
            from django.contrib.auth.models import User
            try:
                user = User.objects.filter(id=int(request.POST.get("id_user")))
                user.delete()
            except (Exception, Error) as error:
                data = {'data': (-1, error.args[0])}
                return JsonResponse(data)
        else:
            from .ldap import ldap_delete_user
            code, error = ldap_delete_user(request.POST.get("LDAP_password"), user_name)

        code, error = delete_user_mined_user(int(request.POST.get("id_user")))
        data = {'data': (code, error)}
        return JsonResponse(data)


def delete_user_mined_user(id_user):
    try:
        with connection.cursor() as cur:
            cur.execute("""
                        DELETE FROM mined_user.mined_user WHERE mined_user.mined_user.id = %s;
                    """,
                        (id_user,))

        transaction.commit()
        return 0, "ok"

    except (Exception, Error) as error:
        print("Error  PostgreSQL", error.args[0])
        transaction.rollback()
        return -1, error.args[0]
