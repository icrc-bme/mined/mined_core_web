from django.db import connection, Error, IntegrityError
from django.http import JsonResponse
from django.shortcuts import render, reverse, redirect
from urllib.parse import urlencode

from .models_recording import SessionView, SessionType, Modality, TrialView, AnnotationSessionType, AnnotationSessionView
from .models_signal import ChannelContactView, SeizureType, SeizureChannelType, SeizureView, AnnotationChannelView, \
    RecordingReference
from .models_space import ElectrodeView
from .views_common import *


####################################
#
# S E S S I O N     pages
#
####################################
@login_test
def overview(request):
    alert = ""
    if not request.session['db_id_mined']:
        alert = 'Please select subject'
    request.session['db_contact_count'] = 0
    request.session['db_electrode_count'] = 0
    request.session['db_id_electrode'] = 0
    request.session['db_id_contact'] = 0
    #  request.session['db_session_name'] = ''
    request.session['db_contact_name'] = ''
    request.session['db_electrode_name'] = ''
    request.session['db_id_trial'] = 0
    request.session['db_id_annotation_channel'] = 0
    context = get_header_context(request, alert=alert)
    context_data = {
        'card_name': 'Session',
        'timezone': request.session['institution_timezone'],
    }
    context.update(context_data)
    return render(request, 'mined_app/recording/session-overview.html', context)


@login_test
def detail(request):
    detail_session = list()
    alert = ""
    if request.session['db_id_session']:
        detail_session = SessionView.objects.get(id=request.session['db_id_session'])
    else:
        alert = 'Please select session'

    context = get_header_context(request, alert=alert)
    context_data = {
        'detail_session': detail_session,
        'timezone': request.session['institution_timezone'],
    }
    context.update(context_data)
    return render(request, 'mined_app/recording/session-detail.html', context)


@login_test
def edit(request):
    # id session db record is needed for edit
    print("session edit")
    request.session['db_id_trial'] = 0
    request.session['db_id_annotation_channel'] = 0

    alert = ""
    actual_data = dict()
    if not request.session['db_id_session']:
        alert = 'Please select session'
    else:
        if request.method == 'POST':  # read only:
            print('post session_edit')
            success = save(request)
            if success <= 0:
                # internal error - return from SQL function
                alert = "System internal error --> session edit <-- id = %s " % success
        # actual data
        actual_data = SessionView.objects.get(id=request.session['db_id_session'])

    # catalogs data
    types = SessionType.objects.all().order_by('type_name')
    modalities = Modality.objects.all().order_by('modality_name')
    references = RecordingReference.objects.all().order_by('reference_name')

    context = get_header_context(request, alert=alert)
    context_data = {
        'card_name': 'Session',
        # catalogs without JSON
        'form_type': 1,
        'types': types,
        'modalities': modalities,
        'references': references,
        # actual data
        'actual_data': actual_data,
    }
    context.update(context_data)
    return render(request, 'mined_app/recording/session-edit.html', context)


@login_test
def session_annotation_edit(request):
    alert = ""
    # actual data
    if not request.session['db_id_session']:
        alert = 'Please select session'

    annotation_session_types = AnnotationSessionType.objects.order_by().values('type_name').distinct()
    annotation_session_subtypes = AnnotationSessionType.objects.order_by().values('subtype_name').distinct()

    # catalogs without JSON
    context = get_header_context(request, alert=alert)
    context_data = {
        'card_name': 'Session annotation',
        # catalogs without JSON
        'timezone': request.session['institution_timezone'],
        # data
        'annotation_session_types': annotation_session_types,
        'annotation_session_subtypes': annotation_session_subtypes
    }
    context.update(context_data)
    return render(request, 'mined_app/recording/session-annotation-edit.html', context)


@login_test
def trial_edit(request):
    base_url = reverse('alert-common')  # 1 /alert-common/
    query_string = urlencode({'id_alert': 6})  # 2 id_alert=1
    url = '{}?{}'.format(base_url, query_string)  # 3 /alert-common/?id_alert=1
    return redirect(url)  # 4

    alert = ""
    print("trial edit")
    if not request.session['db_id_session']:
        alert = 'Please select session'

    # catalogs data
    task_names = Task.objects.all().order_by('task_name')
    trial_types = TrialType.objects.all().order_by('type_name')

    context = get_header_context(request, alert=alert)
    context_data = {
        # catalogs without JSON
        'task_names': task_names,
        'trial_types': trial_types,
    }
    context.update(context_data)
    return render(request, 'mined_app/recording/trial-edit.html', context)


@access(requested_permission='change_session')
def save(request):
    if request.method == 'POST':
        print("inputNote")
        print(request.POST.get('inputNote'))
        with connection.cursor() as cur:
            try:

                # dbo.update_session(id_session integer, session_type character varying, device character varying,
                # modality character varying, reference character varying, note character varying,
                # OUT out_id_session integer)
                cur.callproc('recording.update_session',
                             [int(request.session['db_id_session']),
                              request.POST.get('name'),
                              request.POST.get('type'),
                              request.POST.get('device'),
                              request.POST.get('modality'),
                              request.POST.get('reference'),
                              request.POST.get('ip_address'),
                              request.POST.get('note')
                              ]
                             )
                session = cur.fetchall()
                if session[0][0] == -1:
                    return 1, "type not found"
                if session[0][0] == -2:
                    return 1, "modality not found"
                if session[0][0] == -3:
                    return 1, "refrence not found"

            except IntegrityError:
                return 0, ""

            except (Exception, Error) as error:
                return 1, error.args[0]

        return 0, ""


####################################
#
# S E S S I O N     AJAX data
#
####################################
def ajax_data(request):
    if request.method == 'POST':
        record_list = list(SessionView.objects.filter(id_mined=request.session['db_id_mined']))
        print(record_list)
        data = {
            'data': [[
                record.id,
                record.session_name,
                record.modality_name,
                record.type_name,
                record.dt_start_utc,
                record.dt_stop_utc,
                record.acquisition_device,
                record.ip_address,
                record.type_name,
                record.reference_name,
                record.trial_count,
            ] for record in record_list]
        }
        return JsonResponse(data)


def ajax_select(request):
    if request.method == 'POST':
        selected = request.POST.get('selected')
        request.session['db_id_session'] = selected
        request.session['db_contact_count'] = ContactView.objects.filter(id_mined_subject=request.session['db_id_mined']).count()
        record_list = list(ElectrodeView.objects.filter(id_mined_subject=request.session['db_id_mined']))

        # unique - it not exists relationship between session table and electrode table, only session_contact
        for idx in range(len(record_list) - 1, -1, -1):
            if record_list.count(record_list[idx]) > 1:
                del record_list[idx]
        request.session['db_session_name'] = SessionView.objects.get(id=selected).session_name
        request.session['db_electrode_count'] = len(record_list)

        request.session['db_id_contact'] = 0
        request.session['db_id_electrode'] = 0
        request.session['db_contact_name'] = ''
        request.session['db_electrode_name'] = ''

        data = {
            'data': [
                request.session['db_id_session'],
                request.session['db_id_electrode'],
                request.session['db_session_name'],
                request.session['db_contact_count'],
                request.session['db_electrode_count'],
            ]
        }
        return JsonResponse(data)


def ajax_search_data(request):
    if request.method == 'POST':
        print('session search data')
        print(request.POST)
        if request.POST.get('q'):
            query = request.POST.get('q')
            session_list = list(
                SessionView.objects.filter(id_mined=request.session['db_id_mined'],
                                           session_name__contains=request.POST.get('q')).order_by(
                    'id').values('session_name', 'id'))
            print("session list", session_list)

        else:
            session_list = list(
                SessionView.objects.filter(id_mined=request.session['db_id_mined']).order_by(
                    'id').values('session_name', 'id'))

        return JsonResponse(session_list, safe=False)


def ajax_session_update(request):
    if request.method == 'POST':
        success, message = save(request)
        data = {
            'data': success,
            'message': message
        }
        return JsonResponse(data)


def ajax_trial_data(request):
    print("task_data")
    if request.method == 'POST':
        trial_list = list(
            TrialView.objects.filter(id_session=request.session['db_id_session']))
        data = {
            'data': [[
                trial.id,
                trial.trial_name,
                trial.onset,
                trial.duration,
                trial.response_time,
                trial.response,
                trial.ttl_value,
                trial.task_name,
                trial.task_description,
                trial.patient_instructions,
            ] for trial in trial_list]
        }

        return JsonResponse(data)


def ajax_trial_select(request):
    if request.method == 'POST':
        selected = request.POST.get('selected')
        request.session['db_id_trial'] = selected

        data = {}
        return JsonResponse(data)


def ajax_session_annotation_subtype_select(request):
    if request.method == 'POST':
        annotation_type = request.POST.get('selected_type')
        annotation_session_subtypes = AnnotationSessionType.objects.filter(type_name=annotation_type).values('subtype_name').distinct()

        data = {
            'data': [x['subtype_name'] for x in annotation_session_subtypes]
        }

        return JsonResponse(data)



def ajax_session_annotation_data(request):
    # annotation session
    if request.method == 'POST':
        annotation_list = list(
            AnnotationSessionView.objects.filter(id_session=request.session['db_id_session']))

        for annotation in annotation_list:
            if annotation.event_start_utc:
                annotation.event_start_utc = convert_from_utc(annotation.event_start_utc, annotation.zone_name,
                                                              False)
                if request.session['timezone'] == 1:
                    annotation.event_start_utc = str(annotation.event_start_utc) + u'\t' + \
                                                 request.session['institution_timezone']

            if annotation.event_stop_utc:
                annotation.event_stop_utc = convert_from_utc(annotation.event_stop_utc, annotation.zone_name,
                                                             False)
                if request.session['timezone'] == 1:
                    annotation.event_stop_utc = str(annotation.event_stop_utc) + u'\t' + \
                                                request.session['institution_timezone']

        data = {
            'data': [[
                annotation.id,
                annotation.event_start_utc,
                annotation.event_stop_utc,
                annotation.annotation_text,
                annotation.type_name,
                annotation.subtype_name
            ] for annotation in annotation_list]
        }

        return JsonResponse(data)

@access(requested_permission='change_annotationsession')
def session_annotation_save(request):
    db_start = None
    db_stop = None
    if request.POST.get('start'):
        db_start = convert_to_utc(request.POST.get('start'), request.session['os_timezone'], False)
    if request.POST.get('stop'):
        db_stop = convert_to_utc(request.POST.get('stop'), request.session['os_timezone'], False)

    # recording.save_annotation_session(id_annotation_session integer, event_start_utc timestamp without time zone,
    # id_session integer, event_stop_utc timestamp without time zone, annotation_text character varying,
    # OUT id_annotation integer)

    with connection.cursor() as cur:
        try:
            cur.callproc('recording.save_annotation_session',
                         [int(request.POST.get('id')), db_start, int(request.session['db_id_session']), db_stop,
                          request.POST.get('text'), request.POST.get('annotation_type'), request.POST.get('annotation_subtype')])
            annotation = cur.fetchall()
            if annotation[0][0] < 1:
                return 1, "session annotation not found"

        except IntegrityError:
            return 0, ""

        except (Exception, Error) as error:
            print("internal", error.args[0])
            return 1, error.args[0]

    return 0, ""


def ajax_session_annotation_submit(request):
    if request.method == 'POST':
        success, message = session_annotation_save(request)
        data = {
            'data': success,
            'message': message
        }
        return JsonResponse(data)


def ajax_plotly_type(request):
    if request.method == 'POST':
        types = SessionType.objects.all()
        type_counts = list()
        type_names = list()
        for type in types:
            count = SessionView.objects.filter(type_name__contains=type.type_name).count()
            if count != 0:
                type_counts.append(count)
                type_names.append(type.type_name)
        plot_data = {'v': type_counts}
        plot_labels = {'l': type_names}
        plot_data.update(plot_labels)
        data = {'data': plot_data}
        return JsonResponse(data)
