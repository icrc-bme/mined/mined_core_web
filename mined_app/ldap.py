# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

###############################
#
#   ldap username and password to check
#   internal_api account e.g for desktop applications
#

import ldap3
import logging
from ldap3 import (
    HASHED_SALTED_SHA, MODIFY_REPLACE
)
from ldap3.utils.hashed import hashed

# from pprint import pprint


SERVER_URI = 'ldap://10.34.1.24'
SEARCH_BASE = 'dc=bme-archive,dc=cz'

module_logger = logging.getLogger('BMEApp')


def ldap_auth(user_name, password):
    """

    :rtype: object
    """
    #    server_uri = 'ldap://10.34.1.24'
    #    search_base = 'dc=bme-archive,dc=cz'
    search_filter = '(uid=' + user_name + ')'
    attrs = ['*']
    ret = -1

    access_base = "cn=" + user_name + ",ou=People," + SEARCH_BASE
    server = ldap3.Server(SERVER_URI)

    #   ldap3.Connection(server,"cn=admin" +  'dc=bme-archive, dc=cz' ,'Fra333nK', auto_bind = True)

    try:

        with ldap3.Connection(server, access_base, password, auto_bind=True) as conn:
            conn.search(SEARCH_BASE, search_filter, attributes=attrs)  # true, false
            entries = conn.entries[0]
            ret = str(entries.uidNumber)
            module_logger.info(ret)
            conn.unbind()
            module_logger.info(" LDAP check access OK %s" % user_name)
            return int(ret)

    except Exception as e:
        print(" error %s" % str(e))
        module_logger.error(" LDAP %s" % str(e))
        #        conn.unbind();
        return ret


def ldap_modify_password(user_name, password, new_password):
    search_filter = '(uid=' + user_name + ')'
    attrs = ['*']
    access_base = "cn=" + user_name + ",ou=People," + SEARCH_BASE
    server = ldap3.Server(SERVER_URI)
    user_dn = ''
    try:
        conn = ldap3.Connection(server, access_base, password, auto_bind=True)
        conn.open()
        conn.bind()
        conn.search(SEARCH_BASE, search_filter, attributes=attrs)  # true, false
        for entry in conn.response:
            if entry.get("dn") and entry.get("attributes"):
                if entry.get("attributes").get("cn"):
                    user_dn = entry.get("dn")
        hashed_password = hashed(HASHED_SALTED_SHA, new_password)
        changes = {
            'userPassword': [(MODIFY_REPLACE, [hashed_password])]
        }
        conn.modify(user_dn, changes=changes)
        conn.unbind()

    except Exception as e:
        print(" error %s" % str(e))
        module_logger.error(" LDAP %s" % str(e))
        conn.unbind()
        return 1


def ldap_add_user(password_admin, user_name, password, full_name, id_auth):
    add_string = "cn=" + user_name + ",ou=People," + 'dc=bme-archive,dc=cz'
    access_base = "cn=admin," + 'dc=bme-archive,dc=cz'
    server = ldap3.Server(SERVER_URI)
    try:
        conn = ldap3.Connection(server, access_base, password_admin, auto_bind=True)
        conn.open()
        conn.bind()
        conn.add(add_string, ['inetOrgPerson', 'posixAccount', 'shadowAccount'], {'sn': full_name, 'gidNumber': 5001,
                                                                                  "homeDirectory": "/home/" + user_name,
                                                                                  "uid": user_name,
                                                                                  "uidNumber": 1000 + id_auth})
        hashed_password = hashed(HASHED_SALTED_SHA, password)
        changes = {
            'userPassword': [(MODIFY_REPLACE, [hashed_password])]
        }
        conn.modify(add_string, changes=changes)
        conn.unbind()
        return 0, "ok"

    except Exception as e:
        print(" error %s" % str(e))
        module_logger.error(" LDAP %s" % str(e))
        conn.unbind()
        return -1, str(e)


def ldap_delete_user(password_admin, user_name):
    delete_string = "cn=" + user_name + ",ou=People," + 'dc=bme-archive,dc=cz'
    access_base = "cn=admin," + 'dc=bme-archive,dc=cz'
    server = ldap3.Server(SERVER_URI)
    try:
        conn = ldap3.Connection(server, access_base, password_admin, auto_bind=True)
        conn.open()
        conn.bind()
        conn.delete(delete_string)
        conn.unbind()
        return 0, "ok"

    except Exception as e:
        print(" error %s" % str(e))
        module_logger.error(" LDAP %s" % str(e))
        conn.unbind()
        return -1, str(e)
