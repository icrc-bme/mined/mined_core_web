# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Access(models.Model):
    id = models.BigAutoField(primary_key=True)
    description = models.CharField(max_length=512)

    class Meta:
        managed = False
        db_table = 'access'


class MinedUser(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_auth = models.IntegerField(unique=True)
    id_institution = models.IntegerField()
    full_name = models.CharField(max_length=1024, blank=True, null=True)
    contact = models.CharField(max_length=2048, blank=True, null=True)
    id_access = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'mined_user'


class UserView(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_auth = models.IntegerField(unique=True)
    id_institution = models.IntegerField()
    full_name = models.CharField(max_length=1024, blank=True, null=True)
    contact = models.CharField(max_length=2048, blank=True, null=True)
    id_access = models.IntegerField()
    description = models.CharField(max_length=512)
    institution_name = models.CharField(max_length=1024, blank=True, null=True)
    zone_name = models.CharField(max_length=512)

    class Meta:
        managed = False
        db_table = 'user_view'
