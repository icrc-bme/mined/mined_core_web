from .models_space import ContactView, Electrode, ElectrodeType
from .views_common import *
from django.shortcuts import render
from django.http import JsonResponse
from django.db import connection, transaction, Error, connections, IntegrityError


####################################
#
# E L E C T R O D E     pages
#
####################################

@login_test
def edit(request):
    # Electrode type: Manufacturer: Material: Macro distance: Micro distance: Dimension a: Dimension b:
    alert = ""
    actual_data = dict()
    if not request.session['db_id_electrode']:
        alert = 'Please select electrode'
    else:
        # actual data
        actual_data = Electrode.objects.get(id=request.session['db_id_electrode'])

    request.session['db_id_electrode_type'] = 0
    context = get_header_context(request, alert=alert)
    context_data = {
        'card_name': 'Electrode',
        # actual data
        'actual_data': actual_data,
    }
    context.update(context_data)
    return render(request, 'mined_app/space/electrode-edit.html', context)


@access(requested_permission='change_electrode')
def save(request):
    if request.POST.get('id_electrode_type') == '0':
        return 0, "Please select electrode type"

    try:
        with connection.cursor() as cur:
            cur.execute("UPDATE space.electrode SET electrode_name = %s, id_electrode_type = %s WHERE id = %s AND \
                            id_mined_subject = %s",
                        (request.POST.get('electrode_name'), request.POST.get('id_electrode_type'),
                         request.session['db_id_electrode'], request.session['db_id_mined']))
        ret = 0
        message = ""

    except IntegrityError:
        ret = 0
        message = ""

    except Error as e:
        message = "update electrode error " + e.args[0]
        ret = 1

    return ret, message


####################################
#
# E L E C T R O D E     AJAX data
#
####################################

def ajax_data(request):
    if request.method == 'POST':
        # NOTE: the key in QueryDict here is 'regions[]' so we have to use getlist('regions'), this is a jQuery thing
        # For details see: https://stackoverflow.com/questions/11190070/django-getlist

        record_list = list(ElectrodeView.objects.filter(id_mined_subject=request.session['db_id_mined']))

        # unique - it not exists relationship between session table and electrode table, only session_contact
        """  session_contact not in this version
        for idx in range(len(record_list) - 1, -1, -1):
            if record_list.count(record_list[idx]) > 1:
                del record_list[idx]
        """
        data = {
            'data': [[
                record.id,
                record.electrode_name,
                record.dimension_a,
                record.dimension_b,
                record.manufacturer,
                record.contact_material,
                record.electrode_type,
                record.macro_contact_distance,
                record.micro_contact_distance,
            ] for record in record_list]
        }
        return JsonResponse(data)


def ajax_select(request):
    if request.method == 'POST':
        selected_list = request.POST.get('selected')
        request.session['db_id_electrode'] = selected_list
        electrode_names = list(Electrode.objects.filter(id=selected_list).values(
                'electrode_name'))
        if len(electrode_names) > 0:
            request.session['db_electrode_name'] = electrode_names[0].get('electrode_name')
 #       request.session['db_electrode_name'] = ElectrodeView.objects.filter(id=selected_list[0]).values(
 #               'electrode_name')[0].get('electrode_name')
        request.session['db_id_contact'] = 0
        request.session['db_contact_name'] = ''

        data = {
                'data': [
                    request.session['db_id_contact'],
                    request.session['db_id_electrode'],
                    request.session['db_contact_name'],
                    request.session['db_electrode_name'],
                ]
        }
        return JsonResponse(data)


def ajax_multiple_select(request):
    if request.method == 'POST':
        data = {'data': []}
        if len(request.POST.getlist('selected[]')) > 0:
            selected_list = request.POST.getlist('selected[]')
            if len(selected_list) > 0:
                request.session['db_id_electrode[]'] = selected_list
                request.session['db_id_electrode'] = selected_list[0]
        else:
            selected = request.POST.getlist('selected')
            request.session['db_id_electrode'] = selected

        electrode_names = list(Electrode.objects.filter(id=request.session['db_id_electrode']).values(
                'electrode_name'))
        if len(electrode_names) > 0:
            request.session['db_electrode_name'] = electrode_names[0].get('electrode_name')

#            request.session['db_electrode_name'] = ElectrodeView.objects.filter(id=selected_list[0]).values(
#                'electrode_name')[0].get('electrode_name')
            request.session['db_id_contact'] = 0
            request.session['db_contact_name'] = ''

            data = {
                'data': [
                    request.session['db_id_contact'],
                    request.session['db_id_electrode'],
                    request.session['db_contact_name'],
                    request.session['db_electrode_name'],
                ]
            }
        return JsonResponse(data)




def ajax_contacts_data(request):  ############################## export todo #######################
    if request.method == 'POST':
        # NOTE: the key in QueryDict here is 'regions[]' so we have to use getlist('regions'), this is a jQuery thing
        # For details see: https://stackoverflow.com/questions/11190070/django-getlist
        print('Record ajax contacts')
        record_list = list(ContactView.objects.filter(id_electrode=request.session['db_id_electrode']))

        data = {
            'data': [[
                record.id_electrode,
                record.contact_name,
                record.impedance,
                record.acquisition_number,
                record.contact_type,
                record.contact_size,
                record.contact_size_value,
                record.structure_name,
                record.lobe_name,
                record.hemisphere,
                record.brain_area,
                record.manual_assign,
                record.atlas_name,
                record.system_name,
                record.system_unit,
                record.c_x,
                record.c_y,
                record.c_z,
            ] for record in record_list]
        }
        return JsonResponse(data)


def ajax_search_data(request):
    if request.method == 'POST':
        print('session search data')
        if request.POST.get('q'):
            query = request.POST.get('q')
            electrode_list = list(
                ElectrodeView.objects.filter(id_mined_subject=request.session['db_id_mined'],
                                             electrode_name__contains=request.POST.get('q')).order_by(
                    'id').values('electrode_name', 'id'))
        else:
            electrode_list = list(
                ElectrodeView.objects.filter(id_mined_subject=request.session['db_id_mined']).order_by(
                    'id').values('electrode_name', 'id'))

        # unique - it not exists relationship between session table and electrode table, only session_contact
        for idx in range(len(electrode_list) - 1, -1, -1):
            if electrode_list.count(electrode_list[idx]) > 1:
                del electrode_list[idx]

        return JsonResponse(electrode_list, safe=False)


def ajax_electrode_type_data(request):
    # it is table not pop up
    if request.method == 'POST':
        # NOTE: the key in QueryDict here is 'regions[]' so we have to use getlist('regions'), this is a jQuery thing
        # For details see: https://stackoverflow.com/questions/11190070/django-getlist
        print('Record ajax contacts')
        record_list = list(ElectrodeType.objects.all())

        data = {
            'data': [[
                record.id,
                record.type_name,
                record.manufacturer,
                record.contact_material,
                record.macro_contact_distance,
                record.micro_contact_distance,
                record.dimension_a,
                record.dimension_b,
            ] for record in record_list]
        }
        return JsonResponse(data)


def ajax_electrode_update(request):
    if request.method == 'POST':
        success, message = save(request)
        data = {
            'data': success,
            'message': message
        }
        return JsonResponse(data)
