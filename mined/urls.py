from django.contrib import admin
from django.urls import path, include


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('mined_app.urls')),
    path('session_security', include('session_security.urls')),
]
