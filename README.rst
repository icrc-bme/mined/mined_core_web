=========
Mined web
=========

Mined-web is a web interface for MINED (Multi-Institutional 
NeuroElectrophysiology Databaser) database. It contains basic 
handling of the database and analytics of features comptuted from
electrophysiological signals.

Detailed documentation is in the "docs" directory.
