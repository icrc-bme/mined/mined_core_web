    function getCSRFCookie(name) {
            let cookieValue = null;
            if (document.cookie && document.cookie !== '') {
                let cookies = document.cookie.split(';');
                for (let i = 0; i < cookies.length; i++) {
                    let cookie = cookies[i].trim();
                    // Does this cookie string begin with the name we want?
                    if (cookie.substring(0, name.length + 1) === (name + '=')) {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }return cookieValue;
    }

    function columnOption(column) {

//            var select = $('<select class="js-select-single form-control form-control-sm"><option value=""></option></select>')
//                $(".select-single").select2();

            let select;
                select = $('<select class="basic-single form-control"><option value=""></option></select>')
                .appendTo($(column.footer()).empty())
                .on('change', function () {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );

                    column
                        .search(val ? '^' + val + '$' : '', true, false)
                        .draw();
                });

            column.data().unique().sort().each(function(d, j) {
                select.append('<option value="' + d + '">' + d + '</option>')
            });

    }

    function notify(message, type) {
        let delay = 2500;
        if (type === 'danger' || type === 'warning'){
            delay = 0;
        }

        $.notify({
            icon: 'feather icon-bell',
            title: ' Database notify ',
            message: message,
            url: ''
        }, {
            element: 'body',
            type: type,
            allow_dismiss: true,
            placement: {
                from: 'bottom',
                align: 'left'
            },
            offset: {
                x: 30,
                y: 30
            },
            spacing: 10,
            z_index: 999999,
            delay: delay,
            timer: 1000,
            url_target: '_blank',
            mouse_over: false,
            animate: {
                enter: 'animated fadeInRight',
                exit: 'animated fadeOutLeft'
            },
            icon_type: 'class',
				template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
							'<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
							'<span data-notify="icon"></span> ' +
							'<span data-notify="title">{1}</span> ' + '<br>' +
							'<span data-notify="message"><b>{2}</b></span>' +
							'<div class="progress" data-notify="progressbar">' +
								'<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
							'</div>' +
							'<a href="{3}" target="{4}" data-notify="url"></a>' +
						'</div>'
        });
    }


/*
    $.fn.toggleAttr = function(attr) {
        return this.each(function() {
            let self = $(this);
            if (self.is('['+attr+']')) {
                self.removeAttr(attr)
            }else{
                self.attr(attr,1)
            }
        });
    };
    $('.toggle-li').on('click',function() {
        $('.hiddens').toggleAttr('hidden');
    });
*/



