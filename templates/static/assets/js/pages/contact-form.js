$(document).keypress(
  function(event){
    if (event.which === '13') {
      event.preventDefault();
    }
});

$(document).ready(function() {

    let contact_render = false;
    let atlas_render = false;

    $('textarea.max-textarea').maxlength({
        alwaysShow: true
    });

    ///////////////////////
    //    Table Contact
    //////////////////////

    let contact_table= $('#contact-table').DataTable({
            dom: 'frtip',
            select: 'single',
            paging: true,
            info: false,
            processing: false,
            serverSide: false,
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false,
                },
                {
                    "targets": [ 6 ],
                    render: function (data) {
                        if (!atlas_render) {
                            atlas_render = true;
                            if(data===null)
                                $('#atlas').attr('hidden','hidden');
                            else
                                $('#atlas').removeAttr('hidden');
                        }
                        return data;
                    },
                },
                {
                    "targets": [ 7 ],
                    render: function (data) {
                        if (!contact_render) {
                            contact_render = true;
                            if (data === null)
                                $('#coordinate').attr('hidden', 'hidden');
                            else
                                $('#coordinate').removeAttr('hidden');
                        }
                        return data;
                    },
                },

            ],
            keys: {
                    keys: [ 38 /* UP */, 40 /* DOWN */ ]
            },

            ajax: {
                url:'contact-data',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: function(d) {
                        Object.assign(d, {
                            atlas:$('#inputAtlas').val(),
                            coordinate_sys:$('#inputCoordinate').val()
                        })
                        return d;
                },
            },

            initComplete: function(setting, json){
               $('#contact-table tbody tr:eq(0)').select();
            },
    });

      // [ Individual Column Searching (Text Inputs) ]
    $('#contact-table tfoot th').each(function() {
            let title = $(this).text();
            $(this).html('<input type="text" class="form-control f-12" placeholder="' + title + '" />');
        });

        // [ Apply the search ]
    contact_table.columns().every(function() {
            let that = this;

            $('input', this.footer()).on('keyup change', function() {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });


    contact_table.on( 'select.dt', function ( e, dt, type, indexes ) {
        let data = dt.rows(indexes).data();
        document.getElementById('contact_name').innerHTML=data[0][1];       //for base_site.html
        $('#inputContactName').val(data[0][1]);
        $('#inputType').val(data[0][2]).trigger('change');
        $('#inputStructure').val(data[0][3]).trigger('change');
        $('#inputImaging').val(data[0][4]).trigger('change');
        $('#inputHemisphere').val(data[0][5]).trigger('change');
//        $('#inputAtlas').val(data[0][6]).trigger('change');
//        $('#inputCoordinate').val(data[0][7]+" ["+data[0][8] + "]").trigger('change');    //name and unit
        $('#inputCX').val(data[0][9]);
        $('#inputCY').val(data[0][10]);
        $('#inputCZ').val(data[0][11]);
        $('#inputImpedance').val(data[0][12]);
        $('#inputSizeName').val(data[0][13] + " ["+data[0][14] + "]").trigger('change');      //size name and values
        $('#inputLobe').val(data[0][15]).trigger('change');
        $('#inputBrain').val(data[0][16]).trigger('change');
        $('#inputManual').val(data[0][17]).trigger('change');
        document.getElementById("headerIntervention").style.display = "none";
        document.getElementById("boxIntervention").style.display = "none";
        $('#warning').hide()
        id_contact = data[0][0];
    });

    contact_table.ajax.reload( function (json) {
        $('#contact-table tbody td:eq(0)').click();
    } );

    contact_table.on('key-focus.dt', function(e, datatable, cell){
            // Select highlighted row
            contact_table.row(cell.index().row).select();
    });

    contact_table.on('click', 'tbody td', function(e) {
        try {
            e.stopPropagation();
            // Get index of the clicked row
            let rowIdx = contact_table.cell(this).index().row;
            // Select row
            contact_table.row(rowIdx).select();
        }
        catch (err){

        }
    });
    //[ edit main select ]//
    $("#inputAtlas").change(function() {
        $("#hAnatomy").text('Anatomy ' + $("#inputAtlas").val())
        contact_table.ajax.reload();
    });
    $("#inputCoordinate").change(function() {
        $("#hCoordinate").text('Coordinate ' + $("#inputCoordinate").val())
        contact_table.ajax.reload();
    });

    let btCUpdate = $('#btnContactUpdate');
    btCUpdate.on('click',function(){
        update();
    });
    // switch (id_access){
    //     case 1:
    //             btCUpdate.on('click',function(){
    //                 update();
    //             });
    //     break;
    //     case 2:                                 //read only
    //             btCUpdate.removeClass("btn-primary").addClass("btn-outline-secondary disabled");
    //             break;
    //     case 99:                                 //read only
    //             btCUpdate.removeClass("btn-primary").addClass("btn-outline-secondary disabled");
    //             break;
    //     }


    function update(){
        $.ajax({
                url: 'contact-update',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: {
                    id_contact: id_contact,
                    contact_name: $('#inputContactName').val(),
                    impedance: $('#inputImpedance').val(),
  //                  acq_number:$('#inputAcqNumber').val(),
                    type: $('#inputType').val(),
                    size_name:$('#inputSizeName').val(),      //size name and values
                    imaging:$('#inputImaging').val(),
                    structure:$('#inputStructure').val(),
                    lobe:$('#inputLobe').val(),
                    hemisphere:$('#inputHemisphere').val(),
                    brain: $('#inputBrain').val(),
                    manual: $('#inputManual').val(),
                    atlas: $('#inputAtlas').val(),
                    coordinate: $('#inputCoordinate').val(),    //name and unit
                    cx: $('#inputCX').val(),
                    cy: $('#inputCY').val(),
                    cz: $('#inputCZ').val(),
                    },
                success: function (data) {
                    switch(data.data) {
                        case 0:
                            notify('Database operation success', 'success');
                            contact_table.ajax.reload();
                            break;
                        case 1:
                            notify('Database operation error ' + data.message, 'danger');
                            break;
                        case 2:
                            notify('You do not have the permission to perform this operation', 'danger');
                            break;
                    }

                }
        });
    };

});

