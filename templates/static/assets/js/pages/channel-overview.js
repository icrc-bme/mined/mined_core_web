$(document).ready(function() {
    setTimeout(function() {

        let channel_table=$('#channel-table').DataTable({
            dom: 'frtip',
            select: 'single',
            processing: false,
            serverSide: false,
            keys: {
                    keys: [ 13 /* ENTER */, 38 /* UP */, 40 /* DOWN */ ]
            },
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false,
                    className: 'noVis'
                },
            ],
            ajax: {
                url:'channel-data',
                type: 'POST',
                headers: {'X-CSRFToken': window.getCSRFCookie('csrftoken')},
                data: 'data',
            },
            initComplete: function(){
               $('#channel_table tbody td:eq(0)').click();
            },

        });

        channel_table.ajax.reload( function () {
            $('#channel-table tbody td:eq(0)').click();
        } );


        // [ Individual Column Searching (Text Inputs) ]
        $('#channel-table tfoot th').each(function() {
            $(this).html('<input type="text" class="form-control f-12" placeholder="Search" />');
        });


        // [ Apply the search ]
        channel_table.columns().every(function() {
            let that = this;

            $('input', this.footer()).on('keyup change', function() {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });

                // [key focus td]
        channel_table.on('key-focus.dt', function(e, datatable, cell){
            // Select highlighted row
            channel_table.row(cell.index().row).select();
        });

        channel_table.on('click', 'tbody td', function(e){
               e.stopPropagation();
                // Get index of the clicked row
                let rowIdx = channel_table.cell(this).index().row;
                // Select row
                channel_table.row(rowIdx).select();
         });


   });
})