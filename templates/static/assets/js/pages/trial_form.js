$(document).ready(function() {

    let table_trial = $('#trial-table').DataTable({
            dom: 'Bfrtip',
            select: 'single',
            paging: true,
            info: false,
            processing: false,
            serverSide: false,
            buttons: [],
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false,
                },
            ],

            ajax: {
                url:'trial-data',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: 'data',
            }
    });

    table_trial.on( 'select.dt', function ( e, dt, type, indexes ) {
        document.getElementById('workMode').innerHTML = 'You are editing';
        let data = dt.rows(indexes).data();
        document.getElementById('inputTaskId').value=data[0][0];
        $('#inputTrial').val(data[0][1]).trigger('change');
        $('#inputSet').val(data[0][2]).trigger('change');

        document.getElementById('inputOnset').value=data[0][3];
        document.getElementById('inputDuration').value=data[0][4];
        document.getElementById('inputTime').value=data[0][5];
        document.getElementById('inputResponse').value=data[0][6];

    });

    table_trial.button().add( 0, {
        action: function ( e, dt, button, config ) {
                document.getElementById('workMode').innerHTML = 'You are inserting';
                $('#inputTrial').val("n/a").trigger('change');
                document.getElementById('inputOnset').value=null;
                document.getElementById('inputDuration').value=null;
                document.getElementById('inputResponse').value=null;
                document.getElementById('inputReference').value=null;
                document.getElementById('inputTime').value=null;
                document.getElementById('inputTTL').value=null;
                $('#inputTask').val("n/a").trigger('change');
                document.getElementById('inputDescription').value=null;
                document.getElementById('inputInstruction').value=null;

                $.ajax({
                    url: 'trial-select',
                    type: 'POST',
                    headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                    data: {
                        selected: 0,
                    },
                    success: function (data) {
                    }
                });
        },
        text: 'Clear selection'
    });

    let table_task = $('#task-table').DataTable({
            dom: 'Bfrtip',
            select: 'single',
            paging: true,
            info: false,
            processing: false,
            serverSide: false,
            buttons: [],
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false,
                },
            ],

            ajax: {
                url:'trial-data',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: 'data',
            }
    });

    table_task.on( 'select.dt', function ( e, dt, type, indexes ) {
        document.getElementById('workMode').innerHTML = 'You are editing task';
        let data = dt.rows(indexes).data();
        document.getElementById('inputTaskId').value=data[0][0];
        $('#inputTrial').val(data[0][1]).trigger('change');
        $('#inputSet').val(data[0][2]).trigger('change');
        document.getElementById('inputOnset').value=data[0][3];
        document.getElementById('inputDuration').value=data[0][4];
        document.getElementById('inputTime').value=data[0][5];
        document.getElementById('inputResponse').value=data[0][6];

    });

    table_task.button().add( 0, {
        action: function ( e, dt, button, config ) {
                document.getElementById('workMode').innerHTML = 'You are inserting task';
                document.getElementById('inputHistoDate').value=null;
                document.getElementById('inputHistoId').value=0;
                $('#inputHistoName').val("n/a").trigger('change');

                //$(this).rows().deselect();                                                //todo I don't known yet

                $.ajax({
                    url: 'task-select',
                    type: 'POST',
                    headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                    data: {
                        selected: 0,
                    },
                    success: function (data) {
                    }
                });
        },
        text: 'Clear selection'
    });
});