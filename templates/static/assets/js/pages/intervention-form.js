$(document).ready(function() {

    let interv_actual_data = {
        db_oper: 0,               // 0 - none, 1 - insert, 2 - update
        date: null,
        name: null,
        contact_ids: []
    } 

    let intervention_table = $('#intervention-table').DataTable({
            dom: 'ftp',
            select: 'single',
            paging: true,
            info: false,
            processing: false,
            serverSide: false,
            buttons: [],
            columnDefs: [
                {
                    "targets": [ 0 ],                       //id_subject_intervention
                    "visible": false,
                    "searchable": false,
                },
                {
                    "targets": [ 1 ],                        //id_mined_subject
                    "visible": false,
                    "searchable": false,
                },
                {
                    "targets": [ 2 ],                        //date format
                    render: function (data) {
                        return moment(data.substring(0,16)).format('YYYY-MM-DD HH:mm');
                    },
                },
            ],

            ajax: {
                url:'intervention-data',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: 'data',
            },
            initComplete: function() {
                $('.checkbox-intervention:checked').each(function(){                //remove last chhecked
                   $(this).prop('checked', false);
                })
            },

    });
    // [click on td]
    intervention_table.on( 'select.dt', function ( e, dt, type, indexes ) {
        $('#btnInterventionSubmit').text('Update');
        let data = dt.rows(indexes).data();
        $('#inputInterventionDate').val(moment(data[0][2].substring(0,16)).format('YYYY-MM-DD HH:mm'));
        $('#inputInterventionName').val(data[0][3]).trigger('change');

        $.ajax({
            url: 'intervention-select',
            type: 'POST',
            headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
            data: {
                selected: data[0][0],
            },
            success: function (data) {
                $('.checkbox-intervention:checked').each(function(){                //remove last chhecked
                    $(this).prop('checked', false);
                })

                $.each(data.data,function(index, value){
//                    let contact_box = $('#' + value);
//                    console.log('contact box ', contact_box);
                    $('#' + value[0]).prop('checked',true);
                });
            }
        });

    });
    // [button Clear selection]
    $('#inputClearIntervention').click(function(){
        $('#intervention-table tbody tr').removeClass('selected')
        $('#btnInterventionSubmit').text('Insert');
        $('#inputInterventionDate').val(null);
        $('#inputInterventionName').val("n/a").trigger('change');
         $('.checkbox-intervention:checked').each(function(){
                    $(this).prop('checked', false);
                })
            $.ajax({
                url: 'intervention-select',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: {
                    selected: 0,
                },
                success: function (data) {
                }
            });
    });

    let btSubmit = $('#btnInterventionSubmit');
    btSubmit.on('click',function(){
        submit();
    });

    function submit (){
      
        interv_actual_data.name = $('#inputInterventionName').val();
        interv_actual_data.date = $('#inputInterventionDate').val();

        // build contact ids
        var checkboxes = document.getElementsByClassName("checkbox-intervention");
        // loop over them all
        for (var i=0; i<checkboxes.length; i++) {
            // And stick the checked ones onto an array...
            if (checkboxes[i].checked) {
               interv_actual_data.contact_ids.push(checkboxes[i].id);
            }
        }


        $.ajax({
                url: 'intervention-submit',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: interv_actual_data,
                success: function (data) {
                    switch(data.data) {
                        case 0:
                            notify('Database operation success', 'success');
                            intervention_table.ajax.reload();
                            break;
                        case 1:
                            notify('Database operation error ' + data.message, 'danger');
                            break;
                        case 2:
                            notify('You do not have the permission to perform this operation', 'danger');
                            break;
                    }

                }
        });
    }

    //let btSubmit = $('#btnInterventionSubmit');


});
