$(document).ready(function() {

    $("#subjectCount").text('Subject count ' + 0);
    $("#inputHisto").val('');
    $("#inputInter").val('');
    $("#inputOutcome").val('');

    function show_count(data){
        let subjectCount = $("#subjectCount");
        subjectCount.text('Subject count ' + data);
        if (data === 0){
            subjectCount.attr('class', 'text-danger');
        }
        else {
            subjectCount.attr('class', 'text-warning');
        }
    }
    function plot_two(id_plot, text, json_data) {   //DIV id,'I',list,1200
        let subject_layout = {
            annotations: [
            {
                font: {
                size: 20
                },
                showarrow: false,
                text: text[0],
                x: 0.17,
                y: 0.5
            },
            {
                font: {
                    size: 20
                },
                showarrow: false,
                text: text[1],
                x: 0.82,
                y: 0.5
            }
            ],
            autosize: true,
//        height: 400,
//        width: 600,
            showlegend: true,
            grid: {rows: 1, columns: 2},
            plot_bgcolor:"#39434d",
            paper_bgcolor:"#39434d",

        };
        let plot_data = [
            {
                values: json_data.data['vi'],
                labels: json_data.data['li'],
                domain: {column:0},
                name: '',
                hole: .4,
//                hoverinfo: 'label+percent+name',
                hoverinfo: 'label+value+name',
                textinfo: 'value',
                type: 'pie',
            },
            {
                values: json_data.data['ve'],
                labels: json_data.data['le'],
                domain: {column:1},
                name: '',
                hole: .4,
                hoverinfo: 'label+value+name',
                textinfo: 'value',
                type: 'pie',
            }
        ]
       Plotly.newPlot(id_plot,plot_data,subject_layout,{responsive: true});
    }

    function plot(id_plot, json_data) {   //DIV id,'I',list,1200
        let subject_layout = {
            annotations: [
            {
                font: {
                size: 20
                },
                showarrow: false,
                text: ' ',
                x: 0.17,
                y: 0.5
            },
            ],
            autosize: true,
//        height: 400,
//        width: 600,
            showlegend: true,
            grid: {rows: 1, columns: 1},
            plot_bgcolor:"#39434d",
            paper_bgcolor:"#39434d",
        };
        let plot_data = [{
            values: json_data.data['v'],
            labels: json_data.data['l'],
            domain: {column:0},
            name: '',
            hole: .4,
            hoverinfo: 'label+value+name',
            textinfo: 'value',
            type: 'pie',
        }]
       Plotly.newPlot(id_plot,plot_data,subject_layout,{responsive: true});
    }



    $.ajax({
            url: 'histopathology-plotly-data',
            type: 'POST',
            headers: {'X-CSRFToken': window.getCSRFCookie('csrftoken')},
            data:{
                'type':'patient',
            },
            success: function (data) {
                plot('histoPlot',data);
            }
    })


    $("#histoPlot").on('plotly_relayout', function(data){
        let selected = this.layout.hiddenlabels[this.layout.hiddenlabels.length-1];
        console.log(selected);
        $("#inputHisto").val(selected);
        $.ajax({
            url: 'histopathology-plotly-select',
            type: 'POST',
            headers: {'X-CSRFToken': window.getCSRFCookie('csrftoken')},
            data:{
                'histo':selected,
            },
            success: function (data) {
                show_count(data.data);
            }
        })
    });

   $.ajax({
            url: 'inter-plotly-data',
            type: 'POST',
            headers: {'X-CSRFToken': window.getCSRFCookie('csrftoken')},
            data:{
                'type':'patient',
            },
            success: function (data) {
                plot('interPlot',data);
            }
    })

   $("#interPlot").on('plotly_relayout', function(data){
        let selected = this.layout.hiddenlabels[this.layout.hiddenlabels.length-1];
        $("#inputInter").val(selected);
        $.ajax({
            url: 'inter-plotly-select',
            type: 'POST',
            headers: {'X-CSRFToken': window.getCSRFCookie('csrftoken')},
            data:{
                'inter':selected,
            },
            success: function (data) {
                show_count(data.data);
            }
        })
   });

   $.ajax({
            url: 'outcome-plotly-data',
            type: 'POST',
            headers: {'X-CSRFToken': window.getCSRFCookie('csrftoken')},
            data:{
                'type':'patient',
            },
            success: function (data) {
                plot_two('outcomePlot',['Ilae','Engel'], data);
            }
    })

   $("#outcomePlot").on('plotly_relayout', function(data){
        let selected = this.layout.hiddenlabels[this.layout.hiddenlabels.length-1];
        $("#inputOutcome").val(selected);
        $.ajax({
            url: 'outcome-plotly-select',
            type: 'POST',
            headers: {'X-CSRFToken': window.getCSRFCookie('csrftoken')},
            data:{
                'outcome':selected,
            },
            success: function (data) {
                show_count(data.data);
            }
        })
   });

   $.ajax({
            url: 'type-plotly-data',
            type: 'POST',
            headers: {'X-CSRFToken': window.getCSRFCookie('csrftoken')},
            data:{
                'type':'patient',
            },
            success: function (data) {
                plot('recTypePlot', data);
            }
    })

   $.ajax({
            url: 'lobe-plotly-data',
            type: 'POST',
            headers: {'X-CSRFToken': window.getCSRFCookie('csrftoken')},
            data:{
                'type':'patient',
            },
            success: function (data) {
                plot('lobePlot', data);
            }
    })

   $('#btnSubmit').on('click',function() {
        $.ajax({
                url: 'dashboard-select',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: {
                    histo: $('#inputHisto').val(),
                    inter:  $("#inputInter").val(),
                    outcome: $("#inputOutcome").val(),
                },
                success: function (data) {

                }
        });
   });

});