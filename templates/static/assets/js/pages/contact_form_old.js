$(document).keypress(
  function(event){
    if (event.which == '13') {
      event.preventDefault();
    }
});

$(document).ready(function() {

    function getCSRFCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = cookies[i].trim();
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }return cookieValue;
    }

    $(".basic-single").select2();

        // [ Ip masking ]  mask is not functopn
//    $('.ip').mask('000.000.000.000');


    $('textarea.max-textarea').maxlength({
        alwaysShow: true
    });

    ///////////////////////
    //    Table Contact
    //////////////////////

    let table_contact= $('#contact-table').DataTable({
            select: 'single',
            paging: true,
            info: false,
            processing: false,
            serverSide: false,
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false,
                },
                {
                    "targets": [ 1 ],
                    "visible": false,
                    "searchable": false,
                },
            ],
            keys: {
                    keys: [ 38 /* UP */, 40 /* DOWN */ ]
            },

            ajax: {
                url:'contacts-data',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: 'data',
            }
    });

      // [ Individual Column Searching (Text Inputs) ]
    $('#contact-table tfoot th').each(function() {
            let title = $(this).text();
            $(this).html('<input type="text" class="form-control f-12" placeholder="' + title + '" />');
        });

        // [ Apply the search ]
    table_contact.columns().every(function() {
            let that = this;

            $('input', this.footer()).on('keyup change', function() {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });


    $('#contact-table').on( 'select.dt', function ( e, dt, type, indexes ) {
        let data = dt.rows(indexes).data();
        document.getElementById('contact_name').innerHTML=data[0][2];


        $('#inputAnatomy').val(data[0][1]).trigger('change');

        document.getElementById('inputContactName').value=data[0][2];
        document.getElementById('inputImpedance').value=data[0][3];
        document.getElementById('inputAcqNumber').value=data[0][4];
        $('#inputType').val(data[0][5]).trigger('change');
        $('#inputSizeName').val(data[0][6]).trigger('change');  //+" ["+data[0][7] + "]"
        $('#inputStructure').val(data[0][8]).trigger('change');
        $('#inputLobe').val(data[0][9]).trigger('change');
        $('#inputHemisphere').val(data[0][10]).trigger('change');
        $('#inputBrain').val(data[0][11]).trigger('change');
        $('#inputManual').val(data[0][12]).trigger('change');
        $('#inputAutomated').val(data[0][13]).trigger('change');
        $('#inputAtlas').val(data[0][14]).trigger('change');
        $('#inputCoordinate').val(data[0][15]+" ["+data[0][16] + "]").trigger('change');
        document.getElementById('inputCX').value=data[0][17];
        document.getElementById('inputCY').value=data[0][18];
        document.getElementById('inputCZ').value=data[0][19];
        console.log(data[0][10]);
    });

    table_contact.ajax.reload( function (json) {
        $('#contact-table tbody td:eq(0)').click();
    } );

    $('#contact-table').on('key-focus.dt', function(e, datatable, cell){
            // Select highlighted row
            table_contact.row(cell.index().row).select();
    });

    ///////////////////////
    //    Select id anatomy
    //////////////////////


    $('#inputAnatomy').select2({
        ajax: {
            url: "anatomy-data",
            dataType: 'json',
            type: 'POST',
            delay: 250,
            headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
            data: function(params) {
                return {
                    q: params.term, // search term
                    page: params.page,
                    structure: $('#inputStructure option:selected').text(),
                    atlas:  $('#inputAtlas option:selected').text(),
                    lobe:  $('#inputLobe option:selected').text(),
                    hemisphere:  $('#inputHemisphere option:selected').text(),
                    brain:  $('#inputBrain option:selected').text(),
                    manual:  $('#inputManual option:selected').text(),
                    automated:  $('#inputAutomated option:selected').text(),
                };
            },
            processResults: function(data, params) {
                params.page = params.page || 1;
                return {
                    results: $.map(data, function (item) {

                        return {
                           text: item.id_anatomy,
                           id: item.id,
                        }
                    }),

                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: false,
        },
        escapeMarkup: function(markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 0,

    });

    $('#inputAnatomy').on("select2:selecting", function(e) {
            $.ajax({
                url:'anatomy-search-data',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data:
                    {
                    selected_id: e.params.args.data.id,
                    },
                success: function (data) {
                    let data_array = Object.values(data);
                    console.debug(data_array);
                    $('#inputAnatomy').val(data_array[0][0]).trigger('change');
                    $('#inputAtlas').val(data_array[0][1]).trigger('change');
                    $('#inputStructure').val(data_array[0][2]).trigger('change');
                    $('#inputLobe').val(data_array[0][3]).trigger('change');
                    $('#inputHemisphere').val(data_array[0][4]).trigger('change');
                    if(data_array[0][5] == '-1')
                            data_array[0][5] = ''
                    $('#inputBrain').val(data_array[0][5]).trigger('change');
                    $('#inputManual').val(data_array[0][6]).trigger('change');
                    $('#inputAutomated').val(data_array[0][7]).trigger('change');
                }

            })
    });

    ///////////////////////
    //    Select id structure
    //////////////////////

    // [ load data ]

    $('#inputStructure').select2({
        ajax: {
            url: "structure-data",
            dataType: 'json',
            type: 'POST',
            delay: 250,
            headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
            data: function(params) {
                return {
                    q: params.term, // load all data
                    page: params.page
                };
            },
            processResults: function(data, params) {
                params.page = params.page || 1;
                return {
                    results: $.map(data, function (item) {

                        return {
                           text: item.structure_name,
                           id: item.id_structure
                        }
                    }),

                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: false,
        },
        escapeMarkup: function(markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 0,

    });


    $('#inputStructure').on("select2:selecting", function(e) {
            $.ajax({
                url:'structure-search-data',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data:
                    {
                        selected_id: e.params.args.data.id,
                        structure: e.params.args.data.text,
                    },
                success: function (data_anatomy) {
                    let array_anatomy = Object.values(data_anatomy);
                    console.debug(array_anatomy);
                    $('#inputAnatomy').val(0).trigger('change');
                }

            })
    });

    $('#inputLobe').select2({
        ajax: {
            url: "lobe-data",
            dataType: 'json',
            type: 'POST',
            delay: 250,
            headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
            data: function(params) {
                return {
                    q: params.term, // load all data
                    page: params.page
                };
            },
            processResults: function(data, params) {
                params.page = params.page || 1;
                return {
                    results: $.map(data, function (item) {

                        return {
                           text: item.name,
                           id: item.id
                        }
                    }),

                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: false,
        },
        escapeMarkup: function(markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 0,

    });


    $('#inputLobe').on("select2:selecting", function(e) {
            $.ajax({
                url:'lobe-search-data',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data:
                    {
                        selected_id: e.params.args.data.id,
                        lobe: e.params.args.data.text,
                    },
                success: function (data_anatomy) {
                    let array_anatomy = Object.values(data_anatomy);
                    console.debug(array_anatomy);
                    $('#inputAnatomy').val(0).trigger('change');
                }

            })
    });

    $('#inputAtlas').select2({
        ajax: {
            url: "atlas-data",
            dataType: 'json',
            type: 'POST',
            delay: 250,
            headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
            data: function(params) {
                return {
                    q: params.term, // load all data
                    page: params.page
                };
            },
            processResults: function(data, params) {
                params.page = params.page || 1;
                return {
                    results: $.map(data, function (item) {

                        return {
                           text: item.atlas_name,
                           id: item.id
                        }
                    }),

                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: false,
        },
        escapeMarkup: function(markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 0,

    });


    $('#inputAtlas').on("select2:selecting", function(e) {
            $.ajax({
                url:'atlas-search-data',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data:
                    {
                        selected_id: e.params.args.data.id,
                        atlas: e.params.args.data.text,
                    },
                success: function (data_anatomy) {
                    let array_anatomy = Object.values(data_anatomy);
                    console.debug(array_anatomy);
//                    $('#inputAnatomy').val(array_anatomy[0]).trigger('change');
                    $('#inputAnatomy').val(0).trigger('change');
                }

            })
    });

    $('#buttonSelection').click(function(e){
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "search-clear",
            headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
            data: {
                id: $(this).val(), // < note use of 'this' here
                access_token: $("#access_token").val()
            },
            success: function(result) {
//                alert('ok');
            },
            error: function(result) {
                alert('error');
            }
        });
    });


});

