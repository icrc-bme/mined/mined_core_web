$(document).ready(function() {
    setTimeout(function() {

        let session_table = $('#session-table').DataTable({
            dom: 'Bfrtip',
            select: 'single',
            processing: false,
            serverSide: false,
            buttons: [
                {
                    extend: 'csv',
                    text: 'csv',
                    exportOptions: {
                        modifier: {
                            selected: null
                        },
                        columns: ':visible'
                    }
                },
                {
                    extend: 'excel',
                    text: 'excel',
                    exportOptions: {
                        modifier: {
                            selected: null
                        },
                        columns: ':visible'
                    }
                },
            ],
            keys: {
                    keys: [ 13 /* ENTER */, 38 /* UP */, 40 /* DOWN */ ]
            },

            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false,
                    className: 'noVis'
                },
            ],

            ajax: {
                url:'session-data',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: 'data',
            },
            initComplete: function(setting, json){
                $('#session-table tbody tr:eq(0)').click();
            }
        });


        session_table.ajax.reload( function (json) {
            $('#session-table tbody td:eq(0)').click();
        } );


        session_table.button().add( 0, {
                action: function ( e, dt, button, config ) {
                dt.ajax.reload();
                },
                text: 'reload'
        } );

        // [ Individual Column Searching (Text Inputs) ]
        $('#session-table tfoot th').each(function() {
            let title = $(this).text();
            $(this).html('<input type="text" class="form-control f-12" placeholder="Search ' + title + '" />');
        });


        // [ Apply the search ]
        session_table.columns().every(function() {
            let that = this;

            $('input', this.footer()).on('keyup change', function() {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });


        session_table.on( 'select.dt', function ( e, dt, type, indexes ) {
            let data = dt.rows(indexes).data();
            $.ajax({
                url: 'session-select',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: {
                    selected: data[0][0],
                },
                success: function (data) {
                    var data_array = Object.values(data);
                    document.getElementById('session_id').innerHTML=data_array[0][0];
                    document.getElementById('electrode_id').innerHTML=data_array[0][1];
                    document.getElementById('session_name').innerHTML=data_array[0][2];
                    document.getElementById('contact_count').innerHTML=data_array[0][3];
                    document.getElementById('electrode_count').innerHTML=data_array[0][4];
                }
            });
        });

        // [key focus td]
        session_table.on('key-focus.dt', function(e, datatable, cell){
            // Select highlighted row
            console.log(datatable)
            session_table.row(cell.index().row).select();
        });

        session_table.on('click', 'tbody td', function(e){
               e.stopPropagation();
                // Get index of the clicked row
                let rowIdx = session_table.cell(this).index().row;
                // Select row
                session_table.row(rowIdx).select();
         });


         // [Handle double click on table cell]
        session_table.on('dblclick', 'tbody td', function(e, datatable, cell){
            e.stopPropagation();

            // Get index of the clicked row
            let rowIdx = session_table.cell(this).index().row;

            // Select row
            let row = session_table.row(rowIdx).select();
            let data = row.data();
            // Select data
            let data_array = Object.values(data);
            $.ajax({
                url: 'session-select',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: {
                    selected: data_array[0],                   //id_mined
                },
                success: function (data) {
                    var data_array = Object.values(data);
                    document.location.href = "contact-structured";
                }
            });
        });

        // [Handle key event that hasn't been handled by KeyTable]
        session_table.on('key.dt', function(e, datatable, key, cell, originalEvent){
            // If ENTER key is pressed
            if(key === 13){
                // Get highlighted row data
                let data_array = session_table.row(cell.index().row).data();
                $.ajax({
                    url: 'session-select',
                    type: 'POST',
                    headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                    data: {
                        selected: data_array[0],                   //id_mined
                    },
                    success: function (data) {
                        document.location.href = "contact-structured";
                    }
                });

            }
        });
   });
})