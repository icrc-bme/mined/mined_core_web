$(document).ready(function() {
    setTimeout(function() {


        // Setup - add a text input to each footer cell
        $('#subject-table tfoot th').each(function () {
            var title = $(this).text();
            if ($(this)[0].className === "text_search") {
                $(this).html('<input type="text" class="form-control f-12" placeholder="Search ' + title + '" />');
            }
        })


        let subject_table=$('#subject-table').DataTable({
            dom: 'Bfrtip',
            select: 'single',
            buttons: [
                {
                    extend: 'csv',
                    text: 'csv',
                    exportOptions: {
                        modifier: {
                            selected: null
                        },
                        columns: ':visible'
                    }
                },
                {
                    extend: 'excel',
                    text: 'excel',
                    exportOptions: {
                        modifier: {
                            selected: null
                        },
                        columns: ':visible'
                    }
                },
            ],
            processing: false,
            serverSide: false,
            scrollY:        '50vh',
            scrollCollapse: true,
            paging:         false,
            keys: {
                    keys: [ 13 /* ENTER */, 38 /* UP */, 40 /* DOWN */ ]
            },
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false,
                    className: 'noVis'
                },
            ],
            order: [[ 1, "asc" ]],

            ajax: {
                url:'subject-data',
                type: 'POST',
                headers: {'X-CSRFToken': window.getCSRFCookie('csrftoken')},
                data: 'data',
            },

            initComplete: function(settings, json) {
            },
        });
        /* not available with ajax
https://ittone.ma/ittone/how-to-update-data-after-ajax-reload-using-initcomplete-callback-for-datatables-when-using-ajax-source-data/
        subject_table.ajax.reload( function () {
            $('#subject-table tbody td:eq(0)').click();
        } );

        function initCompleteFunction(settings, json){
            let api = new $.fn.dataTable.Api( settings );
            api.columns().every(function() {
                   let column = this;
                   if ($(column.footer()).hasClass('select_search')) {
                       console.log('select',column);
                       let select = $('<select class="select-single form-control"><option value="">' + $(column.footer()).html() + '</option></select>')
                           .appendTo($(column.footer()).empty())
                           .on('change', function () {
                               let val = $.fn.dataTable.util.escapeRegex(
                                   $(this).val()

                               );
                               column
                                   .search(val ? '^' + val + '$' : '', true, false)
                                   .draw()

                           });
                       column.data().unique().sort().each(function (d, j) {
                            console.log('sort',d,j);
                           select.append('<option value="' + d + '">' + d + '</option>')
                       });

                   }
                   else {

                       let that = this;
                       $('input', this.footer()).on('keyup change', function () {
                           that.search(this.value).draw();
                       });
                   }
               });

        }

        subject_table.on('xhr.dt', function ( e, settings, json, xhr ) {
                initCompleteFunction(settings, json);
        })
*/
        subject_table.button().add( 0, {
                action: function ( e, dt ) {
                dt.ajax.reload();
                },
                text: 'reload'
        } );
        // [button Detail]
/*
        new $.fn.dataTable.Buttons(subject_table, {
            buttons: [{
                text: 'Detail',
                action: function(e, dt, node, config) {
                        document.location.href = "subject-detail";
                    }
                },
            ]
        });
        subject_table.buttons(1, null).container().appendTo(
            subject_table.table().container()
        );
*/

        // [ Individual Column Searching (Text Inputs) ]
        $('#subject-table tfoot th').each(function() {
            $(this).html('<input type="text" class="form-control f-12" placeholder="Search" />');
        });


        // [ Apply the search ]
        subject_table.columns().every(function() {
            let that = this;

            $('input', this.footer()).on('keyup change', function() {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });

        // [click on td]
        subject_table.on( 'select.dt', function ( e, dt, type, indexes ) {
            let data = dt.rows(indexes).data();
            $.ajax({
                url: 'subject-select',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: {
                    selected: data[0][0],                   //id_mined
                },
                success: function (data) {
                    let data_array = Object.values(data);
                    document.getElementById('subject_id').innerHTML= data_array[0][0];  //returned by django
                    document.getElementById('session_count').innerHTML=data_array[0][1];
                }
            });
          });

        // [key focus td]
        subject_table.on('key-focus.dt', function(e, datatable, cell){
            // Select highlighted row
            subject_table.row(cell.index().row).select();
        });


        subject_table.on('click', 'tbody td', function(e){
                e.stopPropagation();
                // Get index of the clicked row
                let rowIdx = subject_table.cell(this).index().row;
                // Select row
                subject_table.row(rowIdx).select();
         });
         // [Handle double click on table cell]
        subject_table.on('dblclick', 'tbody td', function(e){
            e.stopPropagation();

            // Get index of the clicked row
            let rowIdx = subject_table.cell(this).index().row;

            // Select row
            let row = subject_table.row(rowIdx).select();
            let data = row.data();
            // Select data
            let data_array = Object.values(data);
            $.ajax({
                url: 'subject-select',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: {
                    selected: data_array[0],                   //id_mined
                },
                success: function () {
                    document.location.href = "session-overview";
                }
            });
        });

        // [Handle key event that hasn't been handled by KeyTable]
        subject_table.on('key.dt', function(e, datatable, key, cell){
            // If ENTER key is pressed
            if(key === 13){
                // Get highlighted row data
                let data_array = subject_table.row(cell.index().row).data();
                $.ajax({
                    url: 'subject-select',
                    type: 'POST',
                    headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                    data: {
                        selected: data_array[0],                   //id_mined
                    },
                    success: function () {
                        document.location.href = "../session-overview";
                    }
                });

            }
        });

       // [ Individual Column Searching (Select Inputs) ]
        $('#footer-select').DataTable({
            initComplete: function() {
                this.api().columns().every(function() {
                    var column = this;
                    var select = $('<select class="form-control form-control-sm"><option value=""></option></select>')
                        .appendTo($(column.footer()).empty())
                        .on('change', function() {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search(val ? '^' + val + '$' : '', true, false)
                                .draw();
                        });

                    column.data().unique().sort().each(function(d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>')
                    });
                });
            }
        });
        $('#cbFilter').prop('checked',false);
        $('#cbFilter').change(function(){
            let filter_checked = false;
            if (this.checked)
                filter_checked =true;
            $.ajax({
                url: 'subject-filter',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: {
                    filter: filter_checked,
                },
                success: function () {
                    subject_table.ajax.reload();
                }
            });
        });

        let btAdd = $("#btnAdd");
        let btEdit = $("#btnEdit");
        switch (id_access){
            case 1:
                btAdd.onclick = function(){
                    document.location.href = 'subject-add';
                };
                btEdit.onclick = function(){
                    document.location.href = 'subject-edit';
                };
                break;
            case 2:                                 //read only
                btAdd.removeClass("btn-outline-primary").addClass("btn-outline-secondary disabled");
                btEdit.removeClass("btn-outline-primary").addClass("btn-outline-secondary disabled");
        }


   });
})