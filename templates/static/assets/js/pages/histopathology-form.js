$(document).ready(function() {
    //[HYSTOPATOLOGY]

    let histo_actual_data = {
        db_oper: 0,               // 0 - none, 1 - insert, 2 - update
        id_subject_histo: 0,
        date: null,
        id_type: null,
    }
    // [hystopatology table]
    let histopathology_table = $('#histopathology-table').DataTable({
            dom: 'ftp',
            select: 'single',
            paging: true,
            info: false,
            processing: false,
            serverSide: false,
            buttons: [],
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false,
                },
                {
                    "targets": [ 1 ],
                    "visible": false,
                    "searchable": false,
                },
                {
                    "targets": [ 2 ],                        //date format
                    render: function (data) {
                        return moment(data.substring(0,16)).format('YYYY-MM-DD HH:mm');
                    },
                },
            ],
            ajax: {
                url:'histopathology-data',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: 'data',
            }
    });

    // [click on td]
    histopathology_table.on( 'select.dt', function ( e, dt, type, indexes ) {
        $('#btnHistoSubmit').text('Update');
        let data = dt.rows(indexes).data();
        $('#inputHistoDate').val(moment(data[0][2].substring(0,16)).format('YYYY-MM-DD HH:mm'));
        $('#inputHistoType').val(data[0][1]).trigger('change');
        histo_actual_data.db_oper = 2;
        histo_actual_data.id_subject_histo = data[0][0];
    });

    // [button Clear selection]
    $('#inputClearHisto').click(function(){
        $('#histopathology-table tbody tr').removeClass('selected')
        $('#btnHistoSubmit').text('Insert');
        $('#inputHistoDate').val(null);
        $('#inputHistoType').val("1").trigger('change');
        histo_actual_data.db_oper = 1;
        histo_actual_data.id_subject_histo = 0;
            $.ajax({
                url: 'histopathology-select',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: {
                    selected: 0,
                },
                success: function (data) {
                }
            });
    });


    let btSubmit = $('#btnHistoSubmit');
    btSubmit.on('click',function(){
        submit();
    });

    // switch (id_access){
    //     case 1:
    //             btSubmit.on('click',function(){
    //                 submit();
    //             });
    //             break;
    //     case 2:                                 //read only
    //             btSubmit.removeClass("btn-primary").addClass("btn-outline-secondary disabled");
    //             break;
    //     case 99:                                 //read only
    //             btSubmit.removeClass("btn-primary").addClass("btn-outline-secondary disabled");
    //             break;

    //     }

    function submit (){
        histo_actual_data.id_type = $('#inputHistoType').val();
        histo_actual_data.date = $('#inputHistoDate').val();

        $.ajax({
                url: 'histopathology-submit',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: histo_actual_data,
                success: function (data) {
                    switch(data.data) {
                        case 0:
                            notify('Database operation success', 'success');
                            histopathology_table.ajax.reload();
                            break;
                        case 1:
                            notify('Database operation error ' + data.message, 'danger');
                            break;
                        case 2:
                            notify('You do not have the permission to perform this operation', 'danger');
                            break;
                    }

                }
        });
    }
    /*
    $('#btnHistoSubmit').click(function() {
        histo_actual_data.name = $('#inputHistoName').val();
        histo_actual_data.date = $('#inputHistoDate').val();

        $.ajax({
                url: 'histopathology-submit',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: histo_actual_data,
                success: function (data) {
                    switch(data.data) {
                        case 0:
                            notify('Database operation error ' + data.message, 'danger');
                            break;
                        case 1:
                            notify('Database operation success', 'success');
                            histopathology_table.ajax.reload();
                            break;
                    }

                }
        });
    })
     */
});
