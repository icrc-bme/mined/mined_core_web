$(document).ready(function() {
    let outcome_actual_data = {
        db_oper: 0,               // 0 - none, 1 - insert, 2 - update
        id_subject_outcome: 0,
        date: null,
        ilae: null,
        engel: null,
        mchugh: null
    }

    // [outcome table]
    let outcome_table = $('#outcome-table').DataTable({
           dom: 'ftp',
           select: 'single',
           paging: true,
           info: false,
           processing: false,
           serverSide: false,
           buttons: [],
           columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false,
                },
                {
                    "targets": [ 1 ],                        //date format
                    render: function (data) {
                        return moment(data.substring(0,16)).format('YYYY-MM-DD HH:mm');
                    },
                },
            ],

           ajax: {
                url:'outcome-data',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: 'data',
           }
    });
        // [click on td]
    outcome_table.on( 'select.dt', function ( e, dt, type, indexes ) {
        $('#btnOutcomeSubmit').text('Update');
        let data = dt.rows(indexes).data();
        $('#inputOutcomeDate').val(moment(data[0][1].substring(0,16)).format('YYYY-MM-DD HH:mm'));
        $('#inputOutcomeIlae').val(data[0][2]).trigger('change');
        $('#inputOutcomeEngel').val(data[0][3]).trigger('change');
        $('#inputOutcomeMchugh').val(data[0][4]).trigger('change');
        outcome_actual_data.db_oper = 2;
        outcome_actual_data.id_subject_outcome = data[0][0];
    });

    $('#inputClearOutcome').click(function(){
        $('#outcome-table tbody tr').removeClass('selected')
        $('#btnOutcomeSubmit').text('Insert');
        $('#inputOutcomeDate').val(null);
        $('#inputOutcomeIlae').val("n/a").trigger('change');
        $('#inputOutcomeEngel').val("n/a").trigger('change');
        $('#inputOutcomeMchugh').val("n/a").trigger('change');
        outcome_actual_data.db_oper = 1;
        outcome_actual_data.id_subject_outcome = 0;
        $.ajax({
                url: 'outcome-select',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: {
                    selected: 0,
                },
                success: function (data) {
                }
        });
    });
    $('#btnOutcomeSubmit').click(function() {
        outcome_actual_data.date = $('#inputOutcomeDate').val();
        outcome_actual_data.engel = $('#inputOutcomeEngel').val();
        outcome_actual_data.ilae = $('#inputOutcomeIlae').val();
        outcome_actual_data.mchugh = $('#inputOutcomeMchugh').val();

        $.ajax({
                url: 'outcome-submit',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: outcome_actual_data,
                success: function (data) {
                    switch(data.data) {
                        case 0:
                            notify('Database operation success', 'success');
                            outcome_table.ajax.reload();
                            break;
                        case 1:
                            notify('Database operation error ' + data.message, 'danger');
                            break;
                        case 2:
                            notify('You do not have the permission to perform this operation', 'danger');
                            break;
                    }

                }
        });
    })


});
