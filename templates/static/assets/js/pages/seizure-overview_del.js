$(document).ready(function() {

    function getCSRFCookie(name) {
            var cookieValue = null;
            if (document.cookie && document.cookie !== '') {
                var cookies = document.cookie.split(';');
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = cookies[i].trim();
                    // Does this cookie string begin with the name we want?
                    if (cookie.substring(0, name.length + 1) === (name + '=')) {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }return cookieValue;
    }



    let table_seizure= $('#seizure-table').DataTable({
            select: 'single',
            paging: true,
            info: false,
            processing: false,
            serverSide: false,
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false,
                },
            ],

            ajax: {
                url:'seizure-data',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: 'data',
            }
    });

      // [ Individual Column Searching (Text Inputs) ]
        $('#seizure-table tfoot th').each(function() {
            let title = $(this).text();
            $(this).html('<input type="text" class="form-control f-12" placeholder="' + title + '" />');
        });


        // [ Apply the search ]
        table_seizure.columns().every(function() {
            let that = this;

            $('input', this.footer()).on('keyup change', function() {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });


    $('#seizure-table').on( 'select.dt', function ( e, dt, type, indexes ) {
        let data = dt.rows(indexes).data();
        $.ajax({
            url: 'seizure-select',
            type: 'POST',
            headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
            data: {
                selected: data[0][1],                   //id_channel
            },
                success: function (data) {
                }
            });
    });


    let table_channel= $('#channel-table').DataTable({
            select: 'single',
            paging: true,
            info: false,
            processing: false,
            serverSide: false,
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false,
                },
            ],

           keys: {
                    keys: [  38 /* UP */, 40 /* DOWN */ ]
            },

            ajax: {
                url:'channel-data',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: 'data',
            }
    });

      // [ Individual Column Searching (Text Inputs) ]
        $('#channel-table tfoot th').each(function() {
            let title = $(this).text();
            $(this).html('<input type="text" class="form-control f-12" placeholder="' + title + '" />');
        });


        // [ Apply the search ]
        table_channel.columns().every(function() {
            let that = this;

            $('input', this.footer()).on('keyup change', function() {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });


    $('#channel-table').on( 'select.dt', function ( e, dt, type, indexes ) {
        let data = dt.rows(indexes).data();
    });

    /* keyboard */

    $('#channel-table').on('click', 'tbody td', function(e){
                e.stopPropagation();
                // Get index of the clicked row
                let rowIdx = table_channel.cell(this).index().row;
                // Select row
                table_channel.row(rowIdx).select();
    });
            // [key focus td]
    $('#channel-table').on('key-focus.dt', function(e, datatable, cell){
            // Select highlighted row
            table_channel.row(cell.index().row).select();
    });





});