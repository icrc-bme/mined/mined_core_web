$(document).ready(function() {

    let id_inst = 0;
    let id_user = 0;
    let LDAP_Password = "";

    function clearUser() {
            $('#inputUserFull').val('');
            $('#inputUserInst').val(1).trigger('change');
            $('#inputUserContact').val('');
            $('#inputAccess').val(1).trigger('change');
            $("#inputUserName").val('');
            $("#inputPassword").val('');
            $("#inputPasswordRe").val('');

    }
    let institution_table = $('#institution-table').DataTable({
            dom: 't',
            select: 'single',
            paging: true,
            info: false,
            processing: false,
            serverSide: false,
            buttons: [],
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false,
                },
             ],
            ajax: {
                url:'institution-data',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: 'data',
            }
    });

    // [click on td]
    institution_table.on( 'select.dt', function ( e, dt, type, indexes ) {
        let data = dt.rows(indexes).data();
        if(data[0][0] !== 1) { //default institution cannot be deleted
            id_inst = data[0][0];
            $('#inputInstName').val(data[0][1]);
            $('#inputInstZone').val(data[0][2]).trigger('change');
            $('#w_del_inst').text(data[0][1]);
        }
    });

    $("#btnInstDelete").on('click',function(){

        let id = $('#modalInstDelete').data('id');
        $('[data-id=' + id + ']').remove();
        $('#modalInstDelete').modal('hide');

        $.ajax({
            url: 'institution-delete',
            type: 'POST',
            headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
            data:
            {
                id_inst: id_inst,
            },
                success: function (data) {
                    let nType;
                    let nMessage;
                    if(data.data[0] === 0) {
                        nMessage = 'Database operation success';
                        nType = 'success';
                    }
                    else{
                        nMessage = 'Institution delete error:   ' + '<br><h6 class="bg-danger text-black-50">' + data.data[1] + '</h6>';
                        nType = 'danger';
                    }
                    window.notify(nMessage,nType);
                    $('#institution-table').DataTable().ajax.reload();
                }
            })
        })

    $('#btnInstInsert').click(function(e){
        e.preventDefault();
        $.ajax({
            url: 'institution-insert',
            type: 'POST',
            headers: {'X-CSRFToken': window.getCSRFCookie('csrftoken')},
            data: {
                id_inst: 0,
                name: $("#inputInstName").val(),
                zone: $("#inputInstZone").val(),
            },
            success: function (data) {
                let nType;
                let nMessage;
                if(data.data[0] === 0) {
                   nMessage = 'Database operation success';
                   nType = 'success';
                }
                else{
                    nMessage = 'Institution save error:   ' + '<br><h6 class="bg-danger text-black-50">' + data.data[1] + '</h6>';
                    nType = 'danger';
                }
                window.notify(nMessage,nType);
                $('#institution-table').DataTable().ajax.reload();
            }
        });
    });

    $('#btnInstUpdate').click(function(e){
        e.preventDefault();
        $.ajax({
            url: 'institution-update',
            type: 'POST',
            headers: {'X-CSRFToken': window.getCSRFCookie('csrftoken')},
            data: {
                id_inst: id_inst,
                name: $("#inputInstName").val(),
                zone: $("#inputInstZone").val(),
            },
            success: function (data) {
                let nType;
                let nMessage;
                if(data.data[0] === 0) {
                   nMessage = 'Database operation success';
                   nType = 'success';
                }
                else{
                    nMessage = 'Institution:   ' + '<br><h6 class="bg-danger text-black-50">' + data.data[1] + '</h6>';
                    nType = 'danger';
                }
                window.notify(nMessage,nType);
                $('#institution-table').DataTable().ajax.reload();
            }
        });
    });
//-------------------------------------

    let user_table = $('#user-table').DataTable({
            dom: 't',
            select: 'single',
            paging: true,
            info: false,
            processing: false,
            serverSide: false,
            buttons: [],
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false,
                },
             ],
            ajax: {
                url:'user-data',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: 'data',
            }
    });

    // [click on td]
    user_table.on( 'select.dt', function ( e, dt, type, indexes ) {
        let data = dt.rows(indexes).data();
        console.log(data[0]);
        if(data[0][0] !== 1) { //default institution cannot be deleted
            id_user = data[0][0];
            //$('#inputUserName').val(data[0][1]);
            $('#inputUserFull').val(data[0][1]);
            $('#inputUserInst').val(data[0][2]).trigger('change');
            $('#inputUserContact').val(data[0][3]);
            $('#inputAccess').val(data[0][4]).trigger('change');
            $('#w_del_user').text(data[0][1]);
        }
    });

    $('#btnUserInsert').click(function(e){
        if ($("#inputLDAP"))
        {
            LDAP_Password = $("#inputLDAP").val();
        }
        e.preventDefault();
        $.ajax({
            url: 'user-insert',
            type: 'POST',
            headers: {'X-CSRFToken': window.getCSRFCookie('csrftoken')},
            data: {
                LDAP_Password:LDAP_Password,
                id_user: 0,
                full_name: $("#inputUserFull").val(),
                institution: $("#inputUserInst").val(),
                contact: $("#inputUserContact").val(),
                access: $("#inputAccess").val(),
                user_name: $("#inputUserName").val(),
                password: $("#inputPassword").val(),
                password_re: $("#inputPasswordRe").val(),

            },
            success: function (data) {
                let nType;
                let nMessage;
                if(data.data[0] === 0) {
                   nMessage = 'Database operation success';
                   nType = 'success';
                }
                else{
                    nMessage = 'User save error:   ' + '<br><h6 class="bg-danger text-black-50">' + data.data[1] + '</h6>';
                    nType = 'danger';
                }
                window.notify(nMessage,nType);
                $('#user-table').DataTable().ajax.reload();
                clearUser();
            }
        });
    });

    $('#btnUserUpdate').click(function(e){
        if ($("#inputLDAP"))
        {
            LDAP_Password = $("#inputLDAP").val();
        }

        e.preventDefault();
        $.ajax({
            url: 'user-update',
            type: 'POST',
            headers: {'X-CSRFToken': window.getCSRFCookie('csrftoken')},
            data: {
                LDAP_Password:LDAP_Password,
                id_user: id_user,
                full_name: $("#inputUserFull").val(),
                institution: $("#inputUserInst").val(),
                contact: $("#inputUserContact").val(),
                access: $("#inputAccess").val(),
            },
            success: function (data) {
                let nType;
                let nMessage;
                if(data.data[0] === 0) {
                   nMessage = 'Database operation success';
                   nType = 'success';
                }
                else{
                    nMessage = 'User save error:   ' + '<br><h6 class="bg-danger text-black-50">' + data.data[1] + '</h6>';
                    nType = 'danger';
                }
                window.notify(nMessage,nType);
                $('#user-table').DataTable().ajax.reload();
            }
        });
    });

    $('#btnUserDelete').click(function(e){
        let id = $('#modalUserDelete').data('id');
        $('[data-id=' + id + ']').remove();
        $('#modalUserDelete').modal('hide');
        if ($("#inputLDAP"))
        {
            LDAP_Password = $("#inputLDAP").val();
        }

        $.ajax({
            url: 'user-delete',
            type: 'POST',
            headers: {'X-CSRFToken': window.getCSRFCookie('csrftoken')},
            data: {
                LDAP_Password:LDAP_Password,
                id_user: id_user,
            },
            success: function (data) {
                let nType;
                let nMessage;
                if(data.data[0] === 0) {
                   nMessage = 'Database operation success';
                   nType = 'success';
                }
                else{
                    nMessage = 'User delete error:   ' + '<br><h6 class="bg-danger text-black-50">' + data.data[1] + '</h6>';
                    nType = 'danger';
                }
                window.notify(nMessage,nType);
                $('#user-table').DataTable().ajax.reload();
                clearUser();
            }
        });
    });

});
