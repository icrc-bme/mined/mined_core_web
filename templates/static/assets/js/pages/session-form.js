$(document).ready(function() {

    //actual data
        // [ Ip masking ]  mask is not functopn
//    $('.ip').mask('ZZZ.ZZZ.ZZZ.ZZZ',{ translation: { 'Z': { pattern: /[0-9]/, optional: true } } });

    let id_annotation = 0;

    $('textarea.max-textarea').maxlength({
        alwaysShow: true
    });

    let btSessionSubmit = $('#btnSessionSubmit');
    btSessionSubmit.on('click',function(){
        sessionSubmit();
    });
    // switch (id_access){
    //     case 1:
    //             btSessionSubmit.on('click',function(){
    //                 sessionSubmit();
    //             });
    //     break;
    //     case 2:                                 //read only
    //             btSessionSubmit.removeClass("btn-primary").addClass("btn-outline-secondary disabled");
    //     break;
    //     case 99:                                 //read only
    //             btSessionSubmit.removeClass("btn-primary").addClass("btn-outline-secondary disabled");
    //     break;

    //     }

    function sessionSubmit() {
        $.ajax({
            url: 'session-update',
            type: 'POST',
            headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
            data: {
                name: $("#inputName").val(),
                type: $("#inputRecordType").val(),
                device: $("#inputDevice").val(),
                modality: $("#inputModality").val(),
                reference: $("#inputReference").val(),
                ip_address: $("#inputIpAddress").val(),
                note: $("#inputNote").val(),
            },
            success: function (data) {
                switch (data.data) {
                    case 0:
                        notify('Database operation success', 'success');
                        break;
                    case 1:
                        notify('Database operation error ' + data.message, 'danger');
                        break;
                    case 2:
                        notify('You do not have the permission to perform this operation', 'danger');
                        break;
                }

            }
        });
    }

    // ---------------- annotation ----------------

    let annotation_table = $('#annotation-table').DataTable({
            dom: 'tp',
            select: 'single',
            paging: true,
            info: false,
            processing: false,
            serverSide: false,
            buttons: [],
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false,
                },
                {
                    "targets": [ 1 ],                        //date format
                    render: function (data) {
                        return moment(data.substring(0,16)).format('YYYY-MM-DD HH:mm');
                    },
                },

                {
                    "targets": [ 2 ],                        //date format
                    render: function (data) {
                        return moment(data.substring(0,16)).format('YYYY-MM-DD HH:mm');
                    },
                },

            ],
            // keys: {
            //         keys: [  38  UP , 40 /* DOWN */ ]
            // },

            ajax: {
                url:'session-annotation-data',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: 'data',
            },
            // initComplete: function() {
            //     $('.checkbox-channel:checked').each(function(){                //remove last checked
            //        $(this).prop('checked', false);
            //     })
            // },


    });

    annotation_table.on( 'select.dt', function ( e, dt, type, indexes ) {
        let data = dt.rows(indexes).data();
        id_annotation = data[0][0]
        $('#inputStart').val(moment(data[0][1].substring(0,16)).format('YYYY-MM-DD HH:mm'));
        $('#inputStop').val(moment(data[0][2].substring(0,16)).format('YYYY-MM-DD HH:mm'));
        $('#inputText').val(data[0][3]);
        $('#inputType').val(data[0][4]).trigger('change');
        // Set delay for the below code to let the update_subtype() function to execute
        setTimeout(function(){ 
           $('#inputSubtype').val(data[0][5]).trigger('change');
        },100);
        $('#btnAnnotationSubmit').text('Update')

    });

    let ddTypeSelect = $('#inputType');
    ddTypeSelect.on('change',function(){
                    update_subtype();
                });

    function update_subtype() {

        selected_type = $('#inputType').val();

        $.ajax({
                url: 'session-annotation-subtype-select',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: {'selected_type': selected_type},
                success: function (data) {

                    // Remove old options from the dropdown
                    let ddSubtypeSelect = $('#inputSubtype');
                    var option_value = null;
                    var i, L = ddSubtypeSelect.get(0).options.length;

                    // Delete old values
                    for(i = L; i >= 0; i--) {
                       ddSubtypeSelect.get(0).options.remove(i);
                    }

                    // Insert new options
                    var i , L = data.data.length;
                    ddSubtypeSelect.append("<option value=\"" + data.data[0] + "\">" + data.data[0] + "</option>");
                    for(i = 1; i < L; i++) {
                       ddSubtypeSelect.append("<option value=\"" + data.data[i] + "\">" + data.data[i] + "</option>");
                    }
                }
        });  
    }

    // Trugger when the page is loaded
    update_subtype();

    // [key focus td]
    annotation_table.on('key-focus.dt', function(e, datatable, cell){
            // Select highlighted row
        annotation_table.row(cell.index().row).select();
    });

    annotation_table.on('click', 'tbody td', function(e){
        e.stopPropagation();
        let rowIdx = annotation_table.cell(this).index().row;
        // Select row
        annotation_table.row(rowIdx).select();
    });

    $('#inputClearAnnotation').click(function() {
        $('#btnAnnotationSubmit').text('Insert');
        $('#inputStart').val(null);
        $('#inputStop').val(null);
        $('#inputText').val(null);
        id_annotation = 0;
    })


    let btAnnotSubmit = $('#btnAnnotationSubmit');
    btAnnotSubmit.on('click',function(){
                    annotationSubmit();
                });

    function annotationSubmit()
    {
        let annotation_data = {}
        annotation_data.id = id_annotation;
        annotation_data.start = $('#inputStart').val();
        annotation_data.stop = $('#inputStop').val();
        annotation_data.text = $('#inputText').val();
        annotation_data.annotation_type = $('#inputType').val();
        annotation_data.annotation_subtype = $('#inputSubtype').val();

        $.ajax({
                url: 'session-annotation-submit',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: annotation_data,
                success: function (data) {
                    switch(data.data) {
                        case 0:
                            notify('Database operation success', 'success');
                            annotation_table.ajax.reload();
                            break;
                        case 1:
                            notify('Database operation error ' + data.message, 'danger');
                            break;
                        case 2:
                            notify('You do not have the permission to perform this operation', 'danger');
                            break;
                    }

                }
        });
    }
});