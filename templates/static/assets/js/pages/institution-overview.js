$(document).ready(function() {
    setTimeout(function() {

        let institution_table=$('#institution-table').DataTable({
            dom: 'tip',
            select: 'single',
            processing: false,
            serverSide: false,
            order: [[ 1, "asc" ]],
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false,
                },
             ],

            ajax: {
                url:'institution-data',
                type: 'POST',
                headers: {'X-CSRFToken': window.getCSRFCookie('csrftoken')},
                data: 'data',
            },
            initComplete: function(){
            },

        });
        institution_table.on( 'select.dt', function ( e, dt, type, indexes ) {
            let data = dt.rows(indexes).data();
            $.ajax({
                url: 'institution-select',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: {
                    selected: data[0][0],
                },
                success: function () {
                    document.getElementById('nameInstitution').innerHTML = data[0][0];
                    document.getElementById('nameTimeZone').innerHTML = data[0][1];
                }
            });
        });


   });
})