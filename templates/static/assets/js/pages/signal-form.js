$(document).ready(function() {

    let insert = true;


    let annotation_table = $('#annotation-table').DataTable({
            dom: 'rtpi',
            select: 'multi',
            paging: true,
            info: false,
            processing: false,
            serverSide: false,
            buttons: [],
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false,
                    //className: 'noVis'
                },
                {
                    "targets": [ 2 ],                        //date format
                    render: function (data) {
                        return moment(data.substring(0,16)).format('YYYY-MM-DD HH:mm');
                    },
                },

                {
                    "targets": [ 3 ],                        //date format
                    render: function (data) {
                        return moment(data.substring(0,16)).format('YYYY-MM-DD HH:mm');
                    },
                },

            ],
            keys: {
                    keys: [  38 /* UP */, 40 /* DOWN */ ]
            },

            ajax: {
                url:'channel-annotation-data',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: 'data',
            },
            // initComplete: function() {
            //     $('.checkbox-channel:checked').each(function(){                //remove last checked
            //        $(this).prop('checked', false);
            //     })
            // },


    });


    annotation_table.on( 'select.dt', function ( e, dt, type, indexes ) {
        $('#channel_select').fadeOut();
        insert = false;
        $('#btnAnnotationSubmit').text('Update');
        let data = dt.rows(indexes).data();
        $('#inputStart').val(moment(data[0][2].substring(0,16)).format('YYYY-MM-DD HH:mm'));
        $('#inputStop').val(moment(data[0][3].substring(0,16)).format('YYYY-MM-DD HH:mm'));
        $('#inputText').val(data[0][4]);
        $('#inputType').val(data[0][5]).trigger('change');
        // Set delay for the below code to let the update_subtype() function to execute
        setTimeout(function(){ 
           $('#inputSubtype').val(data[0][6]).trigger('change');
        },100);
        $('#btnAnnotationSubmit').text('Update')
    });

    let ddTypeSelect = $('#inputType');
    ddTypeSelect.on('change',function(){
                    update_subtype();
                });

    function update_subtype() {

        selected_type = $('#inputType').val();

        $.ajax({
                url: 'channel-annotation-subtype-select',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: {'selected_type': selected_type},
                success: function (data) {

                    // Remove old options from the dropdown
                    let ddSubtypeSelect = $('#inputSubtype');
                    var option_value = null;
                    var i, L = ddSubtypeSelect.get(0).options.length;

                    // Delete old values
                    for(i = L; i >= 0; i--) {
                       ddSubtypeSelect.get(0).options.remove(i);
                    }

                    // Insert new options
                    var i , L = data.data.length;
                    ddSubtypeSelect.append("<option value=\"" + data.data[0] + "\">" + data.data[0] + "</option>");
                    for(i = 1; i < L; i++) {
                       ddSubtypeSelect.append("<option value=\"" + data.data[i] + "\">" + data.data[i] + "</option>");
                    }
                }
        });  
    }

    // Trugger when the page is loaded
    update_subtype();



    // [ Individual Column Searching (Text Inputs) ]
    $('#annotation-table tfoot th').each(function() {
            var title = $(this).text();
            $(this).html('<input type="text" class="form-control text-sm-center" placeholder="Search ' + title + '" />');
        });
        // [ Apply the search ]
    annotation_table.columns().every(function() {
            var that = this;

            $('input', this.footer()).on('keyup change', function() {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });

    $('#inputClearAnnotation').click(function(){
        $('#channel_select').fadeIn();
        insert = true;
        $('#annotation-table tbody tr').removeClass('selected')
        $('#btnAnnotationSubmit').text('Insert');
        $('#inputStart').val(null);
        $('#inputStop').val(null);
        $('.checkbox-channel:checked').each(function(){
                    $(this).prop('checked', false);
        })
        annotation_table.rows({selected:true}).deselect();
        annotation_table.ajax.reload();
        console.log("Clear selection called");

    });

    let btAnnotSubmit = $('#btnAnnotationSubmit');
    btAnnotSubmit.on('click',function(){
        annotSubmit();
    });
    // switch (id_access){
    //     case 1:
    //             btAnnotSubmit.on('click',function(){
    //                 annotSubmit();
    //             });
    //     break;
    //     case 2:                                 //read only
    //             btAnnotSubmit.removeClass("btn-primary").addClass("btn-outline-secondary disabled");
    //     break;
    //     case 99:                                 //read only
    //             btAnnotSubmit.removeClass("btn-primary").addClass("btn-outline-secondary disabled");
    //             break;

    // }


    function annotSubmit(){
        let ids_insert = {};
        let ids_update = {};
        let i = 0;
        if (insert) {
            $('.checkbox-channel:checked').each(function () {
                ids_insert[i] = ($(this).attr('name'));
                i = i + 1;
            })
        }
        else {
            let rows_selected = annotation_table.column(0).checkboxes.selected();
            $.each(rows_selected, function(index, rowId) {
                ids_update[i] = rowId
                i = i + 1
            });
        }
       $.ajax({
                url: 'channel-annotation-submit',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: {
                    insert: insert.toString(),
                    ids_insert: ids_insert,
                    ids_update: ids_update,
                    start: $('#inputStart').val(),
                    stop: $('#inputStop').val(),
                    text: $('#inputText').val(),
                    type: $('#inputType').val(),
                    subtype: $('#inputSubtype').val()
                },
                success: function (data) {
                    switch(data.data) {
                        case 0:
                            notify('Database operation success', 'success');
                            annotation_table.ajax.reload();
                            break;
                        case 1:
                            notify('Database operation error ' + data.message, 'danger');
                            break;
                        case 2:
                            notify(data.message, 'warning');
                            break;
                        case 3:
                            notify('You do not have the permission to perform this operation', 'danger');
                            break;
                    }

                }
       });
    };



//     let seizure_table = $('#seizure-table').DataTable({
//             dom: 'rtip',
//             select: 'multi',
//             paging: true,
//             info: true,
//             processing: false,
//             serverSide: false,
//             columnDefs: [
//                 {
//                     "targets": [ 0 ],
//                     "checkboxes":{
//                         'selectRow':true
//                     },
//                     "searchable": false,
//                 },
//                 {
//                     "targets": [ 2 ],                        //date format
//                     render: function (data) {
//                         return moment(data.substring(0,16)).format('YYYY-MM-DD HH:mm');
//                     },
//                 },

//                 {
//                     "targets": [ 3 ],                        //date format
//                     render: function (data) {
//                         return moment(data.substring(0,16)).format('YYYY-MM-DD HH:mm');
//                     },
//                 },

//             ],
//             keys: {
//                     keys: [  38 /* UP */, 40 /* DOWN */ ]
//             },

//             ajax: {
//                 url:'seizure-data',
//                 type: 'POST',
//                 headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
//                 data: 'data',
//             },
//             initComplete: function() {
//                 $('.checkbox-channel:checked').each(function(){                //remove last chhecked
//                    $(this).prop('checked', false);
//                 })
//                 /*
//                 $('#seizure-table tbody td:eq(0)').click();
// */
// /*
//                 this.api().columns().every(function() {
//                     columnOption(this);
//                 });
// */
//             },
//     });
//         // [ Individual Column Searching (Text Inputs) ]
//     $('#seizure-table tfoot th').each(function() {
//             var title = $(this).text();
//             $(this).html('<input type="text" class="form-control text-sm-center" placeholder="Search ' + title + '" />');
//         });
//         // [ Apply the search ]
//     seizure_table.columns().every(function() {
//             var that = this;

//             $('input', this.footer()).on('keyup change', function() {
//                 if (that.search() !== this.value) {
//                     that
//                         .search(this.value)
//                         .draw();
//                 }
//             });
//         });

//     seizure_table.on( 'select.dt', function ( e, dt, type, indexes ) {
//         $('#btnSeizureSubmit').text('Update');
//         $('#channel_select').fadeOut();
//         insert = false;
//         let data = dt.rows(indexes).data();
//         $('#inputStart').val(moment(data[0][2].substring(0,16)).format('YYYY-MM-DD HH:mm'));
//         $('#inputStop').val(moment(data[0][3].substring(0,16)).format('YYYY-MM-DD HH:mm'));
//         $('#inputText').val(data[0][4]);
//         $('#inputType').val(data[0][5]).trigger('change');
//         $('#inputChannelType').val(data[0][6]).trigger('change');
//     });

//     $('#inputClearSeizure').click(function(){

//         $('#channel_select').fadeIn();
//         insert = true;
//         $('#annotation-table tbody tr').removeClass('selected')
//         $('#btnSeizureSubmit').text('Insert');
//         $('#inputStart').val(null);
//         $('#inputStop').val(null);
//         $('#inputText').val(null);
//         $('#inputType').val('n/a').trigger('change');
//         $('#inputChannelType').val('n/a').trigger('change');
//         $('.checkbox-channel:checked').each(function(){
//                     $(this).prop('checked', false);
//         })
//         seizure_table.rows({selected:true}).deselect();
//     });


//     let btSeizureSubmit = $('#btnSeizureSubmit');
//     switch (id_access){
//         case 1:
//                 btSeizureSubmit.on('click',function(){
//                     seizureSubmit();
//                 });
//                 break;
//         case 2:                                 //read only
//                 btSeizureSubmit.removeClass("btn-primary").addClass("btn-outline-secondary disabled");
//         break;
//         case 99:                                 //read only
//                 btSeizureSubmit.removeClass("btn-primary").addClass("btn-outline-secondary disabled");
//         break;

//     }

//     function seizureSubmit(){
//         let ids_insert = {};
//         let ids_update = {};
//         let i = 0;
//         if (insert) {
//             $('.checkbox-channel:checked').each(function () {
//                 ids_insert[i] = ($(this).attr('name'));
//                 i = i + 1;
//             })
//         }
//         else {
//             let rows_selected = seizure_table.column(0).checkboxes.selected();
//             $.each(rows_selected, function(index, rowId) {
//                 ids_update[i] = rowId
//                 i = i + 1
//             });
//         }
//        $.ajax({
//                 url: 'seizure-submit',
//                 type: 'POST',
//                 headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
//                 data: {
//                     insert: insert.toString(),
//                     ids_insert: ids_insert,
//                     ids_update: ids_update,
//                     start: $('#inputStart').val(),
//                     stop: $('#inputStop').val(),
//                     seizure_type: $('#inputType').val(),
//                     channel_type: $('#inputChannelType').val(),
//                     text: $('#inputText').val(),
//                 },
//                 success: function (data) {
//                     switch(data.data) {
//                         case 0:
//                             notify('Database operation error ' + data.message, 'danger');
//                             break;
//                         case 1:
//                             notify('Database operation success', 'success');
//                             seizure_table.ajax.reload();
//                             break;
//                     }

//                 }
//        });
//     }

});