$(document).ready(function() {

        function getCSRFCookie(name) {
            var cookieValue = null;
            if (document.cookie && document.cookie !== '') {
                var cookies = document.cookie.split(';');
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = cookies[i].trim();
                    // Does this cookie string begin with the name we want?
                    if (cookie.substring(0, name.length + 1) === (name + '=')) {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }return cookieValue;
        }

        $('#histopathology-table').DataTable({
            select: 'single',
            paging: false,
            info: false,
            processing: false,
            serverSide: false,
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false,
                },
            ],

            ajax: {
                url:'histopathology-data',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: 'data',
            }
        });

            // [click on td]
        $('#histopathology-table').on( 'select.dt', function ( e, dt, type, indexes ) {
            let data = dt.rows(indexes).data();
            console.log(data);
/*
            $.ajax({
                url: 'subject-select',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: {
                    selected: data[0][0],                   //id_mined
                },
                success: function (data) {
                    var data_array = Object.values(data);
                    console.log(data_array);
                    document.getElementById('id_subject').innerHTML='Subject: '+ data_array[0][0];  //returned by django
                    document.getElementById('session_count').innerHTML=data_array[0][1];
                }
            });

 */
         });


        $('#intervention-table').DataTable({
            select: 'single',
            paging: false,
            info: false,
            processing: false,
            serverSide: false,
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false,
                },
                {
                    "targets": [ 1 ],
                    "visible": false,
                    "searchable": false,
                },
            ],

            ajax: {
                url:'intervention-data',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: 'data',
            }
        });
        $('#outcome-table').DataTable({
            select: 'single',
            paging: false,
            info: false,
            processing: false,
            serverSide: false,
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false,
                },
            ],

            ajax: {
                url:'outcome-data',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: 'data',
            }
        });

})