$(document).ready(function() {
    setTimeout(function() {

        let contact_render = false;
        let atlas_render = false;

        let table_electrode = $('#electrode-table').DataTable({
            dom: 'frtip',
            select: 'multiple',
            buttons: ['copy', 'csv', 'excel', 'pdf', 'print', 'colvis'],
            processing: false,
            serverSide: false,
            keys: {
                    keys: [ 38 /* UP */, 40 /* DOWN */ ]
            },
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false,
                    className: 'noVis'
                },
            ],
            ajax: {
                url:'electrode-data',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: 'data',
            },
            initComplete: function(setting, json){
               $('#table_electrode tbody td:eq(0)').click();
             }
        });

        table_electrode.ajax.reload( function (json) {
            $('#electrode-table tbody td:eq(0)').click();
        } );


        // [ Individual Column Searching (Text Inputs) ]
        $('#electrode-table tfoot th').each(function() {
            let title = $(this).text();
            $(this).html('<input type="text" class="form-control f-12" placeholder="Search ' + title + '" />');
        });


        // [ Apply the search ]
        table_electrode.columns().every(function() {
            let that = this;

            $('input', this.footer()).on('keyup change', function() {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });
/*      not applicable with multiselect

/*
        table_electrode.on( 'select.dt', function ( e, dt, type, indexes ) {
            let data = dt.rows(indexes).data();
            let ids={};
            ids = $.map(table_electrode.rows('.selected').data(), function (item) {
                return item[0]
            });

            console.log('select');
            console.log(ids);
            console.log(data[0]);
            $.ajax({
                url: 'electrode-select',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: {
                    selected: data[0][0],
                },
                success: function (data) {
                    var data_array = Object.values(data);
//                    console.log(data_array[0][0]);
                    document.getElementById('contact_id').innerHTML=data_array[0][0];
                    document.getElementById('electrode_id').innerHTML=data_array[0][1];
                    document.getElementById('contact_name').innerHTML=data_array[0][2];
                    document.getElementById('electrode_name').innerHTML=data_array[0][3];
                    table_contact.ajax.reload()
                }
            });

        });

        // [key focus td]
        table_electrode.on('key-focus.dt', function(e, datatable, cell){
            // Select highlighted row
            table_electrode.row(cell.index().row).select();
        });
        table_electrode.on('click', 'tbody td', function(e){
                e.stopPropagation();
                // Get index of the clicked row
               let rowIdx = table_electrode.cell(this).index().row;

                 // Select row
                table_electrode.row(rowIdx).select();
         });
*/
        table_electrode.on('click', 'tbody tr', function(e){
            $(this).toggleClass('selected');
            let ids={};
            ids = $.map(table_electrode.rows('.selected').data(), function (item) {
                return item[0]
            });
            let json_ids = JSON.stringify(ids);

            $.ajax({
                url: 'electrode-multiple',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: {
                    selected: ids,
                },
                success: function (data) {
                    let data_array = Object.values(data);
//                    console.log(data_array[0][0]);
                    document.getElementById('contact_id').innerHTML=data_array[0][0];
                    document.getElementById('electrode_id').innerHTML=data_array[0][1];
                    document.getElementById('contact_name').innerHTML=data_array[0][2];
                    document.getElementById('electrode_name').innerHTML=data_array[0][3];
                    table_contact.ajax.reload()
                }
            });
        });




        let table_contact = $('#contact-table').DataTable({
            dom: 'frtip',
            select: 'single',
            buttons: ['copy', 'csv', 'excel', 'pdf', 'print', 'colvis'],
            processing: false,
            serverSide: false,
            order: [[1,'asc']],

            keys: {
                    keys: [  38 /* UP */, 40 /* DOWN */ ]
            },
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false,
                    className: 'noVis'
                },
                {
                    "targets": [ 6 ],
                    render: function (data) {
                        if (!atlas_render) {
                            atlas_render = true;
                            if(data===null)
                                $('#atlas').attr('hidden','hidden');
                            else
                                $('#atlas').removeAttr('hidden');
                        }
                        return data;
                    },
                },
                {
                    "targets": [ 7 ],
                    render: function (data) {
                        if (!contact_render) {
                            contact_render = true;
                            if (data === null)
                                $('#coordinate').attr('hidden', 'hidden');
                            else
                                $('#coordinate').removeAttr('hidden');
                        }
                        return data;
                    },
                },

            ],

            ajax: {
                url:'contact-data',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: function(d) {
                        Object.assign(d, {
                            atlas:$('#inputAtlas').val(),
                            coordinate_sys:$('#inputCoordinate').val()
                        })
                        return d;
                },
            },

            initComplete: function(settings, json) {
                 $('#coordinate').attr('hidden','hidden');
                 $('#contact-table tbody td:eq(0)').click();

                this.api().column(5).every(function() {
                    columnOption(this);
                });


                this.api().column(6).every(function() {
                    columnOption(this);
                });

                this.api().column(7).every(function() {
                    columnOption(this);
                });
            },
        });

        table_contact.ajax.reload( function (json) {
            $('#contact-table tbody td:eq(0)').click();
        } );

/*
        table_contact.button().add( 0, {
                action: function ( e, dt, button, config ) {
                dt.ajax.reload();
                },
                text: 'Reload table'
        } );

        // [button Detail - bottom]
        new $.fn.dataTable.Buttons(table_contact, {
            buttons: [{
                dom:{
                    tag: 'button',
                    className: 'btn btn-primary',
                },
                text: 'Detail',
                action: function(e, dt, node, config) {
                          document.location.href = "contact-detail";
                          //  [ see [click on td] ]

                          //let data = table.rows({selected: true}).data();
                          //console.log("data---"+data[0][0]);
                          //document.location.href = "contact-detail?id="+ data[0][0];

                    }
                },
            ]
        });
        table_contact.buttons(1, null).container().appendTo(
            table_contact.table().container()
        );
*/
          // [click on td]
        table_contact.on( 'select.dt', function ( e, dt, type, indexes ) {
            let data = dt.rows(indexes).data();
            $.ajax({
                url: 'contact-select',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: {
                    selected: data[0][0],
                },
                success: function (data) {
                    var data_array = Object.values(data);
  //                  console.log(data_array[0][0]);
                    document.getElementById('contact_id').innerHTML=data_array[0][0];
                    document.getElementById('electrode_id').innerHTML=data_array[0][1];
                    document.getElementById('contact_name').innerHTML=data_array[0][2];
                    document.getElementById('electrode_name').innerHTML=data_array[0][3];
                }
            });
        });

        table_contact.on('click', 'tbody td', function(e){
            try {
                e.stopPropagation();
                // Get index of the clicked row
                let rowIdx = table_contact.cell(this).index().row;
                // Select row
                table_contact.row(rowIdx).select();
            }
            catch (err){
       //         console.log("click error")
            }
         });


        // [key focus td]
        table_contact.on('key-focus.dt', function(e, datatable, cell){
            // Select highlighted row
            table_contact.row(cell.index().row).select();
        });


        // [ Individual Column Searching (Text Inputs) ]
        $('#contact-table tfoot th').each(function() {
            let title = $(this).text();
 //           $(this).html('<input type="text" class="form-control" placeholder="Search ' + title + '" />');
            $(this).html('<input type="text" class="form-control f-12" placeholder="' + title + '" />');
        });

        table_contact.buttons(1, null).container().appendTo(
            table_contact.table().container()
        );

        // [ Apply the search ]
        table_contact.columns().every(function() {
            let that = this;

            $('input', this.footer()).on('keyup change', function() {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });

        $("#inputAtlas").change(function() {
            table_contact.ajax.reload();
        });
        $("#inputCoordinate").change(function() {
            table_contact.ajax.reload();
        });
    });
});