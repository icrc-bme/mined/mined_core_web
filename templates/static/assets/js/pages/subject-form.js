$(document).ready(function() {
    $.ajax({
        url: 'subject-actual-data',
        type: 'POST',
        headers: {'X-CSRFToken': window.getCSRFCookie('csrftoken')},
        data:
            {
            },
        success: function (data) {
            if(data.form_type === 'edit') {
                $("#inputIdInternal").val(data.id_internal);
                $("#inputType").val(data.type_name).trigger('change');
                $("#inputSex").val(data.sex_name).trigger('change');
                $("#inputBirth").val(data.birth.substring(0, 10)); //.format('YYYY-MM-DD')),
                $("#inputHandedness").val(data.handedness_name).trigger('change');
                $("#inputSeizure").val(data.seizure);
                $("#inputNote").val(data.note);
            }
        }
    });


    $('textarea.max-textarea').maxlength({
        alwaysShow: true
    });

    $('#inputIdInternal').on('input', function(ev) {
        if($(this).val())
            $.ajax({
                url: 'internal-id-select',
                data: {
                    selected: $(this).val(),
                },
                type: 'post',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                success: function(data) {                                           //data is internal id count
                    console.log(data)
                    if(data.data > 0 )                                              //this internal id exists
                        document.getElementById('labelIdInternal').innerHTML = '\tInstitution internal id is used';
                    else
                        document.getElementById('labelIdInternal').innerHTML = '';
            }
        });
    });



    let btInsert = $('#btnSubjectInsert');
    let btUpdate = $('#btnSubjectUpdate');
    btInsert.on('click',function(){
        insert();
    });
    btUpdate.on('click',function(){
        update();
    });
    // switch (id_access){
    //     case 1:
    //             btInsert.on('click',function(){
    //                 insert();
    //             });
    //             btUpdate.on('click',function(){
    //                 update();
    //             });
    //     break;
    //     case 2:                                 //read only
    //             btInsert.removeClass("btn-primary").addClass("btn-outline-secondary disabled");
    //             btUpdate.removeClass("btn-primary").addClass("btn-outline-secondary disabled");
    //     break;
    //     case 99:                                 //read only
    //             btInsert.removeClass("btn-primary").addClass("btn-outline-secondary disabled");
    //             btUpdate.removeClass("btn-primary").addClass("btn-outline-secondary disabled");
    //     break;

    //     }


    function insert()
    {
        $.ajax({
                url: 'subject-insert',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: {
                    id_internal: $("#inputIdInternal").val(),
                    type: $("#inputType").val(),
                    sex: $("#inputSex").val(),
                    birth: $("#inputBirth").val(),
                    handedness: $("#inputHandedness").val(),
                    seizure: $("#inputSeizure").val(),
                    note: $("#inputNote").val(),
                },
                success: function (data) {
                    switch(data.data) {
                        case 0:
                            notify('Database operation success', 'success');
                            break;
                        case 1:
                            notify('Database operation error ' + data.message, 'danger');
                            break;
                        case 2:
                            notify('You do not have the permission to perform this operation', 'danger');
                            break;
                    }

                }
        });
    }

    function update() {
        $.ajax({
            url: 'subject-update',
            type: 'POST',
            headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
            data: {
                id_internal: $("#inputIdInternal").val(),
                type: $("#inputType").val(),
                sex: $("#inputSex").val(),
                birth: $("#inputBirth").val(),
                handedness: $("#inputHandedness").val(),
                seizure: $("#inputSeizure").val(),
                note: $("#inputNote").val(),
            },
            success: function (data) {
                switch (data.data) {
                    case 0:
                        notify('Database operation success', 'success');
                        break;
                    case 1:
                        notify('Database operation error ' + data.message, 'danger');
                        break;
                    case 2:
                        notify('You do not have the permission to perform this operation', 'danger');
                        break;
                }

            }
        });
    }

 /*
    $('#btnSubjectInsert').on('click',function(){
        $.ajax({
                url: 'subject-insert',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: {
                    id_internal: $("#inputIdInternal").val(),
                    type: $("#inputType").val(),
                    sex: $("#inputSex").val(),
                    birth: $("#inputBirth").val(),
                    handedness: $("#inputHandedness").val(),
                    seizure: $("#inputSeizure").val(),
                    note: $("#inputNote").val(),
                },
                success: function (data) {
                    switch(data.data) {
                        case 0:
                            notify('Database operation error ' + data.message, 'danger');
                            break;
                        case 1:
                            notify('Database operation success', 'success');
                            break;
                    }

                }
        });
    });

    $('#btnSubjectUpdate').on('click',function(){
        $.ajax({
                url: 'subject-update',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: {
                    id_internal: $("#inputIdInternal").val(),
                    type: $("#inputType").val(),
                    sex: $("#inputSex").val(),
                    birth: $("#inputBirth").val(),
                    handedness: $("#inputHandedness").val(),
                    seizure: $("#inputSeizure").val(),
                    note: $("#inputNote").val(),
                },
                success: function (data) {
                    switch(data.data) {
                        case 0:
                            notify('Database operation error ' + data.message, 'danger');
                            break;
                        case 1:
                            notify('Database operation success', 'success');
                            break;
                    }

                }
        });
    });

  */
});