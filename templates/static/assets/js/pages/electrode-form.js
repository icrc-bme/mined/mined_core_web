$(document).ready(function() {



    let id_electrode_type = 0;
    let selected_row = 0;
    let electrode_name = "";

    let table_electrode_type= $('#electrode-type-table').DataTable({
            dom: 'frtip',
            select: 'single',
            paging: true,
            pageLength: 5,
            info: false,
            processing: false,
            serverSide: false,
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false,
                },
            ],

            keys: {
                    keys: [ 38 /* UP */, 40 /* DOWN */ ]
            },



            ajax: {
                url:'electrode-type-data',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: 'data',
            },

            initComplete: function(setting, json){
                $('#electrode-type-table tbody tr:eq(0)').click();

 //               $('#electrode-type-table tbody tr:eq(0)').select();
            },

    });

      // [ Individual Column Searching (Text Inputs) ]
    $('#electrode-type-table tfoot th').each(function() {
            let title = $(this).text();
            $(this).html('<input type="text" class="form-control f-12" placeholder="' + title + '" />');
        });

        // [ Apply the search ]
    table_electrode_type.columns().every(function() {
            let that = this;

            $('input', this.footer()).on('keyup change', function() {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });

    table_electrode_type.on( 'select.dt', function ( e, dt, type, indexes ) {
        let data = dt.rows(indexes).data();
        selected_row = indexes[0]                           //only one - no multiple select
        id_electrode_type = data[0][0];
        $('#inputType').val(data[0][1]);
        $('#inputManu').val(data[0][2]);
        $('#inputMate').val(data[0][3]);
        $('#inputMacro').val(data[0][4]);
        $('#inputMicro').val(data[0][5]);
        $('#inputDimA').val(data[0][6]);
        $('#inputDimB').val(data[0][7]);


    });

    table_electrode_type.on('key-focus.dt', function(e, datatable, cell){
            // Select highlighted row
            table_electrode_type.row(cell.index().row).select();
    });

    table_electrode_type.on('click', 'tbody td', function(e){
        e.stopPropagation();
        // Get index of the clicked row
        let rowIdx = table_electrode_type.cell(this).index().row;

        // Select row
        table_electrode_type.row(rowIdx).select();
    });

    let btUpdate = $('#btnElectrodeUpdate');
    btUpdate.on('click',function(){
        update();
    });
    // switch (id_access){
    //     case 1:
    //             btUpdate.on('click',function(){
    //                 update();
    //             });
    //             break;
    //     case 2:                                 //read only
    //             btUpdate.removeClass("btn-primary").addClass("btn-outline-secondary disabled");
    //             break;
    //     case 99:                                 //read only
    //             btUpdate.removeClass("btn-primary").addClass("btn-outline-secondary disabled");
    //             break;

    //     }


   function update(){
       electrode_name = $('#inputName').val();
        $.ajax({
                url: 'electrode-update',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data: {
                    id_electrode_type: id_electrode_type,
                    electrode_name: electrode_name,
                    },
                success: function (data) {
                    switch(data.data) {
                        case 0:
                            $('#inputName').val(electrode_name);
                            let row_data = table_electrode_type.rows(selected_row).data();
                            $('#inputType').val(row_data[0][1]);
                            $('#inputManu').val(row_data[0][2]);
                            $('#inputMate').val(row_data[0][3]);
                            $('#inputMacro').val(row_data[0][4]);
                            $('#inputMicro').val(row_data[0][5]);
                            $('#inputDimA').val(row_data[0][6]);
                            $('#inputDimB').val(row_data[0][7]);
                            notify('Database operation success', 'success');
                            break;
                        case 1:
                            notify('Database operation error ' + data.message, 'danger');
                            break;
                        case 2:
                            notify('You do not have the permission to perform this operation', 'danger');
                            break;
                    }

                }
        });
    };
});