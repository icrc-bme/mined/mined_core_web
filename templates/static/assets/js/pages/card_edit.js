        let basic_datepicker = $('.basic-datepicker').daterangepicker({

            singleDatePicker: true,
            timePicker: false,
            showWeekNumbers: true,
            autoUpdateInput: true,
            startDate: false,
            //startDate: new Date(),
            showDropdowns: true,

            locale: {
                format: "YYYY-MM-DD",
                separator: " - ",
                applyLabel: "Apply",
                cancelLabel: "Cancel",
                fromLabel: "From",
                toLabel: "To",
                customRangeLabel: "Custom",
                weekLabel: "W",
                daysOfWeek: [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                monthNames: [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],
                firstDay: 1
            },
        });
        basic_datepicker.on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });

        let time_datepicker = $('.time-datepicker').daterangepicker({

            singleDatePicker: true,
            timePicker: true,
            showWeekNumbers: true,
            timePicker24Hour: true,
            autoUpdateInput: false,
            showDropdowns: true,

            locale: {
                format: "YYYY-MM-DD HH:mm",
                separator: " - ",
                applyLabel: "Apply",
                cancelLabel: "Cancel",
                fromLabel: "From",
                toLabel: "To",
                customRangeLabel: "Custom",
                weekLabel: "W",
                daysOfWeek: [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                monthNames: [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],
                firstDay: 1
            },
        });

        time_datepicker.on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('YYYY-MM-DD HH:mm'));
        });
        time_datepicker.on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });

