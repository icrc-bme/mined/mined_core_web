    let id_access = 2;



$(document).ready(function() {

    id_access = parseInt($('#idAccess').text());

    $('.basic-multiple').select2({
            tags: false,
            tokenSeparators: [',', ' '],
            placeholder: {
                id: '-1', // the value of the option
                text: 'Select an option'
            }
    })

    $('.basic-single').select2({
            placeholder: 'Select',
            allowClear: false,
    });

    if(($('.daterangepicker')).length) {
        let basic_datepicker = $('.basic-datepicker').daterangepicker({

            singleDatePicker: true,
            timePicker: false,
            showWeekNumbers: true,
            autoUpdateInput: true,
            showDropdowns: true,

            customArrowPrevSymbol: '<i class="feather icon-chevron-left"></i>',
            customArrowNextSymbol: '<i class="feather icon-chevron-right"></i>',
            //startDate: new Date(),

            locale: {
                format: "YYYY-MM-DD",
                separator: " - ",
                applyLabel: "Apply",
                cancelLabel: "Cancel",
                fromLabel: "From",
                toLabel: "To",
                customRangeLabel: "Custom",
                weekLabel: "W",
                daysOfWeek: [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                monthNames: [
                    "Jan",
                    "Feb",
                    "Mar",
                    "Apr",
                    "May",
                    "June",
                    "July",
                    "Aug",
                    "Sept",
                    "Oct",
                    "Nov",
                    "Dec"
                ],
                firstDay: 1
            },
        });
        basic_datepicker.on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });
        let time_datepicker = $('.time-datepicker').daterangepicker({
            singleDatePicker: true,
            timePicker: true,
            showWeekNumbers: true,
            autoUpdateInput: true,
            timePicker24Hour: true,
            drops:'auto',
            showDropdowns: true,

            //startDate: new Date(),

            locale: {
                format: "YYYY-MM-DD HH:mm",
                separator: " - ",
                applyLabel: "Apply",
                cancelLabel: "Cancel",
                fromLabel: "From",
                toLabel: "To",
                customRangeLabel: "Custom",
                weekLabel: "W",
                daysOfWeek: [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                monthNames: [
                    "Jan",
                    "Feb",
                    "Mar",
                    "Apr",
                    "May",
                    "June",
                    "July",
                    "Aug",
                    "Sept",
                    "Oct",
                    "Nov",
                    "Dec"
                ],
                firstDay: 1
            },
        });
        time_datepicker.on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });
    }

    let input_subject = $("#inputSubject").select2({
        ajax: {
            url: "subject-search-data",
            dataType: 'json',
            type: 'POST',
            delay: 250,
            headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
            data: function(params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function(data, params) {
                params.page = params.page || 1;
                return {
                    results: $.map(data, function (item) {

                        return {
                           text: item.id_subject,
                           id: item.id_mined
                        }
                    }),

                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: false,
        },
        escapeMarkup: function(markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 0,

    });
    input_subject.on("select2:selecting", function(e) {
            $.ajax({
                url:'subject-select',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data:
                    {
                    selected: e.params.args.data.id,                   //id_subject
                    },
                success: function (data) {
                    location.reload();
                }

            })
    });

    let input_session = $("#inputSession").select2({
        ajax: {
            url: "session-search-data",
            dataType: 'json',
            type: 'POST',
            delay: 250,
            headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
            data: function(params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function(data, params) {
                console.log(data);
                params.page = params.page || 1;
                return {
                    results: $.map(data, function (item) {

                        return {
                           text: item.session_name,
                           id: item.id
                        }
                    }),

                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
        },
        cache: false,
        /*
        initSelection: function(element, callback) {
        },
*/
        escapeMarkup: function(markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 0,
    });


    input_session.on("select2:selecting", function(e) {
            $.ajax({
                url:'session-select',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data:
                    {
                    selected: e.params.args.data.id,                   //id_session
                    },
                success: function (data) {
                    location.reload();
                }

            })
    });

    let input_electrode = $("#inputElectrode").select2({
        ajax: {
            url: "electrode-search-data",
            dataType: 'json',
            type: 'POST',
            delay: 250,
            headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
            data: function(params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function(data, params) {
                params.page = params.page || 1;
                return {
                    results: $.map(data, function (item) {

                        return {
                           text: item.electrode_name,
                           id: item.id
                        }
                    }),

                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
        },
        cache: false,

        escapeMarkup: function(markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 0,

    });

    input_electrode.on("select2:selecting", function(e) {
  //          document.getElementById('contact_id').innerHTML= '0';
            $.ajax({
                url:'electrode-select',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data:
                    {
                    selected: e.params.args.data.id,                   //id_electrode
                    },
                success: function (data) {
                    location.reload();
//                    document.location.href = "../available-detail";
                }

            })
    });

    let input_contact = $("#inputContact").select2({
        ajax: {
            url: "contact-search-data",
            dataType: 'json',
            type: 'POST',
            delay: 250,
            headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
            data: function(params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function(data, params) {
                params.page = params.page || 1;
                return {
                    results: $.map(data, function (item) {

                        return {
                           text: item.contact_name,
                           id: item.id
                        }
                    }),

                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: false
        },
        escapeMarkup: function(markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 0,

    });
    input_contact.on("select2:selecting", function(e) {
            $.ajax({
                url:'contact-select',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data:
                    {
                    selected: e.params.args.data.id,                   //id_contact
                    },
                success: function (data) {
                   location.reload();
                }
            })
    });

});

