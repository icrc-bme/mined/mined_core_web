// [ graph functions]

    function set_time_x(size){
        let time_array = new Array(size);
        for(let i = 0; i < size; i ++){
            time_array[i] = i/1000;
        }
        return time_array;
    }
    function plot(id_plot,name_connect,data,size){   //DIV id,'I',list,1200
        let time_x = set_time_x(size)
        let trace = {
            x:time_x,
            y:data,
            line: {
                shape: "spline",
                color: "#FF5370",
            },
            mode: 'lines',
            type:'scatter',
            name: name_connect,
        };


        let layout = {
            autosize: true,
            annotations: [
            {
                    xref: 'paper',
                    yref: 'paper',
                    x: 0,
                    xanchor: 'right',
                    y: 1,
                    yanchor: 'bottom',
                    text: 'mV',
                    showarrow: false
            },
           {
                xref: 'paper',
                yref: 'paper',
                x: 1,
                xanchor: 'left',
                y: 0,
                yanchor: 'top',
                text: 'sec',
                showarrow: false
            }],

            plot_bgcolor:"#39434d",
            paper_bgcolor:"#39434d",
            title: name_connect,
            showlegend: false,
            xaxis: {
                autotick: false,
                ticks: 'outside',
                tick0: 0,
                dtick: 200/1000,  //40
                ticklen: 0,
                tickwidth: 0,
                showgrid: true,
                zeroline: true,
                showline: false,
                mirror: 'ticks',
                gridcolor: '#969696', //bdbdbd
                gridwidth: 1,
                zerolinecolor: '#bdbdbd',  //#969696
                zerolinewidth: 2,
                linecolor: '#969696',
                linewidth: 2,
            },
            yaxis: {
                autorange: false,
                range: [-2.4,+2.4],
                type: 'linear',
                autotick: false,
                ticks: 'outside',
                tick0: 0,
                dtick: 0.40,
                ticklen: 0,
                tickwidth: 0,
                tickcolor: '#000',
                showgrid: true,
                zeroline: true,
                gridcolor: '#969696',
                gridwidth: 1,
                zerolinecolor: '#bdbdbd',
                zerolinewidth: 2,
            }
        };
        let config = {responsive: true}
        let data_plot = [trace];
        Plotly.newPlot(id_plot,data_plot,layout,config);

    }


// [ name of  CSRF Cookie is 'csrftoken']

    function getCSRFCookie(name) {
            let cookieValue = null;
            if (document.cookie && document.cookie !== '') {
                let cookies = document.cookie.split(';');
                for (let i = 0; i < cookies.length; i++) {
                    let cookie = cookies[i].trim();
                    // Does this cookie string begin with the name we want?
                    if (cookie.substring(0, name.length + 1) === (name + '=')) {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }return cookieValue;
    }

    function columnOption(column) {

//            var select = $('<select class="js-select-single form-control form-control-sm"><option value=""></option></select>')
//                $(".select-single").select2();

            let select;
                select = $('<select class="select-single form-control"><option value=""></option></select>')
                .appendTo($(column.footer()).empty())
                .on('change', function () {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );

                    column
                        .search(val ? '^' + val + '$' : '', true, false)
                        .draw();
                });

            column.data().unique().sort().each(function(d, j) {
                select.append('<option value="' + d + '">' + d + '</option>')
            });

    }

    function notify(message, type) {
        let delay = 2500;
        if (type === 'danger'){
            delay = 7500;
        }

        $.notify({
            icon: 'feather icon-bell',
            title: ' Database notify ',
            message: message,
            url: ''
        }, {
            element: 'body',
            type: type,
            allow_dismiss: true,
            placement: {
                from: 'bottom',
                align: 'left'
            },
            offset: {
                x: 30,
                y: 30
            },
            spacing: 10,
            z_index: 999999,
            delay: delay,
            timer: 1000,
            url_target: '_blank',
            mouse_over: false,
            animate: {
                enter: 'animated fadeInRight',
                exit: 'animated fadeOutLeft'
            },
            icon_type: 'class',
				template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
							'<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
							'<span data-notify="icon"></span> ' +
							'<span data-notify="title">{1}</span> ' + '<br>' +
							'<span data-notify="message"><b>{2}</b></span>' +
							'<div class="progress" data-notify="progressbar">' +
								'<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
							'</div>' +
							'<a href="{3}" target="{4}" data-notify="url"></a>' +
						'</div>'
        });
    }

    function pad (str, max) {
        str = str.toString();
        return str.length < max ? pad("0" + str, max) : str;
    }

    function getBirth(personal_number) {
        let year = parseInt(personal_number.substring(0, 2))
        if (year > 30) {
            year = year + 1900
        } else {
            year = year + 2000
        }
        let month = parseInt(personal_number.substring(2, 4))
        if (month > 50) {
            $("#inputSex").val('Female').trigger('change');
            month = month - 50;
        } else {
            $("#inputSex").val('Male').trigger('change');
        }
        let day = parseInt(personal_number.substring(4, 6))

        let birth = pad(year.toString(), 4) + '-' + pad(month.toString(), 2) + '-' + pad(day.toString(), 2);
        console.log(birth);
        return birth;
    }

    function stringSex(sex) {                    //from int to string
        let ret = "blank";
        switch (sex){
            case 0:
                ret = "male";
                break;
            case 1:
                ret = 'female';
                break;
            default:
                ret = "blank"
        }
        return ret;
    }

    function set_patient_option(data_time, diagnose_only) {         //used in patient-form js and ambulant.js - diagnose only
        let i;
        if(diagnose_only) {
//            $("#inputDiagnose").css('select2-selection{ color: }', '#4099ff');
            $("#labelDiagnose").css('color', '#4099ff');
            $("#labelDiagnose").css('font-weight', 'bold');
            $("#labelDiagnose").text('Specific diagnoses are not saved');
        }
        else {
            $("#labelDiagnose").css('color', '#adb7be');
            $("#labelDiagnose").css('font-weight', 'normal');
            $("#labelDiagnose").text('Specific diagnoses by');
        }
        for (i = 0; i < data_time['diagnoses_list'].length; i++) {
            let newOption = new Option(data_time['diagnoses_list'][i][1], data_time['diagnoses_list'][i][0], false, true);
            $("#inputDiagnose").append(newOption).trigger('change');

        }
        if(diagnose_only) return;
        for (i = 0; i < data_time['diseases_list'].length; i++) {
            let newOption = new Option(data_time['diseases_list'][i][1], data_time['diseases_list'][i][0], false, true);
            $("#inputDisease").append(newOption).trigger('change');
        }
        for (i = 0; i < data_time['treatments_list'].length; i++) {
            let newOption = new Option(data_time['treatments_list'][i][1], data_time['treatments_list'][i][0], false, true);
            $("#inputTreatment").append(newOption).trigger('change');
        }
    }




$(document).ready(function() {

    let patient = $('.patient-ajax').select2({
        ajax: {
            url: "patient-search-data",
            dataType: 'json',
            type: 'POST',
            delay: 250,
            headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
            data: function(params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function(data, params) {
                params.page = params.page || 1;
                return {
                    results: $.map(data, function (item) {

                        return {
                           text: item.specific_id,
                           id: item.id_patient
                        }
                    }),

                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: false,
        },
        escapeMarkup: function(markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 0,

    });
    patient.on("select2:selecting", function(e) {
           $("#spinnerPatient").removeAttr("hidden");
           $.ajax({
                url:'patient-select',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data:
                    {
                    selected: e.params.args.data.id,                   //id_patient
                    },
                success: function (data) {
 //                   location.reload();
                    window.location=window.location;

                }
            })
    });


    $('.name-ajax').select2({
        ajax: {
            url: "patient-name-data",
            dataType: 'json',
            type: 'POST',
            delay: 250,
            headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
            data: function(params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function(data, params) {
                params.page = params.page || 1;
                return {
                    results: $.map(data, function (item) {

                        return {
                           text: item.name,
                           id: item.id_patient
                        }
                    }),

                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: false,
        },
        escapeMarkup: function(markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 0,

    });

    $('.name-ajax').on("select2:selecting", function(e) {
            $("#spinnerPatient").removeAttr("hidden");
            $.ajax({
                url:'patient-select',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data:
                    {
                    selected: e.params.args.data.id,                //id_patient
                    },
                success: function (data) {
//                    location.reload();
                      window.location=window.location;

//                    document.location.href = "session-overview"
//                    document.location.href = "../available-detail";
//                    var data_array = Object.values(data);
//                    document.getElementById('subject_id').innerHTML= data_array[0][0];  //returned by django
//                    document.getElementById('session_count').innerHTML=data_array[0][1];
 //                   document.location.href = "../available-detail";

                }
            })
    });



    function personalAjax() {
            $.ajax({
                url: 'patient-personal',
                type: 'POST',
                headers: {'X-CSRFToken': getCSRFCookie('csrftoken')},
                data:
                    {
                        personal: $("#inputPersonal").val(),
                    },
                success: function (data) {
                    if (jQuery.isEmptyObject(data)) {
                        $("#inputPersonal").val('Not found');
                    }
                    else {
                        //location.reload();
                        window.location=window.location;
                    }
                }
            });
    }

    $("#inputPersonal").on('keypress',function(e) {
        if(e.which == 13) {
           $("#spinnerPatient").removeAttr("hidden");
            personalAjax();
        }
    });

    $("#buttonPersonal").on('click',function(){
        $("#spinnerPatient").removeAttr("hidden");
        personalAjax();
    });

    $("#liMedical").on('click',function(){
        $("#spinnerMedical").removeAttr("hidden");
    });

    $("#liExport").on('click',function(){
        $("#spinnerExport").removeAttr("hidden");
    });

    $("#liActivity14").on('click',function(){                   //laboratory
        $("#spinnerStudyActivity").removeAttr("hidden");
    });

});
